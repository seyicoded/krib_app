import React, { useState, useEffect, useRef } from "react";
import {
  NavigationContainer,
  useNavigationContainerRef,
} from "@react-navigation/native";
import {
  StatusBar,
  BackHandler,
  Appearance,
  AppState,
  Platform,
  View,
  Alert
} from "react-native";
import RootNavigator from "./navigation/Index";
// import { Provider } from 'react-redux';
import { useFlipper } from "@react-navigation/devtools";
// import { Asset } from 'react-native-unimodules';
// import * as Font from 'expo-font';
import { colors, fonts } from "./theme";
//import firebase from "react-native-firebase";
import firestore from '@react-native-firebase/firestore';

import AsyncStorage from "@react-native-async-storage/async-storage";
// import { saveColorSchemeAction } from './redux/auth';
import LottieView from "lottie-react-native";
import GlobalFont from "react-native-global-font";
import { Layout } from "./constants";
import RNMonnify from "@monnify/react-native-sdk";
import SplashScreen from "react-native-splash-screen";
import messaging from '@react-native-firebase/messaging';
import Messaging from "@react-native-firebase/messaging";

import {
  GetPersistData,
  ShowNotification
} from "./utils/UtilityFunctions";

// export const { store, persistor } = configureStore();

// const MONIFY_CONTRACTCODE = '834378943072';
// const MONIFY_APIKEY = 'MK_PROD_YVD005PQF9';
// const MONIFY_APPLICATIONMODE = 'LIVE';

const MONIFY_CONTRACTCODE = "6865681435";
const MONIFY_APIKEY = "MK_TEST_FCXBB5A7G8";
const MONIFY_APPLICATIONMODE = "TEST";

export default function App() {
  const navigationRef = useNavigationContainerRef();
  const [isLoadingComplete, setIsLoadingComplete] = useState(false);

  useFlipper(navigationRef);

  useEffect(() => {
    (async()=>{
      await checkPermission();
      await createNotificationListeners();
      await InnitialteRNMonnify();
    })()
    
  }, []);

  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);
  let [isDark, setIsDark] = useState(false);

  useEffect(() => {
    setIsDark(Appearance.getColorScheme() === "dark");
  });

  useEffect(() => {
    const subscription = AppState.addEventListener("change", (nextAppState) => {
      if (
        appState.current.match(/inactive|background|active/) &&
        nextAppState === "active"
      ) {
        let mode = Appearance.getColorScheme();
        console.log("App has come to the foreground!");
        // store.dispatch(saveColorSchemeAction(mode))
        // console.log(store)
        setStatusBar();
        setIsDark(Appearance.getColorScheme() === "dark");
      }

      appState.current = nextAppState;
      setAppStateVisible(appState.current);
    });
    

    if(Platform.OS == 'android'){
      SplashScreen.hide();
    }else{
      SplashScreen.hide();
    }
  }, []);

  const setStatusBar = () => {
    if (Platform.OS === "android") {
      StatusBar.setBarStyle("light-content");
      StatusBar.setBackgroundColor(colors.alerzoDark);
    }
  };

  const checkPermission = async () => {
    // const enabled = await firebase.messaging().hasPermission();
    try{
      const authStatus = await Messaging().hasPermission();
      const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL;
      if (enabled) {
        console.log("firebase permission : ", enabled);
        getToken();
        // Alert.alert('permission reached and normal requested');
      } else {
        requestPermission();
        // Alert.alert('permission reached and normal');
        // console.log("permission reached and normal");
      }

      Messaging().onMessage(async remoteMessage => {
        // Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
        ShowNotification('New Message Recieved...');
      });

      Messaging().onNotificationOpenedApp(async remoteMessage => {
        // Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
        console.log('onOpenApp is')
        console.log(remoteMessage.data)
        if(navigationRef.current != null && remoteMessage.data != {} && remoteMessage.data != undefined){
          navigationRef.current.navigate('Main', {
            screen: "MessageView",
            params: {
              foundConversationId: remoteMessage.data.id,
              foundConversation: remoteMessage.data.item,
            }
          })
        }
        
        ShowNotification('New Message Recieved...');
      })

      Messaging().getInitialNotification().then(remoteMessage => {
        // Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
        
        if(remoteMessage != null){
          if(navigationRef.current != null && remoteMessage.data != {} && remoteMessage.data != undefined){
            navigationRef.current.navigate('Main', {
              screen: "MessageView",
              params: {
                foundConversationId: remoteMessage.data.id,
                foundConversation: remoteMessage.data.item,
              }
            })
          }
          ShowNotification('New Message Recieved InitialNotification...');
        }
        
        
      })
  
    }catch(e){
      console.log("check Permission: "+e);
    }
    
  };

  const InnitialteRNMonnify = async () => {
    RNMonnify.initialize({
      apiKey: MONIFY_APIKEY,
      contractCode: MONIFY_CONTRACTCODE,
      applicationMode: MONIFY_APPLICATIONMODE,
    });
  };

  const requestPermission = async () => {
    try {
      // await firebase.messaging().requestPermission();
      await Messaging().requestPermission();
      // User has authorised
      getToken();
      // Alert.alert('permission requested');
    } catch (error) {
      // User has rejected permissions
      console.log("permission rejected");
      Alert.alert('notification permission is required');
    }
  };

  const getToken = async () => {
    let fcmToken = await AsyncStorage.getItem("fcmToken");
    console.log("fcmToken", fcmToken);
    if (!fcmToken) {
      fcmToken = await Messaging().getToken();
      if (fcmToken) {
        await AsyncStorage.setItem("fcmToken", fcmToken);
        console.log("fcmToken hiiiii", fcmToken);
      }
    }

    updateFCMToken(fcmToken);
  };

  const updateFCMToken = async(token: string)=>{
    console.warn(token)
    // check if logined
    try {
      let userToken = await GetPersistData("Loggedinstate");
      let loggedinUserdetails = await GetPersistData("loggedinUserdetails");
      loggedinUserdetails = JSON.parse(loggedinUserdetails);

      console.log(loggedinUserdetails, "loggedinUserdetails.new ");
      console.log(loggedinUserdetails.profile, "loggedinUserdetails.profile ");
      // setLoginProfile(loggedinUserdetails?.profile);

      let ss = JSON.parse(userToken);
      console.log(ss, "login stateeee");
      if (ss?.isloggedin === true) {
        // is logged in
        // Alert.alert('signed in')
        console.log(loggedinUserdetails)
        const email_ = loggedinUserdetails.profile.username;
        const avatar_ = loggedinUserdetails.profile.usersProfile.avatar;

        // Alert.alert('sa')
        await firestore().collection('device_token').doc(email_).set({token: token, avatar: avatar_, name: loggedinUserdetails.profile.usersProfile.fullname});
      } else {
        console.warn('........')
      }
      console.log(ss, "login details ");
    } catch (err) {
      Alert.alert('can\'t connect to firebase')
      console.warn(err)
    }
  }

  // async function requestUserPermission() {
  //   const authStatus = await messaging().requestPermission();
  //   const enabled =
  //     authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
  //     authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  //   if (enabled) {
  //     console.log('Authorization status:', authStatus);
  //   }
  // }

  const createNotificationListeners = async () => {
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    const notificationListener = firebase
      .notifications()
      .onNotification((notification) => {
        const { title, body } = notification;
        showAlert(title, body);
      });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    const notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen) => {
        const { title, body } = notificationOpen.notification;
        showAlert(title, body);
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      showAlert(title, body);
    }

    /*
     * Triggered for data only payload in foreground
     * */
    const messageListener = Messaging().onMessage((message) => {
      //process data message
      console.log(JSON.stringify(message));
    });
  };

  const showAlert = (title: string, body: string | undefined) => {
    // Alert.alert(
    //   title,
    //   body,
    //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    //   { cancelable: false }
    // );
  };

  const backAction = () => {
    // store.dispatch(NavigationActions.back())
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backAction);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backAction);
  }, []);

  useEffect(() => {
    // loadResourcesAsync();
    let fontName = fonts.GilmerLight;
    GlobalFont.applyGlobal(fontName);
  }, []);

  // const loadResourcesAsync = async () => {
  //   await Promise.all([
  //     Asset.loadAsync([
  //       require("./assets/images/intro_one.png"),
  //       require("./assets/images/intro_two.png"),
  //       require("./assets/images/intro_three.png"),
  //       require("./assets/images/buttonArrow.png"),
  //       require("./assets/images/alerzo_white.png"),
  //       require("./assets/images/progressDots.png"),
  //       require("./assets/images/progressDotsTwo.png"),
  //       require("./assets/images/progressDotsThree.png"),
  //       require("./assets/images/logo.png"),
  //     ]),
  //     Font.loadAsync({
  //       FilsonProRegular: require("./assets/fonts/FilsonProRegular.ttf"),
  //       FilsonProBold: require("./assets/fonts/FilsonProBold.ttf"),
  //       GilmerLight: require("./assets/fonts/GilmerLight.otf"),
  //       GilmerBold: require("./assets/fonts/GilmerBold.otf"),
  //       GilmerHeavy: require("./assets/fonts/GilmerHeavy.otf"),
  //       GilmerMedium: require("./assets/fonts/GilmerMedium.otf"),
  //       GilmerOutline: require("./assets/fonts/GilmerOutline.otf"),
  //       GilmerRegular: require("./assets/fonts/GilmerRegular.otf"),
  //     }),
  //   ]);
  // };

  // if (!isLoadingComplete)
  //   return (
  //     <View
  //       style={{
  //         height: Layout.window.height,
  //         backgroundColor: isDark ? colors.alerzoDark : colors.alerzoBlue,
  //       }}
  //     >
  //       <LottieView
  //         source={require("./lottie/loading.json")}
  //         autoPlay
  //         onAnimationFinish={() => setIsLoadingComplete(true)}
  //         loop={false}
  //       />
  //     </View>
  //   );
  return (
    // <Provider store={store}>
    <NavigationContainer ref={navigationRef}>
      <RootNavigator />
    </NavigationContainer>
    // </Provider>
  );
}
