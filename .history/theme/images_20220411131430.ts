export const images = {
    // intro
 
    mobile9: require("../assets/images/9mobile.png"),
    drawer: require("../assets/images/menu.png"),
    back: require("../assets/images/arr_Back.png"),
    search: require("../assets/images/search.png"),
    settings: require("../assets/images/settings.png"),
    naira: require("../assets/images/naira.png"),
    bg1: require("../assets/images/bg1.png"),
    bg2: require("../assets/images/bg2.png"),
    bg3: require("../assets/images/bg3.png"),
    more: require("../assets/images/more.png"),
    arrDown: require("../assets/images/arrDown.png"),
    arrFowr: require("../assets/images/arrFowr.png"),
    add: require("../assets/images/add.png"),
    caretDown: require("../assets/images/caretDown.png"),
    card2: require("../assets/images/card2.png"),
    card3: require("../assets/images/card3.png"),
    card4: require("../assets/images/card4.png"),
    card5: require("../assets/images/card5.png"),
    card6: require("../assets/images/card6.png"),
    card7: require("../assets/images/card7.png"),
    prop: require("../assets/images/prop.png"),
    msg: require("../assets/images/msg.png"),
    msgs: require("../assets/images/msgs.png"),
    msg_A: require("../assets/images/msg_A.png"),
    bell: require("../assets/images/bell.png"),
    bell_A: require("../assets/images/bell_A.png"),
    prof: require("../assets/images/prof.png"),
    prof_A: require("../assets/images/prof_A.png"),
    exp: require("../assets/images/exp.png"),
    exp_A: require("../assets/images/exp_A.png"),
    profile: require("../assets/images/profile.png"),
    line: require("../assets/images/line.png"),
    close: require("../assets/images/close.png"),
    correct: require("../assets/images/correct.png"),
    correct2: require("../assets/images/correct2.png"),
    inputCard: require("../assets/images/inputCard.png"),
    upload: require("../assets/images/upload.png"),
    upload2: require("../assets/images/upload2.png"),
    upload3: require("../assets/images/upload3.png"),
    verify: require("../assets/images/verify.png"),
    faq: require("../assets/images/settingsIcon/faq.png"),
    customerSupport: require("../assets/images/settingsIcon/customerSupport.png"),
    logout: require("../assets/images/settingsIcon/logout.png"),
    pin_security: require("../assets/images/settingsIcon/pin_security.png"),
    // profileIcon: require("../assets/images/settingsIcon/profile.png"),
    tellFriends: require("../assets/images/settingsIcon/tellFriends.png"),
    termService: require("../assets/images/settingsIcon/term_service.png"),
    camera: require("../assets/images/camera.png"),
    profileImg: require("../assets/images/profileImg.png"),
    plusIcon: require("../assets/images/plus.png"),
    arwBack: require("../assets/images/arwBack.png"),
    card1: require("../assets/images/card1.png"),
    img1: require("../assets/images/img1.png"),
    img2: require("../assets/images/img2.png"),
    img3: require("../assets/images/img3.png"),
    comment: require("../assets/images/comment.png"),
    house: require("../assets/images/house.png"),
    call: require("../assets/images/call.png"),
    oyin: require("../assets/images/oyin.png"),
    SbExplore: require("../assets/images/SbExplore.png"),
    SbMsg: require("../assets/images/SbMsg.png"),
    SbNoti: require("../assets/images/SbNoti.png"),
    SbProfile: require("../assets/images/SbProfile.png"),
    SbSet: require("../assets/images/SbSet.png"),
    cc: require("../assets/images/cc.png"),
    ccWrap: require("../assets/images/ccWrap.png"),
    checkfalse: require("../assets/images/checkfalse.png"),
    checktrue: require("../assets/images/checktrue.png"),
    arrBack: require("../assets/images/arrBack.png"),
    call2: require("../assets/images/call2.png"),
     
}
export type ImageTypes = keyof typeof images
