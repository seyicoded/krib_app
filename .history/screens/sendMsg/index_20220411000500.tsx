// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";
import { KeyboardAvoidContainer } from '../../components/keyboard.avoid.container'

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

import CheckBox from "@react-native-community/checkbox";
import firestore from '@react-native-firebase/firestore';

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import MapView from "react-native-maps";

// redux
import { ApplicationState } from "../../redux";

import { ShowNotification } from "../../utils/UtilityFunctions";
// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomInput from "../../components/customInput/CustomInput";
import CustomAreaInput from "../../components/customInput/CustomAreaInput";
import CustomDate from "../../components/date/CustomDate";
import CustomTooltip from "../../components/tooltip/tooltip";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { sendMsg } from "../../services/api";

// util

interface StepRegistrationScreenProps {}
const actionSheeYearBuilttRef = React.createRef();
const actionSheetUnitRef = React.createRef();

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  flexWrap: "wrap",
};
const FLEXROW2: TextStyle = {
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
};
const FLEXROWVERIFY: TextStyle = {
  width: "100%",
  alignItems: "flex-start",
  justifyContent: "flex-start",
  flexDirection: "row",
  flex: 1,
  marginBottom: 15,
};
const SETMARGIN: TextStyle = {
  marginRight: 10,
};
const CARDCONTAINER: TextStyle = {
  height: "100%",
  width: "100%",
  padding: 10,
  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: 240,
  marginRight: 10,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BTNSTYLE: TextStyle = {
  borderRadius: 5,
  backgroundColor: colors.dark2,
  justifyContent: "center",
  alignItems: "center",
  padding: 15,
  width: "100%",
  marginTop: 40,
};
const CHECKWRAP: TextStyle = {
  flex: 1,
  padding: 10,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "42%",
  width: "45%",
  height: 70,
  minWidth: "40%",
};
const CHECKWRAP3: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "48%",
  width: "45%",
  height: 70,
  minWidth: "40%",
};
const CHECKWRAP5: TextStyle = {
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  width: "43%",
  height: 120,
  minWidth: "40%",
  marginVertical: 10,
};
const CHECKWRAP2: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 15,
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  maxWidth: "100%",
  minWidth: "100%",
  height: 75,
  marginBottom: 10,
};
const LIKECHECKBOX: TextStyle = {
  flex: 1,
  borderWidth: 1,
  width: "97%",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 3,
};
const INTERIOCHECKER: TextStyle = {
  flex: 1,
  borderWidth: 0,
  width: "20%",
  maxWidth: "20%",
  justifyContent: "center",
  alignItems: "center",
  maxHeight: "91%",
  minHeight: "91%",
};
const INPUTWRAP2: ViewStyle = {
  width: "100%",
  paddingHorizontal: 15,
  marginBottom: 20,
};
const RNCHECKBOXSTYLE: ViewStyle = {
  height: 5,
  width: 10,
  borderRadius: 10 / 2,
};
const SETINPUTSTYLE: TextStyle = {
  marginBottom: 10,
};

const StepRegistration = ({ navigation, route }) => {
  const { propertyData } = route.params;
  console.warn(propertyData, "property data");
  const navigations = useNavigation();
  const [stepValue, setStepValue] = useState("Step 5");
  const [show, setShow] = useState(false);
  const [subjectSet, setSubject] = useState("");
  const [messageSet, setMessage] = useState("");
  const [processing, setprocessing] = useState(false);

  const mode = "dark";
  const handleYearBuilt = () => {
    actionSheeYearBuilttRef?.current?.show();
  };
  const handleUnit = () => {
    actionSheetUnitRef?.current?.show();
  };

  const processSendMsg = async () => {
    if (subjectSet === "") {
      ShowNotification("Subject is  required", "warning");
      return;
    }
    if (messageSet === "") {
      ShowNotification("Pls type your message", "warning");
      return;
    }

    let payload = {
      propertyId: propertyData.id,
      subject: subjectSet,
      message: messageSet,
    };
    try {
      setprocessing(true);
      // using firebase instead now...
      await firestore().collection('chat_session').add({
        message: [propertyData.id, subjectSet, messageSet]
      });

      const result = await sendMsg(payload);
      const { kind, data } = result;
      const loginData = data;
      console.warn(loginData); ``
      setprocessing(false);
      if (kind === "ok") {
        const msg = loginData?.message;
        ShowNotification("Message sent to the seller", "success");
        // ShowNotification(msg);
        // navigation.navigate("Main", {
        //   screen: "Message",
        // });
        navigation.navigate("Main", { 
          screen: "TabHome",
          params: {
            screen: "Home",
            params: {
              screen: "Message",
            },
          },
        });
      } else {
        const newMsg = data.message;

        ShowNotification(newMsg);
      }
    } catch ({ message }) {
      ShowNotification(message);
    }
  };

  //props
  const actionSheetYearBuiltBoddy = (
    <View style={{ padding: 20 }}>
      <TouchableOpacity
        onPress={() => {
          handleYearBuilt();
        }}
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <CheckBox style={RNCHECKBOXSTYLE} />
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={"Year 2018"}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          handleYearBuilt();
        }}
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <CheckBox style={RNCHECKBOXSTYLE} />
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={"Year 2019"}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          handleYearBuilt();
        }}
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <CheckBox style={RNCHECKBOXSTYLE} />
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={"Year 2020"}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          handleYearBuilt();
        }}
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <CheckBox style={RNCHECKBOXSTYLE} />
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={"Year 2021"}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          handleYearBuilt();
        }}
        style={{
          borderBottomWidth: 0,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <CheckBox style={RNCHECKBOXSTYLE} />
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={"Year 2022"}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
    </View>
  );
  const actionSheetUnitBody = (
    <View style={{ padding: 20 }}>
      <TouchableOpacity
        onPress={() => {
          handleUnit();
        }}
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <CheckBox style={RNCHECKBOXSTYLE} />
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={"1 Unit"}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          handleUnit();
        }}
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <CheckBox style={RNCHECKBOXSTYLE} />
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={"2 Unit"}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          handleUnit();
        }}
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <CheckBox style={RNCHECKBOXSTYLE} />
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={"3 Unit"}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          handleUnit();
        }}
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <CheckBox style={RNCHECKBOXSTYLE} />
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={"4 Unit"}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          handleUnit();
        }}
        style={{
          borderBottomWidth: 0,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <CheckBox style={RNCHECKBOXSTYLE} />
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={"5 Unit"}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
    </View>
  );
  const AppliancesandFinishings = [
    {
      label: "Air conditioning",
      value: "Air conditioning",
    },
    {
      label: "Tiled flooring",
      value: "Tiled flooring",
    },
    {
      label: "Hardwood flooring",
      value: "Hardwood flooring",
    },
    {
      label: "POP ceiling",
      value: "POP ceiling",
    },
    {
      label: "PVC ceiling",
      value: "PVC ceiling",
    },
    {
      label: "Oven",
      value: "Oven",
    },
    {
      label: "Bath tubs",
      value: "Bath tubs",
    },
    {
      label: "Cabinet & drawers",
      value: "Cabinet & drawers",
    },
    {
      label: "Cooking stove & gas",
      value: " Cooking stove & gas",
    },
    {
      label: "Heat extractor",
      value: "Heat extractor",
    },
    {
      label: "Fire alarm",
      value: "Fire alarm",
    },
    {
      label: "Wifi",
      value: "Wifi",
    },
    {
      label: "Granite tops",
      value: "Granite tops",
    },
    {
      label: "Glass showers",
      value: "Glass showers",
    },
    {
      label: "Washer/dryer",
      value: "Washer/dryer",
    },
  ];
  const BuildingAmenities = [
    {
      label: "Swimming pool",
      value: "Swimming pool",
    },
    {
      label: "Gym/fitness center",
      value: "Gym/fitness center",
    },
    {
      label: "Children playground",
      value: "Children playground",
    },
    {
      label: "Parking lot",
      value: "Parking lot",
    },
    {
      label: "Mall",
      value: "Mall",
    },
    {
      label: "Hospital",
      value: "Hospital",
    },
    {
      label: "School",
      value: "School",
    },
    {
      label: "Business Center",
      value: "Business Center",
    },
    {
      label: "Electricity",
      value: "Electricity",
    },
    {
      label: " Good & accessable roads",
      value: " Good & accessable roads",
    },
    {
      label: "Security",
      value: "Security",
    },
    {
      label: "Street lighting",
      value: "Street lighting",
    },
    {
      label: "Cleaning",
      value: "Cleaning",
    },
    {
      label: "Palto/balcony",
      value: "Palto/balcony",
    },
    {
      label: "Allowance of pets",
      value: "Allowance of pets",
    },
    {
      label: "CCTV Camera",
      value: "CCTV Camera",
    },
  ];
  const attashedImage = [
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726675/samples/Rectangle_uzu3mk.png",
    },
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726676/samples/Rectangle-1_ivkege.png",
    },
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726676/samples/Rectangle-2_glpu9u.png",
    },
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726676/samples/Rectangle-3_lwa9z5.png",
    },
  ];

  let LoopedattashedImage = attashedImage.map((item, index) => {
    return (
      <ImageBackground
        key={index}
        resizeMode="stretch"
        source={{ uri: item.imageUrl }}
        style={[CHECKWRAP5]}
      />
    );
  });
  let LoopedBuildingAmenities = BuildingAmenities.map((item, index) => {
    return (
      <ImageBackground
        key={index}
        resizeMode="stretch"
        source={images.card5}
        style={[CHECKWRAP3]}
      >
        <TouchableOpacity
          style={[LIKECHECKBOX, { borderColor: colors.transparent }]}
        >
          <CustomText
            textSize={16}
            textWeight={"normal"}
            textcolor={colors.black}
            displayText={item.label}
            textStyle={{
              lineHeight: 18,
              textAlign: "center",
            }}
          />
        </TouchableOpacity>
      </ImageBackground>
    );
  });
  let LoopedAppliancesandFinishings = AppliancesandFinishings.map(
    (item, index) => {
      return (
        <ImageBackground
          key={index}
          resizeMode="stretch"
          source={images.card5}
          style={[CHECKWRAP3]}
        >
          <TouchableOpacity
            style={[LIKECHECKBOX, { borderColor: colors.transparent }]}
          >
            <CustomText
              textSize={16}
              textWeight={"normal"}
              textcolor={colors.black}
              displayText={item.label}
              textStyle={{
                lineHeight: 18,
                textAlign: "center",
              }}
            />
          </TouchableOpacity>
        </ImageBackground>
      );
    }
  );

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header leftIcon={true} onLeftPress={() => navigation.goBack()} />
      <KeyboardAvoidContainer style={{ paddingVertical: 15 }}>
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View style={[{ paddingHorizontal: 15, marginBottom: 20 }]}>
              <View style={FLEXROWVERIFY}>
                <View style={{ flex: 1 }}>
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.black}
                    displayText={propertyData?.address}
                    textStyle={{ textAlign: "left", flex: 1 }}
                  />
                  <Naira
                    amount={propertyData?.price}
                    amountWeight={"bold"}
                    boldnaira
                  />
                </View>
                <Image
                  source={{
                    uri: propertyData?.propertyImages[0]?.url,
                  }}
                  style={{ width: 181, height: 121 }}
                />
              </View>
            </View>
            <View style={INPUTWRAP2}>
              <CustomInput
                inputText={"Subject"}
                inputTextWeight={"normal"}
                inputTextSize={15}
                inputTextcolor={colors.black}
                inputTextSty={SETINPUTSTYLE}
                onChangeText={(value: string) => {
                  setSubject(value);
                }}
                placeHolderText={"Enquiry for new 5 bedroom terrace duplex"}
                showIcon={false}
              />
            </View>
            <View style={INPUTWRAP2}>
              <CustomAreaInput
                inputText={"Message"}
                inputTextWeight={"normal"}
                inputTextSize={15}
                inputTextcolor={colors.black}
                inputTextSty={SETINPUTSTYLE}
                onChangeText={(value: string) => {
                  setMessage(value);
                }}
                placeHolderText={""}
                showIcon={false}
              />
            </View>

            <View style={[{ paddingHorizontal: 20, marginBottom: 22 }]}>
              <TouchableOpacity
                style={BTNSTYLE}
                onPress={() => {
                  processSendMsg();
                  // navigation.navigate("ThankYou");
                }}
              >
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.white}
                  displayText={
                    processing === true ? "Processing...." : "Send message"
                  }
                  textStyle={{ marginBottom: 6, textAlign: "center" }}
                />
              </TouchableOpacity>
            </View>
          </Animatable.View>
        </Animatable.View>
      </KeyboardAvoidContainer>
    </View>
  );
};

export default StepRegistration;
