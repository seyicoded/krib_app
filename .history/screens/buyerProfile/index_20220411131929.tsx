// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";

// redux
import { ApplicationState } from "../../redux";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { GetPersistData } from "../../utils/UtilityFunctions";
import { getviewedproperty } from "../../services/api";

// util

interface BuyersProfileScreenProps { }

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: "100%",
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BTNSTYLE: TextStyle = {
  borderRadius: 5,
  backgroundColor: colors.dark2,
  justifyContent: "center",
  alignItems: "center",
  padding: 15,
  width: "100%",
  marginTop: 40,
};

const BuyersProfile = ({ route }) => {
  const navigation = useNavigation();
  const [userDataGotten, setUserDataGotten] = useState(null);
  const [viewedPropertys, setViewedProperty] = useState([]);
  const [listedProperty, setListedProperty] = useState([]);
  const [reservedProperty, setReservedProperty] = useState([]);
  const mode = "dark";

  useEffect(() => {
    async function fetAllUserdata() {
      let userData = await GetPersistData("loggedinUserdetails");
      const newdata = JSON.parse(userData);
      setUserDataGotten(newdata?.profile);
    }
    fetAllUserdata();
  }, []);

  useEffect(() => {
    // alert("hyey")
    async function fetchViewedProperty() {
      // setLoadingState(true);
      // <Myspiner state={true} />;
      try {
        const result = await getviewedproperty();
        const { kind, data } = result;

        console.warn(data, "get viewed property");

        if (kind === "ok") {
          const newViewProperties = data?.data?.viewProperties;
          const newPropertyReserved = data?.data?.propertyReserved;
          const newListedPropeerty = data?.data?.listedProperty;
          // console.warn(dd.length);listedPropeerty

          setViewedProperty(newViewProperties);
          setListedProperty(newListedPropeerty);
          setReservedProperty(newPropertyReserved);
          // setLoadingState(false);
        }
      } catch ({ message }) {
        console.warn(message);

        // ShowNotification(message);
      }
    }
    fetchViewedProperty();
  }, []);

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftIcon={false}
        title="Profile"
        rightIcon
        onLeftPress={() => navigation.goBack()}
        onRightPress={() => {
          navigation.navigate("Main", {
            screen: "MySettings",
            params: { propertyData: listedProperty },
          })
        }
        }
      />
      <ScrollView style={{ paddingVertical: 15 }}>
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View
              style={{
                paddingHorizontal: 15,
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
              }}
            >
              <ImageBackground
                source={images.profile}
                style={{
                  width: 149,
                  height: 110,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Image
                  source={{
                    uri:
                      userDataGotten?.usersProfile?.avatar ||
                      "https://res.cloudinary.com/db2tci0wk/image/upload/v1644118791/Pngtree_avatar_user_profile_flat_4898047_bye3bj.png",
                  }}
                  style={{
                    width: 70,
                    height: 70,
                    borderRadius: 70 / 2,
                    marginLeft: -10,
                  }}
                />
              </ImageBackground>
            </View>
            <View style={[{ paddingHorizontal: 15 }]}>
              <CustomText
                textSize={16}
                textWeight={"bold"}
                textcolor={colors.black}
                displayText={userDataGotten?.usersProfile?.fullname}
                textStyle={{ marginTop: 14, textAlign: "center" }}
              />
              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={userDataGotten?.username}
                textStyle={{ marginBottom: 6, textAlign: "center" }}
              />
              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={userDataGotten?.usersProfile?.phoneNumber}
                textStyle={{ marginBottom: 6, textAlign: "center" }}
              />

              <View
                style={[
                  FLEXROW,
                  { marginBottom: 10, justifyContent: "center" },
                ]}
              >
                <ImageBackground
                  resizeMode="stretch"
                  source={images.card3}
                  style={{
                    flex: 1,
                    padding: 10,
                    justifyContent: "center",
                    alignItems: "center",
                    maxWidth: "32%",
                    paddingTop: 40,
                    paddingBottom: 50,
                  }}
                >
                  <CustomText
                    textSize={34}
                    textWeight={"bold"}
                    textcolor={colors.yellow}
                    displayText={`${viewedPropertys.length}`}
                    textStyle={{
                      marginBottom: 0,
                      lineHeight: 52,
                    }}
                  />
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.dark}
                    displayText={"Property"}
                    textStyle={{
                      marginBottom: 0,
                      marginTop: 0,
                      lineHeight: 24,
                    }}
                  />

                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.dark}
                    displayText={"Viewed"}
                    textStyle={{
                      marginBottom: 0,
                      marginTop: 0,
                      lineHeight: 24,
                    }}
                  />
                </ImageBackground>
                <View
                  style={{
                    width: 5,
                    height: "80%",
                    backgroundColor: colors.dark2,
                    borderRadius: 10,
                    marginHorizontal: 30,
                  }}
                ></View>
                <ImageBackground
                  resizeMode="stretch"
                  source={images.card3}
                  style={{
                    flex: 1,
                    padding: 10,
                    justifyContent: "center",
                    alignItems: "center",
                    maxWidth: "32%",
                    paddingTop: 40,
                    paddingBottom: 50,
                  }}
                >
                  <CustomText
                    textSize={34}
                    textWeight={"bold"}
                    textcolor={colors.yellow}
                    displayText={`${reservedProperty.length}`}
                    textStyle={{
                      marginBottom: 0,
                      lineHeight: 52,
                    }}
                  />
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.dark}
                    displayText={"Reserved"}
                    textStyle={{
                      marginBottom: 0,
                      marginTop: 0,
                      lineHeight: 24,
                    }}
                  />
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.dark}
                    displayText={"Properties"}
                    textStyle={{
                      marginBottom: 0,
                      marginTop: 0,
                      lineHeight: 24,
                    }}
                  />
                </ImageBackground>
              </View>
              <TouchableOpacity style={BTNSTYLE}
                onPress={() => {
                  navigation.navigate('Main', {
                    screen: 'TabHome',
                    params: {
                      screen: 'Home',
                      params: {
                        screen: 'Explore',
                      },
                    },
                  });


                }}
              >
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.white}
                  displayText={"See more listings"}
                  textStyle={{ marginBottom: 6, textAlign: "center" }}
                />
              </TouchableOpacity>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
    </View>
  );
};

export default BuyersProfile;
