// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
  Alert
} from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import PhoneInput from "react-native-phone-number-input";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import MapView from "react-native-maps";
import {
  PersistLoginRecord,
  GetCompanyFullDetails,
  ShowNotification,
  GetCompanyKey,
  GetPersistData,
  PersistData,
  cloudinaryUpload,
} from "../../utils/UtilityFunctions";
// redux
import { ApplicationState } from "../../redux";

// components
import {
  YOUR_CLOUDINARY_PRESET,
  YOUR_CLOUDINARY_NAME,
  requestCameraPermission,
  YOUR_CLOUDINARY_PRESET_JobBanner,
} from "../../utils/UtilityFunctions";

import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import RNRestart from "react-native-restart";
// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomMap from "../../components/map/GooglePlacesInput";
import CustomInput from "../../components/customInput/CustomInput";
import CustomDate from "../../components/date/CustomDate";
import CustomTooltip from "../../components/tooltip/tooltip";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import {
  getPropertyType,
  getBuildingAmenities,
  getProfile,
  updateProfile,
} from "../../services/api";

// util

interface ProfileRegistrationScreenProps {}
const actionSheeYearBuilttRef = React.createRef();
const actionSheetUnitRef = React.createRef();

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  flexWrap: "wrap",
};
const FLEXROW2: TextStyle = {
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
};
const FLEXROWVERIFY: TextStyle = {
  width: "100%",
  alignItems: "flex-start",
  justifyContent: "flex-start",
  flexDirection: "row",
  flex: 1,
  marginBottom: 23,
};
const SETMARGIN: TextStyle = {
  marginRight: 10,
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: 240,
  marginRight: 10,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BTNSTYLE: TextStyle = {
  borderRadius: 5,
  backgroundColor: colors.dark2,
  justifyContent: "center",
  alignItems: "center",
  padding: 15,
  width: "100%",
  marginTop: 40,
};
const CHECKWRAP: TextStyle = {
  flex: 1,
  padding: 10,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "42%",
  width: "48%",
  height: 70,
  minWidth: "48%",
};
const CHECKWRAP3: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "48%",
  width: "45%",
  height: 70,
  minWidth: "40%",
};
const CHECKWRAP5: TextStyle = {
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  width: "43%",
  height: 120,
  minWidth: "40%",
  marginVertical: 10,
};
const CHECKWRAP2: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 15,
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  maxWidth: "100%",
  minWidth: "100%",
  height: 75,
  marginBottom: 10,
};
const LIKECHECKBOX: TextStyle = {
  flex: 1,
  borderWidth: 1,
  width: "97%",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 3,
};
const INTERIOCHECKER: TextStyle = {
  flex: 1,
  borderWidth: 0,
  width: "20%",
  maxWidth: "20%",
  justifyContent: "center",
  alignItems: "center",
  maxHeight: "91%",
  minHeight: "91%",
};
const INPUTWRAP2: ViewStyle = {
  width: "100%",
  paddingHorizontal: 15,
  marginBottom: 20,
};
const RNCHECKBOXSTYLE: ViewStyle = {
  height: 5,
  width: 10,
  borderRadius: 10 / 2,
};
const SETINPUTSTYLE: TextStyle = {
  marginBottom: 10,
};

const ProfileRegistration = ({ navigation, route }) => {
  const { userProfileDetails } = route.params;
  console.warn(userProfileDetails, "property data");
  const navigations = useNavigation();
  const [stepValue, setStepValue] = useState("Step 1");
  const [enteredlocation, setEnteredlocation] = useState("");
  const [listingTitle, setListingTitle] = useState("");
  const [priceTitle, setPriceTitle] = useState("");
  const [describedProperty, setDescribedProperty] = useState("");
  const [SquareMeter, setSquareMeter] = useState("");
  const [listingprice, setListingprice] = useState(0);
  const [serviceCharge, setServiceCharge] = useState(0);
  const [userFullName, setFullName] = useState("");
  const [userEmailAddress, setEmailAddress] = useState("");
  const [userPhoneNumber, setPhoneNumber] = useState("");
  const [addressValue, setAddressValue] = useState("");
  const [interiorBedroomSelect, setInteriorBedroomSelect] = useState(0);
  const [interiorBathroomSelect, setInteriorBathroomSelect] = useState(0);
  const [interiorKitchenSelect, setInteriorKitchenSelect] = useState(0);
  const [yearSelect, setYearSelect] = useState("");
  const [unitSelect, setUnitSelect] = useState();
  const [selectPropertyType, setSelectPropertyType] = useState("");
  const [profileFound, setProfileFound] = useState(null);
  const [buildingAmenities, setBuildingAmenities] = useState(null);
  const [appliancesAndFinishings, setAppliancesAndFinishings] = useState(null);
  const [propTypeSelected, setPropTypeSelected] = useState(null);
  const [buildingAmenitiesSelected, setBuildingAmenitiesSelected] = useState(
    []
  );
  const [appliancesAndFinishingsSelected, setAppliancesAndFinishingsSelected] =
    useState([]);
  const [loadingState, setLoadingState] = useState(false);
  const [imageArray, setImageArray] = useState("");
  const [loopedImageArray, setLoopedImageArray] = useState(null);
  const [fileArray, setFileArray] = useState([]);
  const [uploaderFileName, setFileName] = useState("");
  const [userSelectedDate, setUserSelectedDate] = useState("DD/MM/YY");
  const [userSelectedTime, setUserSelectedTime] = useState("00:00");
  const [updateText, setUpateText] = useState("Update");
  const [value, setValue] = useState("");
  const [valid, setValid] = useState(false);
  const [showMessage, setShowMessage] = useState(false);

  const [show, setShow] = useState(false);
  const mode = "dark";

  const ref = useRef();

  const removeImage = (e) => {
    let array = imageArray; // make a separate copy of the array
    let index = array.indexOf(e);
    if (index !== -1) {
      array.splice(index, 1);
      setImageArray(array);
    }
  };
  const phoneInput = useRef<PhoneInput>(null);

  useEffect(() => {
    async function fetUserProperty() {
      setLoadingState(true);

      //setLoadingState(true);
      // <Myspiner state={true} />;
      try {
        const result = await getProfile();
        const { kind, data } = result;

        if (kind === "ok") {
          // console.warn(data?.data);
          setProfileFound(data?.data);
          console.warn(profileFound, "ssssss");

          setLoadingState(false);
        }
      } catch ({ message }) {
        setLoadingState(false);
        ShowNotification(message);
      }
    }
    fetUserProperty();
  }, []);

  const processEditProfile = async () => {
    // if (emailOrUsername === "") {
    //   ShowNotification("Email is  required");
    //   return;
    // }
    // if (password === "") {
    //   ShowNotification("Pls enter your password");
    //   return;
    // }

    let payload = {
      fullname: userFullName || userProfileDetails?.usersProfile?.fullname,
      avatar: imageArray || userProfileDetails?.usersProfile?.avatar,
      phoneNumber:
        userPhoneNumber || userProfileDetails?.usersProfile?.phoneNumber,
    };
    console.warn(payload);
    setUpateText("Processing...");

    try {
      const result = await updateProfile(payload);
      const { kind, data } = result;
      const loginData = data?.data;

      if (kind === "ok") {
        // async function fetUserProperty() {
        // setLoadingState(true);
        // <Myspiner state={true} />;
        try {
          const result = await getProfile();
          const { kind, data } = result;
          // const loginData = data?.data;
          // console.log(loginData)

          if (kind === "ok") {
            ShowNotification("Profile update successful", "success");
            setUpateText("Update");
            // setLoadingState(false);
            console.warn(data?.data, "diamond");

            PersistData("loggedinUserdetails", {profile: data?.data});
            Alert.alert('Profile update successful', 'A Refresh is Required', [
              {
                text: 'OK',
                onPress: () => {
                  RNRestart.Restart();
                }
              }
            ])
            // RNRestart.Restart();
            // navigation.navigate("Main", { screen: "MySettings" });
          }
        } catch ({ message }) {
          setLoadingState(false);
          ShowNotification(message, "danger");
        }
        // }
      } else {
        setUpateText("Update");
        const newMsg = data.message;
        ShowNotification(newMsg, "warning");
      }
    } catch ({ message }) {
      setUpateText("Update");
      ShowNotification(message, "danger");
    }
  };

  // props

  const _selectPhotoTapped = async () => {
    // const options = {
    //   quality: 1.0,
    //   maxWidth: 500,
    //   maxHeight: 500,
    //   storageOptions: {
    //     skipBackup: true
    //   },
    //   mediaType:'photo',
    //   // mediaType:'video',
    // };

    const options = {
      mediaType: "photo",
    };

    launchImageLibrary(options, async (response) => {
      console.warn("Response = ", response);

      if (response.didCancel) {
        console.warn("User cancelled photo picker");
      } else if (response.errorMessage) {
        console.warn("ImagePicker Error: ", response.errorMessage);
      } else {
        const imagedata = response?.assets;
        imagedata.map((item, index) => {
          console.warn(item?.fileSize, "sss");

          let fileSize = item?.fileSize / 1048576;

          let thissizeInMB = Math.round(fileSize);
          // alert(thissizeInMB)
          //                 if(thissizeInMB > parseInt(DashboardStore.MaxFileUpload.Genaral)){
          //                     // let acceptsize= DashboardStore.MaxFileUpload.Genaral/1024/1024;
          //                     let acceptsize= DashboardStore.MaxFileUpload.Genaral
          //                     ShowNotification(`File too large , file should not be more than ${acceptsize} MB`, true);

          // return;
          //                 }
          let Selected_image_localPath = { uri: item?.uri };
          ShowNotification("Please Wait, You would be Notified once done", "warning");

          //  this.setState({
          //    avatarSource: Selected_image_localPath,
          //    isImagefromdevice: true,
          //    showsaveBtn: true,
          //    // file2upload: source,
          //    uploading: true,
          //  });

          // this.cloudinaryUpload();

          const source = {
            uri: item?.uri,
            type: item?.type,
            name: item?.fileName,
          };
          //  this.setState({
          //    filename: response.fileName,
          //  });

          cloudinaryUpload(source, YOUR_CLOUDINARY_PRESET())
            .then((response) => {
              if (response !== false) {
                console.warn("FileUrl");
                console.warn(response);
                console.warn("FileUrl_done");
                setImageArray(response);
                // let links = [];
                // // let fileUrl = data.secure_url;
                // links.push(response);
                ShowNotification("Done Saving your File", "success");
              }
            })
            .catch((error) => {
              console.warn(error); // error is a javascript Error object
              ShowNotification(error, "danger");
            });
        });
      }
    });
  };
  let userNewImg =
    userProfileDetails?.usersProfile?.avatar ||
    "https://res.cloudinary.com/db2tci0wk/image/upload/v1644142243/profileImg_fyuqzt.png";
  const handlegeneralAction = (params, params2) => {
    params(params2);
  };
  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftIcon={true}
        title={"Profile details"}
        rightIcon
        onLeftPress={() => navigation.goBack()}
      />

      <ScrollView style={{ paddingVertical: 15, paddingHorizontal: 15 }}>
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <ImageBackground
              resizeMode="stretch"
              source={
                imageArray === "" ? { uri: userNewImg } : { uri: imageArray }
              }
              style={{
                width: 100,
                height: 100,
                borderRadius: 50,
                justifyContent: "center",
                alignItems: "center",
              }}
              imageStyle={{ borderRadius: 100 / 2 }}
            >
              <TouchableOpacity onPress={() => _selectPhotoTapped()}>
                <Image source={images.camera} />
              </TouchableOpacity>
            </ImageBackground>
            <CustomText
              textSize={16}
              textWeight={"normal"}
              textcolor={colors.black}
              displayText={"Select Image"}
              textStyle={{ marginTop: 6, marginBottom: 6, textAlign: "left" }}
            />

            <View style={INPUTWRAP2}>
              <CustomInput
                inputText={"Full name"}
                inputTextWeight={"normal"}
                inputTextSize={15}
                inputTextcolor={colors.black}
                inputTextSty={SETINPUTSTYLE}
                onChangeText={(value: number) =>
                  handlegeneralAction(setFullName, value)
                }
                placeHolderText={""}
                showIcon={false}
                defaultValue={userProfileDetails?.usersProfile?.fullname}
              />
            </View>
            <View style={INPUTWRAP2}>
              <CustomInput
                inputText={"Email address"}
                inputTextWeight={"normal"}
                inputTextSize={15}
                inputTextcolor={colors.black}
                inputTextSty={SETINPUTSTYLE}
                onChangeText={(value: number) =>
                  handlegeneralAction(setEmailAddress, value)
                }
                placeHolderText={""}
                showIcon={false}
                defaultValue={userProfileDetails?.username}
              />
            </View>

            <View style={INPUTWRAP2}>
              <ImageBackground
                resizeMode="stretch"
                source={images.inputCard}
                style={{
                  width: "100%",
                  height: 64,
                  justifyContent: "center",
                  alignItems: "center",
                }}
                // style={(this, this.props.inputSty || styles.GeneralInputSty)}
              >
                <PhoneInput
                  ref={phoneInput}
                  defaultValue={userProfileDetails?.usersProfile?.phoneNumber}
                  defaultCode="NG"
                  onChangeFormattedText={(text) => {
                    handlegeneralAction(setPhoneNumber, text);
                    // setValue(text);
                  }}
                  textInputStyle={{
                    height: 20,
                    backgroundColor: "transparent",
                  }}
                  codeTextStyle={{
                    // height: 20,
                    backgroundColor: "transparent",
                  }}
                  containerStyle={{
                    // height: 20,
                    backgroundColor: "transparent",
                  }}
                  textContainerStyle={{
                    // height: 20,
                    backgroundColor: "transparent",
                  }}
                  withDarkTheme
                  withShadow
                  autoFocus
                />
                {/* <CustomInput
                inputText={"Edit your phone number"}
                inputTextWeight={"normal"}
                inputTextSize={15}
                inputTextcolor={colors.black}
                inputTextSty={SETINPUTSTYLE}
                onChangeText={(value: number) =>
                  handlegeneralAction(setPhoneNumber, value)
                }
                placeHolderText={""}
                showIcon={false}
                defaultValue={userProfileDetails?.usersProfile?.phoneNumber}
              /> */}
              </ImageBackground>
            </View>

            <View style={[{ paddingHorizontal: 20, marginBottom: 22 }]}>
              <TouchableOpacity
                disabled={updateText === "Update" ? false : true}
                style={BTNSTYLE}
                onPress={() => processEditProfile()}
              >
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.white}
                  displayText={updateText}
                  textStyle={{ marginBottom: 6, textAlign: "center" }}
                />
              </TouchableOpacity>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
    </View>
  );
};

export default ProfileRegistration;
