// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  CheckBox,
  ActivityIndicator,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import MapView from "react-native-maps";
import {
  PersistLoginRecord,
  GetPersistData,
  myspiner as Myspiner,
  SkeletonLoader,
  ShowNotification,
} from "../../utils/UtilityFunctions";
// redux
import { ApplicationState } from "../../redux";
import firestore from '@react-native-firebase/firestore';

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomInput from "../../components/customInput/CustomInput";
import CustomDate from "../../components/date/CustomDate";
import CustomTooltip from "../../components/tooltip/tooltip";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { getMessage } from "../../services/api";
import ThankYou from "../thankYou";
// util
import moment from "moment";

interface MessagesListingScreenProps {}
const actionSheeYearBuilttRef = React.createRef();
const actionSheetUnitRef = React.createRef();

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};
const MAKEHEADER: ViewStyle = {
  width: "100%",
  height: 200,
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: colors.dark2,
};
const FLOATBERDGE: ViewStyle = {
  width: 10,
  height: 10,
  borderRadius: 10 / 2,
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: colors.dark2,
  position: "absolute",
  right: 5,
  bottom: 25,
  zIndex: 9,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  paddingVertical: 10,
};
const FLEXROW2: TextStyle = {
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
};
const FLEXROWVERIFY: TextStyle = {
  width: "100%",
  alignItems: "flex-start",
  justifyContent: "flex-start",
  flexDirection: "row",
  flex: 1,
  marginBottom: 23,
};
const SETMARGIN: TextStyle = {
  marginRight: 10,
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: 240,
  marginRight: 10,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BTNSTYLE: TextStyle = {
  borderRadius: 5,
  backgroundColor: colors.dark2,
  justifyContent: "center",
  alignItems: "center",
  padding: 15,
  width: "100%",
  marginTop: 40,
};
const CHECKWRAP: TextStyle = {
  flex: 1,
  padding: 10,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "42%",
  width: "45%",
  height: 70,
  minWidth: "40%",
};
const CHECKWRAP3: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "48%",
  width: "45%",
  height: 70,
  minWidth: "40%",
};
const CHECKWRAP5: TextStyle = {
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  width: "43%",
  height: 120,
  minWidth: "40%",
  marginVertical: 10,
};
const CHECKWRAP2: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 15,
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  maxWidth: "100%",
  minWidth: "100%",
  height: 75,
  marginBottom: 10,
};
const LIKECHECKBOX: TextStyle = {
  width: "100%",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  borderBottomColor: colors.lightDark,
  borderBottomWidth: 0.5,
  paddingHorizontal: 10,
};
const INTERIOCHECKER: TextStyle = {
  flex: 1,
  borderWidth: 0,
  width: "20%",
  maxWidth: "20%",
  justifyContent: "center",
  alignItems: "center",
  maxHeight: "91%",
  minHeight: "91%",
};
const INPUTWRAP2: ViewStyle = {
  width: "100%",
  paddingHorizontal: 15,
  marginBottom: 20,
};
const RNCHECKBOXSTYLE: ViewStyle = {
  height: 5,
  width: 10,
  borderRadius: 10 / 2,
};
const SETINPUTSTYLE: TextStyle = {
  marginBottom: 10,
};

const MessagesListing = ({ navigation, route }) => {
  const navigations = useNavigation();
  const [stepValue, setStepValue] = useState("Step 3");
  const [show, setShow] = useState(false);
  const [messageFetched, setMessageFetched] = useState([]);
  const [loadingState, setLoadingState] = useState(true);
  const [isloagin, setisloagin] = useState(false);
  const [loginState, setLoginState] = useState(false);
  const [userDataGotten, setUserDataGotten] = useState([]);

  const mode = "dark";
  const handleYearBuilt = () => {
    actionSheeYearBuilttRef?.current?.show();
  };
  const handleUnit = () => {
    actionSheetUnitRef?.current?.show();
  };

  useEffect(() => {
    fetAllUserdata();
    GetUserLogin();
  }, []);

  const userDataRef_ = useRef(null);

  async function fetAllUserdata() {
    let userData = await GetPersistData("loggedinUserdetails");
    const newdata = JSON.parse(userData);
    console.warn(newdata, "i will be fine");

    setUserDataGotten(newdata?.profile);
    userDataRef_.current = newdata.profile;
  }

  const fetchFirebaseChat = async ()=>{
    var my_email = userDataRef_.current.username;
    console.log(my_email)

    await firestore().collection('chat_session').where("userEmailData", "array-contains", my_email).orderBy('timeStampUpdated', 'desc').onSnapshot(async (snapshot)=>{
      // var r = snapshot.docs.map(doc =>{
      //     return (doc.data())
      // });

      var r = [];

      for(var i = 0 ; i < (snapshot.docs).length ; i++ ){
        var array_data = (snapshot.docs)[i].data();

        var emails = array_data.userEmailData;

        var index = 0;
        for(var pp = 0 ; pp < emails ; pp++){
          if(my_email != emails[i]){
            index = i;
          }
        }

        var other_email = emails[index];
        var avatar = '';
        var name = '';
        var token = '';

        var snapshote = await firestore().collection('device_token').doc(other_email).get();
        // console.log('Docs: ', snapshote.data())
        let data = snapshote.data();
        if(data == undefined){
          
        }else{
          avatar = data.avatar;
          name = data.name;
          token = data.token;
        }

        r[i] = {
          data: array_data,
          my_email: my_email,
          other_email: other_email,
          avatar: avatar,
          name: name,
          token: token,
        };
      }

      // Alert.alert(r.length)
      console.log(r)
      setMessageFetched(r)
      setLoadingState(false);
    }, error=>{
      console.log(error)
    });
  }

  async function fetchMsg() {
    setLoadingState(true);
    // <Myspiner state={true} />;
    try {
      const result = await getMessage();
      const { kind, data } = result;
      console.log("message data : ", data, kind);
      // console.warn(data, "get viewed property");
      // message;
      // console.warn(kind, "kindkindkinds");
      if (kind === "ok") {
        const dd = data.data;

        console.warn("Loaded Message = ", dd);

        setMessageFetched(data.data);
        setLoadingState(false);
      }
    } catch ({ message }) {
      // console.warn(message, "heyeyyeye");
      // ShowNotification(message);
    }
  }

  const GetUserLogin = async () => {
    try {
      let userToken = await GetPersistData("Loggedinstate");
      let loggedinUserdetails = await GetPersistData("loggedinUserdetails");
      loggedinUserdetails = JSON.parse(loggedinUserdetails);

      console.log(loggedinUserdetails.profile, "loggedinUserdetails.profile ");
      // setLoginProfile(loggedinUserdetails?.profile);

      let ss = JSON.parse(userToken);
      console.log(ss, "login stateeee");
      if (ss?.isloggedin === true) {
        setLoginState(ss?.isloggedin);
        // setLoginState(false);
        await fetchFirebaseChat();
        // fetchMsg();
      } else {
        setLoginState(false);
      }
      console.log(ss, "login details ");
    } catch (err) {}
  };

  // props

  const MessagesList = [
    {
      label: "Swimming pool",
      value: "Swimming pool",
    },
    {
      label: "Gym/fitness center",
      value: "Gym/fitness center",
    },
    {
      label: "Children playground",
      value: "Children playground",
    },
    {
      label: "Parking lot",
      value: "Parking lot",
    },
    {
      label: "Mall",
      value: "Mall",
    },
    {
      label: "Hospital",
      value: "Hospital",
    },
    {
      label: "School",
      value: "School",
    },
    {
      label: "Business Center",
      value: "Business Center",
    },
    {
      label: "Electricity",
      value: "Electricity",
    },

    {
      label: "Palto/balcony",
      value: "Palto/balcony",
    },
    {
      label: "Allowance of pets",
      value: "Allowance of pets",
    },
    {
      label: "CCTV Camera",
      value: "CCTV Camera",
    },
  ];
  const attashedImage = [
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726675/samples/Rectangle_uzu3mk.png",
    },
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726676/samples/Rectangle-1_ivkege.png",
    },
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726676/samples/Rectangle-2_glpu9u.png",
    },
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726676/samples/Rectangle-3_lwa9z5.png",
    },
  ];

  let LoopedMessagesList = messageFetched.map((item, index) => {
    // let text1 = item.createdAt;
    // const splitCreatedAt = text1.split("T");
    let text1 = new Date(item.data.timeStampCreated).toUTCString();

    console.warn(item, "neww");

    return (
      <TouchableOpacity
        style={[LIKECHECKBOX, { borderColor: colors.transparent }]}
        onPress={() => {
          navigation.navigate("Main", {
            screen: "MessageView",
            params: {
              foundConversationId: index,
              foundConversation: item,
            },
          });
        }}
      >
        <View style={FLEXROW}>
          <View style={{ position: "relative" }}>
            {
              //Remove badge untill functionality is clear
              /* <View style={FLOATBERDGE}>
              <CustomText
                textSize={10}
                textWeight={"normal"}
                textcolor={colors.white}
                displayText={"1"}
                textStyle={{ textAlign: "center" }}
              />
            </View> */
            }
            <Image
              source={{
                uri: item?.senderProfile?.avatar,
              }}
              style={{
                width: 50,
                height: 50,
                borderRadius: 50 / 2,
                marginRight: 10,
              }}
            />
          </View>
          <View>
            <View style={FLEXROW}>
              <CustomText
                textSize={13}
                textWeight={"normal"}
                textcolor={colors.black}
                displayText={item?.data.propertyInfo[1]}
                textStyle={{
                  lineHeight: 18,
                }}
              />
              <Text> </Text>
              <CustomText
                textSize={11}
                textWeight={"normal"}
                textcolor={colors.lightDark}
                displayText={moment(text1).utc().format("DD/MM/YYYY HH:mm")}
                textStyle={{
                  lineHeight: 18,
                  textAlign: "center",
                }}
              />
            </View>
            <CustomText
              textSize={11}
              textWeight={"normal"}
              textcolor={colors.lightDark}
              displayText={`
              Description: 
              ${item?.data.propertyInfo[2]}`}
              textStyle={{
                lineHeight: 18,
              }}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  });
  const BgColor = "#001D38";

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      {loginState ? (
        <React.Fragment>
          <View style={MAKEHEADER}>
            <CustomText
              textSize={20}
              textWeight={"bold"}
              textcolor={colors.white2}
              displayText={"Messages"}
              textStyle={{ textAlign: "center" }}
            />
          </View>
          <ScrollView style={{ paddingVertical: 15 }}>
            <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
              <Animatable.View animation={"slideInUp"}>
                {loadingState ? (
                  <ActivityIndicator size="large" color={BgColor} />
                ) : (
                  LoopedMessagesList
                )}
              </Animatable.View>
            </Animatable.View>
          </ScrollView>
        </React.Fragment>
      ) : (
        <ThankYou />
      )}
    </View>
  );
};

export default MessagesListing;
