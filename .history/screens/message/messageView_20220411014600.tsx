// react
import * as React from "react";
import { useEffect, useRef, useState, useCallback } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  CheckBox,
  ActivityIndicator,
} from "react-native";
import { GiftedChat, Bubble } from "react-native-gifted-chat";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import MapView from "react-native-maps";
import {
  PersistLoginRecord,
  GetPersistData,
  myspiner as Myspiner,
  SkeletonLoader,
  ShowNotification,
} from "../../utils/UtilityFunctions";
// redux
import { ApplicationState } from "../../redux";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomInput from "../../components/customInput/CustomInput";
import CustomDate from "../../components/date/CustomDate";
import CustomTooltip from "../../components/tooltip/tooltip";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { getMessage } from "../../services/api";
import ThankYou from "../thankYou";
import { getchatHistory, replyMessage } from "../../services/api";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Messages from "react-native-firebase/messaging";
// util

interface MessageViewScreenProps {}
const actionSheeYearBuilttRef = React.createRef();
const actionSheetUnitRef = React.createRef();

const ROOT: ViewStyle = {
  // height: Layout.window.height,
  minHeight: "96%",
  marginTop: 30,
  width: Layout.window.width,
  backgroundColor: colors.alerzoBlue,
};
const ELEMENTWRAP: ViewStyle = {
  flexDirection: "row",
  // padding: 20,
  alignItems: "center",
};
const ELEMENTWRAP2: ViewStyle = {
  flexDirection: "row",
  // justifyContent: "center",
  alignItems: "center",
};

const MessageView = (props: {
  navigation: any;
  route: any;
  hideHeader: boolean;
}) => {
  const { foundConversationId } = props.route.params;
  const { foundConversation } = props.route.params;

  console.warn(foundConversation, "shoesss");

  const navigations = useNavigation();
  const [stepValue, setStepValue] = useState("Step 3");
  const [show, setShow] = useState(false);
  const [messageFetched, setMessageFetched] = useState([]);
  const [loadingState, setLoadingState] = useState(false);
  const [isloagin, setisloagin] = useState(false);
  const [loginState, setLoginState] = useState(false);
  const [userDataGotten, setUserDataGotten] = useState({});
  const [messages, setMessages] = useState([]);
  const mode = "dark";

  useEffect(() => {
    fetAllUserdata();
  }, []);
  // useEffect(() => {
  //   const interval = setInterval(() => {
  //     if (!userDataGotten?.id) return;
  //     GetUserConversation();
  //   }, 10000);
  //   return () => clearInterval(interval);
  // }, [userDataGotten?.id]);

  async function fetAllUserdata() {
    let userData = await GetPersistData("loggedinUserdetails");
    const newdata = JSON.parse(userData);
    // console.warn(newdata, "i will be fine");

    setUserDataGotten(newdata?.profile);
  }

  console.log("user message", userDataGotten);
  // console.log("user message", messages);

  const GetUserConversation = async () => {
    try {
      setLoadingState(true);
      let result;
      result = await getchatHistory(foundConversationId);
      console.warn(result, "resultresult");
      const { kind, data } = result;
      //  console.warn(data.data, "first page");
      // setLoadingState(false);

      if (kind === "ok") {
        let newMsg = (data.data || []).map((chatItems, index) => {
          return {
            _id: index,

            text: chatItems?.messageBody,
            createdAt: chatItems?.createdAt,
            user: {
              // _id:
              //   userDataGotten?.id === chatItems?.senderId
              //     ? chatItems?.receiverId
              //     : chatItems?.senderId,
              _id:
                userDataGotten?.id === chatItems?.senderId
                  ? chatItems?.senderId
                  : chatItems?.receiverId,

              // name:
              //   userDataGotten?.id === chatItems?.senderId
              //     ? chatItems.senderProfile.fullname
              //     : chatItems.receiverProfile.fullname,
              // avatar:
              //   userDataGotten?.id === chatItems?.senderId
              //     ? chatItems.senderProfile.avater
              //     : chatItems.receiverProfile.avater,
            },
          };
        });
        //    console.warn(newMsg, "ffgfg");

        setMessages(newMsg);
        setLoadingState(false);

        // setAllpropertiesdata(data.data);
      }
    } catch ({ message }) {
      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
      setLoadingState(false);
    }
  };
  // console.warn(messages, "lolo");
  const giveReply = async (value) => {
    setLoadingState(true);
    try {
      let result;
      const params = {
        conversationId: foundConversationId,
        message: value,
      };
      result = await replyMessage(params);
      console.warn(result, "reply");
      const { kind, data } = result;
      // console.warn(data.data, "first page");
      setLoadingState(false);

      if (kind === "ok") {
        // ShowNotification(data.message, "success");
      }
    } catch ({ message }) {
      ShowNotification(message, "danger");
      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };

  const onSend = useCallback((messages = []) => {
    // console.warn(messages[0]?.text, "chilrdreen");
    // console.log("new msg :", messages);
    const msg = messages[0]?.text;
    giveReply(msg);
    setMessages((previousMessages) =>
      GiftedChat.append(previousMessages, messages)
    );
  }, []);

  const BgColor = "#001D38";

  const renderActions = (props) => {
    return (
      <View style={{ margin: 10 }}>
        <Icon name="attachment" size={24} />
      </View>
    );
  };

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <View
        style={{
          width: "100%",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          padding: 20,
        }}
      >
        <View style={[ELEMENTWRAP]}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Image source={images.arrBack} />
          </TouchableOpacity>

          <View style={[ELEMENTWRAP2]}>
            <View>
              <ImageBackground
                resizeMode="stretch"
                source={images.profileImg}
                style={{
                  width: 61,
                  height: 61,
                  borderRadius: 61 / 2,
                  marginRight: 10,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Image
                  source={{
                    uri: ((foundConversation.avatar).length > 2 ) ? foundConversation.avatar:'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.zRG7_6cFjh5TdxTbdW_SkgHaH_%26pid%3DApi&f=1',
                  }}
                  // source={{uri:foundConversation?.receiverProfile?.avatar }}
                  style={{
                    width: 55,
                    height: 55,
                    padding: 3 ,
                    borderRadius: 55 / 2,
                  }}
                />
              </ImageBackground>
            </View>
            <View>
              <Text>
                {foundConversation.name}
              </Text>
              {/* <Text>{foundConversation?.receiverProfile?.phoneNumber}</Text> */}
            </View>
          </View>
        </View>
        <TouchableOpacity>
          {/* <Image source={images.call2} /> */}
        </TouchableOpacity>
      </View>
      {/* {loadingState ? (
          <ActivityIndicator size="large" color={BgColor} />
      ) : ( */}
      {/* eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxNiwiZW1haWxBZGRyZXNzIjoiYWJpbmFzaHBodWxrb253YXJ0ZXN0OTVAZ21haWwuY29tIiwiaWF0IjoxNjQ4OTcxNjY0LCJleHAiOjE2NTUwMTk2NjR9.goZw5W_axzVljsaGsLJNEZKBJY9KnaeL98LPK-cDyyQ 
       
       dh777Zr_SM6VJAoOjsIHw9:APA91bE1nVnmj1bDvrOrw6MQ6UZDY4rQD_yKsiHyv3GMq70xcV3Ol1Gqj7pKKw7Q1Vj16m23rb7BIChGICxlVc3cenE7rld-CBSEk89tbCnsUDwhQtgV1ZU1Dsqb_fRPV2qj6pKrrPg5
       */}
      {userDataGotten?.id && (
        <GiftedChat
          // inverted={false}
          messages={messages}
          // isLoadingEarlier={true}
          renderLoading={() => (
            <ActivityIndicator size="large" color={BgColor} />
          )}
          onSend={(messages) => onSend(messages)}
          //   onSend={(messages) => console.log(messages)}
          user={{
            _id: userDataGotten?.id, //login user
          }}
          placeholder={"Message"}
          renderActions={() => renderActions()}
          renderAvatar={() => null}
          renderBubble={(props) => {
            return (
              <Bubble
                {...props}
                textStyle={{
                  right: {
                    color: "white",
                  },
                  left: { color: "black" },
                }}
                timeTextStyle={{
                  right: { color: "white" },
                  left: { color: "black" },
                }}
                wrapperStyle={{
                  left: {
                    backgroundColor: "#C4C4C4",
                  },
                  right: {
                    backgroundColor: "#001D38",
                  },
                }}
              />
            );
          }}
          textInputStyle={{
            backgroundColor: "#EAEAEA",
            margin: 2,
            marginTop: 6,
            padding: 8,
            paddingTop: 10,
            borderRadius: 15,
          }}
          // quickReplyStyle={{backgroundColor: "red"}}
          // renderAvatar={renderAvatar}
        />
      )}
    </View>
  );
};

export default MessageView;
