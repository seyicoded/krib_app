// react
import * as React from "react";
import { useEffect, useRef, useState, useCallback } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  CheckBox,
  ActivityIndicator,
} from "react-native";
import { GiftedChat, Bubble } from "react-native-gifted-chat";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import MapView from "react-native-maps";
import {
  PersistLoginRecord,
  GetPersistData,
  myspiner as Myspiner,
  SkeletonLoader,
  ShowNotification,
  cloudinaryUpload,
  YOUR_CLOUDINARY_PRESET,
} from "../../utils/UtilityFunctions";

import { launchCamera, launchImageLibrary } from "react-native-image-picker";

// redux
import { ApplicationState } from "../../redux";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomInput from "../../components/customInput/CustomInput";
import CustomDate from "../../components/date/CustomDate";
import CustomTooltip from "../../components/tooltip/tooltip";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { getMessage } from "../../services/api";
import ThankYou from "../thankYou";
import { getchatHistory, replyMessage } from "../../services/api";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Messages from "react-native-firebase/messaging";
import firestore from '@react-native-firebase/firestore';
import axios from 'axios'
// util

interface MessageViewScreenProps {}
const actionSheeYearBuilttRef = React.createRef();
const actionSheetUnitRef = React.createRef();

const ROOT: ViewStyle = {
  // height: Layout.window.height,
  minHeight: "96%",
  marginTop: 30,
  width: Layout.window.width,
  backgroundColor: colors.alerzoBlue,
};
const ELEMENTWRAP: ViewStyle = {
  flexDirection: "row",
  // padding: 20,
  alignItems: "center",
};
const ELEMENTWRAP2: ViewStyle = {
  flexDirection: "row",
  // justifyContent: "center",
  alignItems: "center",
};

const MessageView = (props: {
  navigation: any;
  route: any;
  hideHeader: boolean;
}) => {
  const { foundConversationId } = props.route.params;
  const { foundConversation } = props.route.params;

  console.warn(foundConversation, "shoesss");

  const navigations = useNavigation();
  const [stepValue, setStepValue] = useState("Step 3");
  const [show, setShow] = useState(false);
  const [messageFetched, setMessageFetched] = useState([]);
  const [loadingState, setLoadingState] = useState(false);
  const [isloagin, setisloagin] = useState(false);
  const [loginState, setLoginState] = useState(false);
  const [userDataGotten, setUserDataGotten] = useState({});
  const [messages, setMessages] = useState([
    {
      _id: 1,
      text: 'My message',
      createdAt: new Date(Date.UTC(2016, 5, 11, 17, 20, 0)),
      user: {
        _id: 1,
        name: 'React Native',
        avatar: 'https://facebook.github.io/react/img/logo_og.png',
      },
      image: '',
      // Mark the message as sent, using one tick
      sent: true,
      // Mark the message as received, using two tick
      received: true,
      // Mark the message as pending with a clock loader
    },
    {
      _id: 2,
      text: 'My message',
      createdAt: new Date(Date.UTC(2016, 5, 11, 17, 20, 0)),
      user: {
        _id: 'opadonuseyi01@gmail.com',
        name: 'React Native',
        avatar: 'https://facebook.github.io/react/img/logo_og.png',
      },
      image: '',
      // Mark the message as sent, using one tick
      sent: true,
    },
  ]);
  const [New_messages, setNew_Messages] = useState([]);
  const [attaching, setAttaching] = useState(false)
  const [attachment, setattachment] = useState('')
  const [attachments, setattachments] = useState([])
  const [attachmenttype, setattachmenttype] = useState('')
  const [attachmentsize, setattachmentsize] = useState()
  const [error, seterror] = useState()
  const [selected, setSelected] = useState()
  const [sendingNow, setsendingNow] = useState(false)
  const [sendingNowAA, setsendingNowAA] = useState(false)
  const [message, setmessage] = useState('')
  const [loadingChat, setloadingChat] = useState(false)
  const mode = "dark";

  // console.log(New_messages)

  const userDataRef = useRef(null);

  useEffect(() => {
    fetAllUserdata();
    loadChatAndPlaceListener()
  }, []);
  // useEffect(() => {
  //   const interval = setInterval(() => {
  //     if (!userDataGotten?.id) return;
  //     GetUserConversation();
  //   }, 10000);
  //   return () => clearInterval(interval);
  // }, [userDataGotten?.id]);

  async function fetAllUserdata() {
    let userData = await GetPersistData("loggedinUserdetails");
    const newdata = JSON.parse(userData);
    // console.warn(newdata, "i will be fine");

    setUserDataGotten(newdata?.profile);
    // console.log(newdata?.profile)
    userDataRef.current = newdata?.profile;
  }

  console.log("user message", userDataGotten);
  // console.log("user message", messages);

  const GetUserConversation = async () => {
    try {
      setLoadingState(true);
      let result;
      result = await getchatHistory(foundConversationId);
      // console.warn(result, "resultresult");
      const { kind, data } = result;
      //  console.warn(data.data, "first page");
      // setLoadingState(false);

      if (kind === "ok") {
        let newMsg = (data.data || []).map((chatItems, index) => {
          return {
            _id: index,

            text: chatItems?.messageBody,
            createdAt: chatItems?.createdAt,
            user: {
              // _id:
              //   userDataGotten?.id === chatItems?.senderId
              //     ? chatItems?.receiverId
              //     : chatItems?.senderId,
              _id:
                userDataGotten?.id === chatItems?.senderId
                  ? chatItems?.senderId
                  : chatItems?.receiverId,

              // name:
              //   userDataGotten?.id === chatItems?.senderId
              //     ? chatItems.senderProfile.fullname
              //     : chatItems.receiverProfile.fullname,
              // avatar:
              //   userDataGotten?.id === chatItems?.senderId
              //     ? chatItems.senderProfile.avater
              //     : chatItems.receiverProfile.avater,
            },
          };
        });
        //    console.warn(newMsg, "ffgfg");

        setMessages(newMsg);
        setLoadingState(false);

        // setAllpropertiesdata(data.data);
      }
    } catch ({ message }) {
      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
      setLoadingState(false);
    }
  };
  // console.warn(messages, "lolo");
  const giveReply = async (value) => {
    setLoadingState(true);
    try {
      let result;
      const params = {
        conversationId: foundConversationId,
        message: value,
      };
      result = await replyMessage(params);
      // console.warn(result, "reply");
      const { kind, data } = result;
      // console.warn(data.data, "first page");
      setLoadingState(false);

      if (kind === "ok") {
        // ShowNotification(data.message, "success");
      }
    } catch ({ message }) {
      ShowNotification(message, "danger");
      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };

  const onSend = useCallback((messages = []) => {
    // console.warn(messages[0]?.text, "chilrdreen");
    // console.log("new msg :", messages);
    const msg = messages[0]?.text;
    giveReply(msg);
    setMessages((previousMessages) =>
      GiftedChat.append(previousMessages, messages)
    );
  }, []);

  const loadChatAndPlaceListener = async ()=>{
    setloadingChat(true)

    // start
    firestore().collection('chat_session').doc(foundConversationId).onSnapshot(snapshot=>{
      var r = snapshot.data();
      console.log('arrayused11')
      
      if(r == undefined){
        setloadingChat(false)
        return
      }
      
      // sort variable array by timestamp
      for(var i = 0 ; i < r.message.length ; i++){
        for(var j = i; j > 0 ; j--){

          // check
          if( r.message[j].timeStamp > r.message[j - 1].timeStamp ){
            // swap
            let temp = r.message[j - 1];
            r.message[j - 1] = r.message[j];
            r.message[j] = temp;
          }
        }
      }
      // console.log(r)

      // organise into formate
      var arrayused = [];

      for(var i = 0 ; i < r.message.length ; i++){
        arrayused[i] = {
          _id: i,
          text: r.message[i].message,
          image: r.message[i].attachment,
          sent:true,
          createdAt: new Date(r.message[i].timeStamp),
          user: {
            _id: r.message[i].sender
          }
        };
      }

      console.log(arrayused)

      console.log('arrayused')
      setNew_Messages(arrayused)
      setloadingChat(false)
      
    }, error=>{
      console.log(error)
    });
    // end
  }

  const loadImageAttachment = async ()=>{
    // start
    const options = {
      mediaType: "photo",
    };

    launchImageLibrary(options, async (response) => {
      console.warn("Response = ", response);

      if (response.didCancel) {
        console.warn("User cancelled photo picker");
      } else if (response.errorMessage) {
        console.warn("ImagePicker Error: ", response.errorMessage);
      } else {
        const imagedata = response?.assets;
        imagedata.map((item, index) => {
          console.warn(item?.fileSize, "sss");

          let fileSize = item?.fileSize / 1048576;

          let thissizeInMB = Math.round(fileSize);
          // alert(thissizeInMB)
          //                 if(thissizeInMB > parseInt(DashboardStore.MaxFileUpload.Genaral)){
          //                     // let acceptsize= DashboardStore.MaxFileUpload.Genaral/1024/1024;
          //                     let acceptsize= DashboardStore.MaxFileUpload.Genaral
          //                     ShowNotification(`File too large , file should not be more than ${acceptsize} MB`, true);

          // return;
          //                 }
          let Selected_image_localPath = { uri: item?.uri };
          ShowNotification("Please Wait", "warning");

          //  this.setState({
          //    avatarSource: Selected_image_localPath,
          //    isImagefromdevice: true,
          //    showsaveBtn: true,
          //    // file2upload: source,
          //    uploading: true,
          //  });

          // this.cloudinaryUpload();

          const source = {
            uri: item?.uri,
            type: item?.type,
            name: item?.fileName,
          };
          //  this.setState({
          //    filename: response.fileName,
          //  });

          cloudinaryUpload(source, YOUR_CLOUDINARY_PRESET())
            .then((response) => {
              if (response !== false) {
                console.warn("FileUrl");
                console.warn(response);
                console.warn("FileUrl_done");
                setattachment(response);
                setattachmenttype(item?.type)
                // let links = [];
                // // let fileUrl = data.secure_url;
                // links.push(response);
                ShowNotification("Done Saving your File, Please attach a message", "success");
              }
            })
            .catch((error) => {
              console.warn(error); // error is a javascript Error object
              ShowNotification(error, "danger");
            });
        });
      }
    });
    // end
  }

  const onSend2 = async (message)=>{
    // console.log(message[0].text)
    let url = ''; let type = '';

        var messag = message[0].text;
        if(messag.length == 0){
            messag = 'Media Attachment';
        }

        setsendingNow(true)

        if(false){
            type = attachment.type

            // adding
            // console.log(attachment)
            var path = getPlatformPath(attachment).value;
            var name = getFileName(attachment.name, path);
            // console.log(name+'--'+path);
            // return false;
            try{
                var blob = await uriToBlob(attachment.uri);
                let attached = await firebase.storage().ref(`new-attaches/${name}`).put(blob, {contentType: type})
                url = await firebase.storage().ref(`new-attaches`).child(name).getDownloadURL()
                console.log(url)
                // return false;
            }catch(e){
                console.log('*****')
                console.log(e)
            }
        }

        try{

            let email = foundConversation.my_email

            let sen = {
                message: messag.trim(),
                recipient: foundConversation.other_email,
                attachment: attachment,
                file_type: attachmenttype,
                sender: email,
                createDate: (new Date()).toISOString(),
                timeStamp: Date.now()
            };
            // let sent = await sendMessage(sen)
            // sendMessage(sen)

            try{
              setNew_Messages([
                {
                  _id: New_messages.length,
                  text: sen.message,
                  image: sen.attachment,
                  sent:false,
                  createdAt: sen.timeStamp,
                  user: {
                    _id: email
                  }
                }
              ].concat(New_messages));
            }catch(e){}

            // send to firebase
            await firestore().collection('chat_session').doc(foundConversationId).update({message: firestore.FieldValue.arrayUnion(sen)});

            // send push notification
            axios.post('https://fcm.googleapis.com/fcm/send', {
              "to": foundConversation.token,
              "notification": {
                "title": "New Message",
                "body": 'New Message From '+userDataRef.current.usersProfile.fullname,
                "subtitle": messag.trim(),
              },
              "priority": "high"
            }, {
              headers: {
                Authorization : `key=AAAAwt9BG18:APA91bGYMJXHfYkwkswCwVvzDJFsGRB6Ws-GC5Oj9W6owGMtgNZxOWIv_4t2chtX3_ZFve6k40XIx_WDpsJ17BFPmoIM8VaGQDynWtfs6g0YhZRsTbvokkzBFtONOwidwjYRF`
              }
            });

            if(true){
                setmessage('')
                setattachment(null)
                setloadingChat(false)
            }else{
                seterror('There was an error sending your message')
            }

        }catch(e){
            console.log('message sending failed: ', e)
        }

        setsendingNow(false)
  }

  const BgColor = "#001D38";

  const renderActions = ({loadImageAttachment}) => {
    return (
      <View style={{ margin: 10 }}>
        <Icon onPress={()=>loadImageAttachment()} name="attachment" size={24} />
      </View>
    );
  };

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <View
        style={{
          width: "100%",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          padding: 20,
        }}
      >
        <View style={[ELEMENTWRAP]}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Image source={images.arrBack} />
          </TouchableOpacity>

          <View style={[ELEMENTWRAP2]}>
            <View>
              <ImageBackground
                resizeMode="stretch"
                source={images.profileImg}
                style={{
                  width: 61,
                  height: 61,
                  borderRadius: 61 / 2,
                  marginRight: 10,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Image
                  source={{
                    uri: ((foundConversation.avatar).length > 2 ) ? foundConversation.avatar:'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.zRG7_6cFjh5TdxTbdW_SkgHaH_%26pid%3DApi&f=1',
                  }}
                  // source={{uri:foundConversation?.receiverProfile?.avatar }}
                  style={{
                    width: 55,
                    height: 55,
                    padding: 3 ,
                    borderRadius: 55 / 2,
                  }}
                />
              </ImageBackground>
            </View>
            <View>
              <Text>
                {foundConversation.name}
              </Text>
              {/* <Text>{foundConversation?.receiverProfile?.phoneNumber}</Text> */}
            </View>
          </View>
        </View>
        <TouchableOpacity>
          {/* <Image source={images.call2} /> */}
        </TouchableOpacity>
      </View>
      {/* {loadingState ? (
          <ActivityIndicator size="large" color={BgColor} />
      ) : ( */}
      {/* eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxNiwiZW1haWxBZGRyZXNzIjoiYWJpbmFzaHBodWxrb253YXJ0ZXN0OTVAZ21haWwuY29tIiwiaWF0IjoxNjQ4OTcxNjY0LCJleHAiOjE2NTUwMTk2NjR9.goZw5W_axzVljsaGsLJNEZKBJY9KnaeL98LPK-cDyyQ 
       
       dh777Zr_SM6VJAoOjsIHw9:APA91bE1nVnmj1bDvrOrw6MQ6UZDY4rQD_yKsiHyv3GMq70xcV3Ol1Gqj7pKKw7Q1Vj16m23rb7BIChGICxlVc3cenE7rld-CBSEk89tbCnsUDwhQtgV1ZU1Dsqb_fRPV2qj6pKrrPg5
       */}

       {
         sendingNow ? 
         <View style={{
           justifyContent: 'center',
           alignItems: 'center',
           position: 'absolute',
           top: '10%',
           left: '32%'
         }}>
           <Text style={{
             color: 'rgba(2550, 255, 250, 0.8)',
             backgroundColor: 'rgba(0, 0, 100, 0.2)',
             padding: 6,
             borderRadius: 8,
             overflow: 'hidden'
           }}>sending, please wait</Text>
         </View>
         : <></>
       }
       {
        (loadingChat && 
        <View style={{
          justifyContent: 'center',
          alignItems: 'center',
          position: 'absolute',
          top: '10%',
          left: '37%'
        }}>
          <ActivityIndicator color={'lightblue'} size={'large'}/>
        </View>)
       }
      {userDataGotten?.id && (
        <GiftedChat
          // inverted={false}
          messages={New_messages}
          // isLoadingEarlier={true}
          renderLoading={() => (
            <ActivityIndicator size="large" color={BgColor} />
          )}
          onSend={(messages) => onSend2(messages)}
          //   onSend={(messages) => console.log(messages)}
          user={{
            _id: foundConversation.my_email, //login user
          }}
          placeholder={"Message"}
          renderActions={() => renderActions({loadImageAttachment: loadImageAttachment})}
          renderAvatar={() => null}
          renderChatFooter={() => 
          (<View>
            {
              ((attachment.length > 4) && (
                <>
                  <Icon name="close" color={'red'} size={24} style={{position: 'absolute', zIndex: 99,}} />
                  <Image source={{uri: attachment}} style={{width: '100%', height: '100%'}}/>
                </>
                
              ))
            }
          </View>)}
          renderBubble={(props) => {
            return (
              <Bubble
                {...props}
                textStyle={{
                  right: {
                    color: "white",
                  },
                  left: { color: "black" },
                }}
                timeTextStyle={{
                  right: { color: "white" },
                  left: { color: "black" },
                }}
                wrapperStyle={{
                  left: {
                    backgroundColor: "#C4C4C4",
                  },
                  right: {
                    backgroundColor: "#001D38",
                  },
                }}
              />
            );
          }}
          textInputStyle={{
            backgroundColor: "#EAEAEA",
            margin: 2,
            marginTop: 6,
            padding: 8,
            paddingTop: 10,
            borderRadius: 15,
          }}
          // quickReplyStyle={{backgroundColor: "red"}}
          // renderAvatar={renderAvatar}
        />
      )}
    </View>
  );
};

export default MessageView;
