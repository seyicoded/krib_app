// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  TextInput,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";

// redux
import { ApplicationState } from "../../redux";
// import CustomText from "../../components/customText/CustomText";
import CustomInput from "../../components/customInput/CustomInput";
// import CustomButton from "../../components/customButton/CustomButton";

// components
import {
  PersistLoginRecord,
  GetCompanyFullDetails,
  ShowNotification,
  GetCompanyKey,
  GetPersistData,
  PersistData,
} from "../../utils/UtilityFunctions";

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import qs from "qs";
import CustomButton from "../../components/customButton/CustomButton";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { userLogin as apiUserLogin } from "../../services/api";
import RNRestart from "react-native-restart";
// util

interface LoginModuleScreenProps {}

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const BODY: ViewStyle = {
  marginTop: 20,
};
const INPUTWRAP: ViewStyle = {
  width: "100%",
  justifyContent: "center",
  marginTop: 20,
  //   backgroundColor: "green",
};

const LoginModule = ({ navigation, route }) => {
  const navigations = useNavigation();
  // const [emailOrUsername, setEmailOrUsername] = useState(
  //   "tomzyadex@gmail.com"
  // );
  // const [password, setPassword] = useState("poiuy54321");
  const [emailOrUsername, setEmailOrUsername] = useState(
    "doinglifewithoyintella@gmail.com"
  );
  const [password, setPassword] = useState("Kribmylove");
  const [processing, setprocessing] = useState(false);

  const mode = "dark";

  const processLogin = async () => {
    if (emailOrUsername === "") {
      ShowNotification("Email is  required", "warning");
      return;
    }
    if (password === "") {
      ShowNotification("Pls enter your password", "warning");
      return;
    }
    let payload = {
      emailAddress: emailOrUsername,
      password: password,
    };
    // let payload = {
    //   emailAddress: "jinaddavid+00@gmail.com",
    //   password: "hash",
    // };
    // console.warn("here now");
    try {
      setprocessing(true);
      const result = await apiUserLogin(payload);
      const { kind, data } = result;
      const loginData = data?.data;
      // console.warn(result);
      // setprocessing(false)

      if (kind === "ok") {
        // ShowNotification("login successfully");
        PersistData("UserToken", loginData?.token);
        let logintoken_datas = {
          accessToken: loginData?.token,
          isloggedin: true,
          usertype: "normal",
        };
        PersistLoginRecord(loginData, logintoken_datas);
        // PersistData("MyProfile", profile);
        // PersistData("userId", data.data);
        // PersistData("Userdata", data.data);
        // dispatch(signInUserSuccess())
        // dispatch(setUserDetails(data.data))
        // navigation.navigate("Main", {
        //   screen: "TabHome",
        // });
        // RNRestart.Restart();
        // return first_name !== data.data.first_name ? dispatch(savePin('')) : ''
      } else {
        const newMsg =
          data.message || "account not authorise, pls check you credential";
        // const newMsg = data.message + data.reason;
        ShowNotification(newMsg, "error");
        // dispatch(notify(`${data.message}`, 'danger'))
        // dispatch(signInUserFailure())
      }
    } catch ({ message }) {
      setprocessing(false);
      ShowNotification(message);

      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };
  // props

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      {/* <Header leftView={true} rightView={false} title="Welcome to Krib!" /> */}
      <ScrollView
        contentContainerStyle={{
          justifyContent: "center",
        }}
        style={{
          paddingVertical: 15,
        }}
      >
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View
              style={{
                // justifyContent: "center",
                marginTop: 10,
                minHeight: Layout.window.height,
                // minHeight: "100%",
                flex: 1,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "flex-start",
                  marginBottom: 90,
                }}
              >
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Image source={images.back} />
                </TouchableOpacity>
                <CustomText
                  textSize={18}
                  textWeight={"bold"}
                  textcolor={colors.yellow}
                  displayText={"Welcome to Krib!"}
                  textStyle={{
                    textAlign: "center",

                    alignSelf: "center",
                  }}
                />
                <Image source={images.back} style={{ opacity: 0 }} />
              </View>

              <View style={INPUTWRAP}>
                {/* <CustomInput
                  inputText={"Username or Email"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={{marginBottom: 10,}}
                  onChangeText={(value: string) => {
                    setEmailOrUsername(value.replace(/^\s+|\s+$/gm, ""));
                  }}
                  placeHolderText={"Username or Email"}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  showIcon={false}
                /> */}
                <TextInput
                  placeholder="Username or Email"
                  style={{ paddingHorizontal: 15, fontSize: 18 }}
                  onChangeText={(value: string) => {
                    setEmailOrUsername(value.replace(/^\s+|\s+$/gm, ""));
                  }}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  placeholderTextColor={'black'}
                />
                <ImageBackground
                  resizeMode="stretch"
                  source={images.line}
                  style={{
                    width: "100%",
                    flex: 1,
                    height: 30,
                  }}
                />
              </View>
              <View style={INPUTWRAP}>
                {/* <CustomInput
                  inputText={"Password"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={{marginBottom: 10,}}
                  onChangeText={(value: string) => {
                    setPassword(value.replace(/^\s+|\s+$/gm, ""));
                  }}
                  placeHolderText={"Password"}
                  autoCapitalize="none"
                  secureTextEntry
                  showIcon={false}
                /> */}

                <TextInput
                  placeholder="Password"
                  style={{ paddingHorizontal: 15, fontSize: 18 }}
                  secureTextEntry
                  onChangeText={(value: string) => {
                    setPassword(value.replace(/^\s+|\s+$/gm, ""));
                  }}
                  placeholderTextColor={'black'}
                />
                <ImageBackground
                  resizeMode="stretch"
                  source={images.line}
                  style={{
                    width: "100%",
                    flex: 1,
                    height: 30,
                  }}
                />
              </View>
              <View style={{ paddingHorizontal: 15 }}>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("ForgetPassword");
                  }}
                >
                  <CustomText
                    textSize={13}
                    textWeight={"bold"}
                    textcolor={colors.black}
                    displayText={"Forgot password?"}
                    textStyle={{ textAlign: "right", marginBottom: 50 }}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ paddingHorizontal: 15 }}>
                <CustomButton
                  btnText={processing === true ? "Processing...." : "Login"}
                  handlePress={() => {
                    processLogin();
                    // navigation.navigate("TabHome");
                    // navigation.navigate("Main", { screen: "TabHome" });
                  }}
                />
              </View>
              <View
                style={{
                  paddingHorizontal: 15,
                  paddingVertical: 30,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <CustomText
                  textSize={16}
                  textWeight={"bold"}
                  textcolor={colors.black}
                  displayText={"Don’t have an account? "}
                  textStyle={{ textAlign: "right", marginBottom: 90 }}
                />
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("SignUp");
                  }}
                >
                  <CustomText
                    textSize={16}
                    textWeight={"600"}
                    textcolor={colors.yellow}
                    displayText={"Sign up"}
                    textStyle={{ textAlign: "right", marginBottom: 90 }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
    </View>
  );
};

export default LoginModule;
