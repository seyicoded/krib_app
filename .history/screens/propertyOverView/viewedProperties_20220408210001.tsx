// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import {
  PersistLoginRecord,
  GetPersistData,
  myspiner as Myspiner,
  SkeletonLoader,
  ShowNotification,
} from "../../utils/UtilityFunctions";
// redux
import { ApplicationState } from "../../redux";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import {
  getProperty as apiGetProperty,
  getPropertyWithAuth as apiGetPropertyWithAuth,
  recordviewedProperty,
} from "../../services/api";
import RNMonnify from "@monnify/react-native-sdk";
// util

interface ListedPropertyModuleScreenProps {}

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  paddingHorizontal: 20,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: "100%",
  borderRadius: 6,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BgColor = "#EE8727";

const ListedPropertyModule = ({ navigation, route }) => {
  const { propertyData } = route.params;
  console.warn(propertyData, "property data");

  const navigations = useNavigation();
  const [allPropertiesData, setAllpropertiesdata] = useState(null);
  const [propertiesHolderSale, setPropertiesHolderSale] = useState(null);
  const [loginState, setLoginState] = useState(false);
  const [loadingState, setLoadingState] = useState(false);
  const [loginProfile, setLoginProfile] = useState({});

  const mode = "dark";

  useEffect(() => {
    // GetPersistData("Loggedinstate")
    // console.warn(userToken[0]?.["isloggedin"]);

    // GetUserLogin();
    fetchMyAPI();
  }, []);
  const fetchMyAPI = async () => {
    setLoadingState(true);
    try {
      setAllpropertiesdata(propertyData);
    } catch ({ message }) {
      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };
  // const GetUserLogin = async () => {
  //   try {
  //     let userToken = await GetPersistData("Loggedinstate");
  //     let loggedinUserdetails = await GetPersistData("loggedinUserdetails");
  //     loggedinUserdetails = JSON.parse(loggedinUserdetails);

  //     console.log(loggedinUserdetails.profile, "loggedinUserdetails.profile ");
  //     setLoginProfile(loggedinUserdetails?.profile);

  //     let ss = JSON.parse(userToken);
  //     console.log(ss, "login stateeee");
  //     if (ss?.isloggedin === true) {
  //       setLoginState(ss?.isloggedin);
  //       fetchMyAPI(true);
  //     } else {
  //       setLoginState(false);
  //       fetchMyAPI(false);
  //     }
  //     console.log(ss, "login details ");
  //   } catch (err) {}
  // };
  useEffect(() => {
    let propertiesHolderSale = (allPropertiesData || []).map(
      (propertiesItems, index) => {
        return (
          <TouchableOpacity key={index}>
            <ImageBackground
              resizeMode="stretch"
              source={images.bg2}
              style={{
                flex: 1,
                //   width: 395,
                padding: 10,
                justifyContent: "center",
              }}
            >
              <View style={CARDCONTAINER}>
                <Image
                  source={{
                    uri: propertiesItems?.propertyImages[0]?.url,
                  }}
                  style={CARDIMAGE}
                />
                <View style={FULLWIDTH}>
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.black}
                    displayText={propertiesItems?.address}
                    textStyle={{ marginBottom: 5, marginTop: 20 }}
                  />
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.black}
                    displayText={propertiesItems?.description}
                    textStyle={{ marginBottom: 5 }}
                  />
                  <Naira
                    amount={propertiesItems.price}
                    amountWeight={"bold"}
                    boldnaira
                  />
                </View>
              </View>
              <TouchableOpacity
                style={MOREBTN}
                onPress={() => {
                  loginState === true
                    ? processPropertyView(propertiesItems)
                    : navigation.navigate("Main", {
                        screen: "ThankYou",
                      });
                }}
              >
                <Image source={images.more} />
              </TouchableOpacity>
            </ImageBackground>
          </TouchableOpacity>
        );
      }
    );

    // console.warn(propertiesHolderRent, "what we have here ");

    setPropertiesHolderSale(propertiesHolderSale);
  }, [allPropertiesData]);

  const processPropertyView = async (datas) => {
    console.warn(datas, "ddd");
    const getPropertyId = datas.id;
    const payload = {
      propertyId: getPropertyId,
    };
    try {
      let result;
      result = await recordviewedProperty(payload);

      console.warn(result, "resultresult");

      const { kind, data } = result;

      // console.warn(data.data, "first page");
      // setLoadingState(false);

      if (kind === "ok") {
        navigation.navigate("Main", {
          screen: "ViewDetails",
          params: { propertyData: datas },
        });
      } else {
        const err = data?.message;
        console.warn(err);
        navigation.navigate("Main", {
          screen: "ViewDetails",
          params: { propertyData: datas },
        });

        // ShowNotification(err, "error");
      }
    } catch ({ message }) {
      console.warn(message);
      // ShowNotification({ message });
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };

  // props

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftView={true}
        rightView={true}
        onLeftPress={() => {
          navigation.navigate("Main", {
            screen: "MySettings",
          });
        }}
      />
      <ScrollView style={{ paddingVertical: 15, marginTop: 15 }}>
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View style={{ paddingHorizontal: 15 }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Login");
                }}
              >
                <CustomText
                  textSize={25}
                  textWeight={"normal"}
                  textcolor={colors.dark}
                  displayText={"Discover your"}
                  textStyle={{ marginTop: 0 }}
                />
              </TouchableOpacity>

              <CustomText
                textSize={25}
                textWeight={"bold"}
                textcolor={colors.yellow}
                displayText={"new home"}
                textStyle={{ marginTop: 0 }}
              />
              <CustomText
                textSize={18}
                textWeight={"bold"}
                textcolor={colors.dark}
                displayText={"Find your dream home!"}
                textStyle={{ marginTop: 15, marginBottom: 26 }}
              />
            </View>
            <View style={{ width: "100%", flex: 1 }}>
              <View style={[FLEXROW, { paddingHorizontal: 15 }]}>
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.yellow}
                  displayText={"Buy a home"}
                />
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.dark}
                  displayText={"SALE"}
                  // textStyle={}
                />
              </View>
              {loadingState ? (
                <ActivityIndicator size="large" color={BgColor} />
              ) : (
                propertiesHolderSale
              )}
            </View>
            <View style={{ width: "100%", flex: 1 }}>
              <ImageBackground
                resizeMode="stretch"
                source={images.bg2}
                style={{
                  flex: 1,
                  //   width: 395,
                  padding: 30,
                  justifyContent: "center",
                  marginBottom: 15,
                }}
              >
                <View style={CARDCONTAINER}>
                  <View style={FULLWIDTH}>
                    <CustomText
                      textSize={18}
                      textWeight={"500"}
                      textcolor={colors.black}
                      displayText={"Sell/Lease"}
                      textStyle={{ textAlign: "center", marginBottom: 5 }}
                    />
                    <CustomText
                      textSize={15}
                      textWeight={"500"}
                      textcolor={colors.dark}
                      displayText={
                        "As a property owner, you can list your property in 5 easy steps."
                      }
                      textStyle={{ textAlign: "center" }}
                    />
                    <CustomText
                      textSize={15}
                      textWeight={"500"}
                      textcolor={colors.dark}
                      displayText={
                        "Your property will go live after   verification."
                      }
                      textStyle={{ textAlign: "center" }}
                    />
                  </View>
                  <View
                    style={[
                      FLEXROW,
                      {
                        justifyContent: "flex-end",
                        width: "100%",
                        marginTop: 10,
                      },
                    ]}
                  >
                    <TouchableOpacity
                      style={GETSTARTED}
                      onPress={() => {
                        loginState === true
                          ? navigation.navigate("Main", {
                              screen: "StepRegistration",
                            })
                          : navigation.navigate("Main", { screen: "Login" });
                      }}
                    >
                      <CustomText
                        textSize={12}
                        textWeight={"bold"}
                        textcolor={colors.dark}
                        displayText={"Get Started"}
                        textStyle={{ textAlign: "center" }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </ImageBackground>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
    </View>
  );
};

export default ListedPropertyModule;
