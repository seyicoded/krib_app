// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  Alert
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";

// redux
import { ApplicationState } from "../../redux";
import RNMonnify from "@monnify/react-native-sdk";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import AlertModal from "../../components/alert/Alert";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { GetPersistData, ShowNotification } from "../../utils/UtilityFunctions";
import { PurchaseProperty } from "../../services/api";

import { AppEventsLogger } from "react-native-fbsdk-next";
import analytics from '@react-native-firebase/analytics';

// util

interface ViewDetailsScreenProps {}

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: "100%",
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};

const ViewDetails = ({ navigation, route }) => {
  const { propertyData } = route.params;
  console.warn(propertyData, "property data");
  
  const navigations = useNavigation();

  const [propertiesHolder, setPropertiesHolder] = useState(null);
  const [carouselData, setCarouselData] = useState(null);
  const [loginProfile, setLoginProfile] = useState({});

  const mode = "dark";

  useEffect(() => {
    (async()=>{
      try{

        let loggedinUserdetails = await GetPersistData("loggedinUserdetails");
        loggedinUserdetails = JSON.parse(loggedinUserdetails);

      // console.log(loggedinUserdetails.profile.username, "loggedinUserdetails.profile ");

        // AppEventsLogger.logEvent(`Property ${propertyData.listingTitle} viewed by ${loggedinUserdetails.profile.username}`);
        AppEventsLogger.logEvent('select_content', {
          by: loggedinUserdetails.profile.username,
          property: propertyData.listingTitle,
          onDate: (new Date()).toString(),
          OS: Platform.OS
        });

        await analytics().logEvent('select_content', {
          by: loggedinUserdetails.profile.username,
          property: propertyData.listingTitle,
          onDate: (new Date()).toString(),
          OS: Platform.OS
        })

        await analytics().logEvent('basket', {
          id: 3745092,
          item: 'mens grey t-shirt',
          description: ['round neck', 'long sleeved'],
          size: 'L',
        })

        // Alert.alert('aaa')

        await analytics().logSelectContent({
          content_type: 'clothing',
          item_id: 'abcd',
        })

      }catch(e){
        console.log(e);
        // Alert.alert("Error", "Something went wrong");
      }
    })()
  }, [])

  useEffect(() => {
    setPropertiesHolder(propertyData);
    GetUserLogin();
  }, []);
  // props
  useEffect(() => {
    const propDataImage = propertyData?.propertyImages;

    const processMerchantDeals = (propDataImage || []).map((propItems) => ({
      ...propItems,
      uri: `${propItems?.url}`,
    }));

    setCarouselData(processMerchantDeals);
  }, [propertyData]);
  const GetUserLogin = async () => {
    try {
      let userToken = await GetPersistData("Loggedinstate");
      let loggedinUserdetails = await GetPersistData("loggedinUserdetails");
      loggedinUserdetails = JSON.parse(loggedinUserdetails);

      console.log(loggedinUserdetails.profile, "loggedinUserdetails.profile ");
      setLoginProfile(loggedinUserdetails?.profile);
    } catch (err) {}
  };
  const ReserveProperty = () => {
    console.log(loginProfile, "loginProfile");
    let customerName = loginProfile?.usersProfile?.fullname;
    let customerEmail = loginProfile?.username;
    console.log(propertiesHolder, "propertiesHolder");
    let propertyPrice = propertiesHolder.price;
    let paymentDescription = "Krib Property Payment";
    // alert(customerName)
    // alert(propertyPrice)
    RNMonnify.initializePayment({
      amount: propertyPrice,
      customerName: customerName,
      customerEmail: customerEmail,
      paymentReference: "" + Math.floor(Math.random() * 1000000000 + 1),
      paymentDescription: paymentDescription,
      currencyCode: "NGN",
      incomeSplitConfig: [],
      paymentMethods: ["CARD", "ACCOUNT_TRANSFER"],
    })
      .then((response) => {
        console.log(response, "lllll");
        let refrence = response?.transactionReference;
        ReservePropNew(refrence);
        // card charged successfully, get reference here
      })
      .catch((error) => {
        console.log(error); // error is a javascript Error object
        console.log(error.message);
        console.log(error.code);
        alert(error.message);
      });
  };
  const ReservePropNew = async (refrence) => {
    if (refrence === "") {
      ShowNotification("Payment REFERENCE is required");
      return;
    }
    let propertyId = propertiesHolder.id;
    let payload = {
      propertyId: propertyId,
      transactionReference: refrence,
    };
    // let payload = {
    //   emailAddress: "jinaddavid+00@gmail.com",
    //   password: "hash",
    // };
    // console.warn("here now");
    try {
      const result = await PurchaseProperty(payload);
      console.log(result, "resultresultresult");

      const { kind, data } = result;
      // const loginData = data?.data;
      // console.warn(result);

      if (kind === "ok") {
        alert("please show success modal ");
        console.log(data, "datadatadatadata");

        // ShowNotification("login hsuccessfully");
        // PersistData("UserToken", loginData?.token);
        // let logintoken_datas = {
        //   accessToken: loginData?.token,
        //   isloggedin: true,
        //   usertype: "normal",
        // };
        // PersistLoginRecord(loginData, logintoken_datas);
        // PersistData("MyProfile", profile);
        // PersistData("userId", data.data);
        // PersistData("Userdata", data.data);
        // dispatch(signInUserSuccess())
        // dispatch(setUserDetails(data.data))
        // navigation.navigate("TabHome");
        // return first_name !== data.data.first_name ? dispatch(savePin('')) : ''
      } else {
        const newMsg =
          data.message || "An error occure while recording the payment.";
        // const newMsg = data.message + data.reason;
        ShowNotification(newMsg, "error");
        // dispatch(notify(`${data.message}`, 'danger'))
        // dispatch(signInUserFailure())
      }
    } catch ({ message }) {
      let newMsg = "An error occure while recording the payment";
      ShowNotification(newMsg, "error");

      // ShowNotification(message);
      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftIcon={true}
        rightText={propertiesHolder?.slesTypes}
        onLeftPress={() => navigation.goBack()}
      />
      <ScrollView contentContainerStyle={{ paddingVertical: 15 }}>
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View style={{ paddingHorizontal: 15 }}>
              <NumberCarousel data={carouselData} />
            </View>
            <View style={[{ paddingHorizontal: 15 }]}>
              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={propertiesHolder?.address}
                // displayText={JSON.stringify(loginProfile)}
                textStyle={{ marginTop: 6 }}
              />
              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={propertiesHolder?.description}
                textStyle={{ marginBottom: 6, marginTop: 6 }}
              />

              <Naira
                amount={propertiesHolder?.price}
                amountWeight={"bold"}
                boldnaira
              />
              <CustomText
                textSize={15}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={propertiesHolder?.AppliancesNFinishings}
                textStyle={{ marginBottom: 6, marginTop: 6, lineHeight: 30 }}
              />
              <TouchableOpacity
                style={[
                  FLEXROW,
                  { justifyContent: "flex-start", alignItems: "center" },
                ]}
              >
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.dark2}
                  displayText={"Show more"}
                  textStyle={{ marginBottom: 0, marginTop: 0 }}
                />
                <Image source={images.caretDown} />
              </TouchableOpacity>

              <View style={{ marginBottom: 30 }}>
                <CustomText
                  textSize={16}
                  textWeight={"bold"}
                  textcolor={colors.dark2}
                  displayText={"Facts and Features"}
                  textStyle={{ marginBottom: 6, marginTop: 6 }}
                />
                <CustomText
                  textSize={16}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={"Property details"}
                  textStyle={{ marginBottom: 6, marginTop: 6 }}
                />

                <CustomText
                  textSize={15}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={`Type: ${propertiesHolder?.PropertyType} `}
                  textStyle={{ marginTop: 6 }}
                />
                <CustomText
                  textSize={15}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={`Year built: ${propertiesHolder?.YearBuilt}`}
                  textStyle={{ marginTop: 6 }}
                />
                <CustomText
                  textSize={15}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={`Available units :  ${propertiesHolder?.AvailableUnits}`}
                  textStyle={{ marginTop: 6 }}
                />
                <CustomText
                  textSize={15}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={"Square meter: --- "}
                  textStyle={{ marginTop: 6 }}
                />

                <CustomText
                  textSize={16}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={"Interior details"}
                  textStyle={{ marginBottom: 6, marginTop: 12 }}
                />

                <CustomText
                  textSize={15}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={`Bedrooms : ${propertiesHolder?.BuildingAmenities} `}
                  textStyle={{ marginTop: 6 }}
                />
                <CustomText
                  textSize={15}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={`Bathrooms : ${propertiesHolder?.BuildingAmenities}`}
                  textStyle={{ marginTop: 6 }}
                />
                <CustomText
                  textSize={15}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={`Kitchen : ${propertiesHolder?.BuildingAmenities} `}
                  textStyle={{ marginTop: 6 }}
                />
                <CustomText
                  textSize={15}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={`Sitting rooms : ${propertiesHolder?.BuildingAmenities} `}
                  textStyle={{ marginTop: 6 }}
                />
              </View>

              <View style={{ marginBottom: 15 }}>
                <CustomText
                  textSize={16}
                  textWeight={"bold"}
                  textcolor={colors.dark2}
                  displayText={"Price Insights"}
                  textStyle={{ marginBottom: 6, marginTop: 6 }}
                />
                <View style={[FLEXROW, { justifyContent: "flex-start" }]}>
                  <CustomText
                    textSize={15}
                    textWeight={"500"}
                    textcolor={colors.dark2}
                    displayText={"List price : "}
                    textStyle={{ marginBottom: 6, marginTop: 6 }}
                  />
                  <Naira
                    amount={propertiesHolder?.price}
                    amountWeight={"bold"}
                    boldnaira
                  />
                </View>

                <CustomText
                  textSize={16}
                  textWeight={"500"}
                  textcolor={colors.dark2}
                  displayText={"Buyers brokerage : 0%"}
                  textStyle={{ marginBottom: 6, marginTop: 6 }}
                />
              </View>
              <TouchableOpacity
                style={[
                  FLEXROW,
                  {
                    justifyContent: "flex-start",
                    alignItems: "center",
                    marginBottom: 5,
                  },
                ]}
              >
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.dark2}
                  displayText={"See more facts and features"}
                  textStyle={{ marginBottom: 0, marginTop: 0 }}
                />
                <Image source={images.caretDown} />
              </TouchableOpacity>
              <View style={[FLEXROW, { marginBottom: 10 }]}>
                <ImageBackground
                  resizeMode="stretch"
                  source={images.card2}
                  style={{
                    flex: 1,
                    padding: 10,
                    justifyContent: "center",
                    alignItems: "center",
                    maxWidth: "45%",
                    paddingVertical: 30,
                  }}
                >
                  <TouchableOpacity
                    style={{ justifyContent: "center", alignItems: "center" }}
                    onPress={() =>
                      navigation.navigate("Main", {
                        screen: "SendMsg",
                        params: { propertyData: propertiesHolder },
                      })
                    }
                  >
                    <CustomText
                      textSize={15}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={"Message"}
                      textStyle={{ marginBottom: 0, marginTop: 10 }}
                    />
                    <CustomText
                      textSize={15}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={"seller"}
                      textStyle={{ marginBottom: 0, marginTop: 0 }}
                    />
                    <Image source={images.msg} />
                  </TouchableOpacity>
                </ImageBackground>
                <ImageBackground
                  resizeMode="stretch"
                  source={images.card2}
                  style={{
                    flex: 1,
                    padding: 10,
                    justifyContent: "center",
                    alignItems: "center",
                    maxWidth: "45%",
                    paddingVertical: 30,
                  }}
                >
                  <TouchableOpacity
                    style={{ justifyContent: "center", alignItems: "center" }}
                    onPress={() => ReserveProperty()}
                  >
                    <CustomText
                      textSize={15}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={"Reserve this"}
                      textStyle={{ marginBottom: 0, marginTop: 10 }}
                    />
                    <CustomText
                      textSize={15}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={"property for me"}
                      textStyle={{ marginBottom: 0, marginTop: 0 }}
                    />
                    <Image source={images.prop} />
                  </TouchableOpacity>
                </ImageBackground>
              </View>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
      <AlertModal />
    </View>
  );
};

export default ViewDetails;
