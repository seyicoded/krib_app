// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import {
  GetPersistData,
  ShowNotification,
  myspiner,
  removePersistData,
  Communicator,
} from "../../utils/UtilityFunctions";

// redux
import { ApplicationState } from "../../redux";
import { getviewedproperty } from "../../services/api";

// components
import Share from "react-native-share";

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";

import { getProfile } from "../../services/api";

// util

interface MySettingsScreenProps {}

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,

  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  flexWrap: "wrap",
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: 240,
  marginRight: 10,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BTNSTYLE: TextStyle = {
  borderRadius: 5,
  backgroundColor: colors.dark2,
  justifyContent: "center",
  alignItems: "center",
  padding: 15,
  width: "100%",
  marginTop: 40,
};
const BgColor = "#EE8727";

const MySettings = ({ navigation, route }) => {
  const navigations = useNavigation();
  const [userDataGotten, setUserDataGotten] = useState(null);
  const [viewedPropertys, setViewedProperty] = useState([]);
  const [profileFound, setProfileFound] = useState(null);
  const [loadingState, setLoadingState] = useState(false);

  const mode = "dark";

  // props

  useEffect(() => {
    async function fetUserProperty() {
      //  setLoadingState(true);
      // <Myspiner state={true} />;
      try {
        const result = await getProfile();
        const { kind, data } = result;

        console.log("profile settings : ", result);

        if (kind === "ok") {
          console.warn(data?.data, "david");
          setProfileFound(data?.data);
          console.warn(profileFound, "ssssss");

          setLoadingState(false);
        }
      } catch ({ message }) {
        setLoadingState(false);
        ShowNotification(message, "danger");
      }
    }
    fetUserProperty();
  }, []);

  // useEffect(() => {

  // }, []);
  useFocusEffect(
    React.useCallback(() => {
      async function fetAllUserdata() {
        let userData = await GetPersistData("loggedinUserdetails");
        const newdata = JSON.parse(userData);
        setUserDataGotten(newdata?.profile);
      }
      fetAllUserdata();
    }, [])
  );

  // useEffect(() => {
  //   async function fetchViewedProperty() {
  //     // setLoadingState(true);
  //     // <Myspiner state={true} />;
  //     try {
  //       const result = await getviewedproperty();
  //       const { kind, data } = result;

  //       console.warn(data.data, "get viewed property");

  //       if (kind === "ok") {
  //         const dd = data.data;
  //         console.warn(dd.length);

  //         setViewedProperty(data.data);
  //         // setLoadingState(false);
  //       }
  //     } catch ({ message }) {
  //       console.warn(message);

  //       // ShowNotification(message);
  //     }
  //   }
  //   fetchViewedProperty();
  // }, []);

  const dataNew = [
    {
      image: images.profileIcon,
      text: "Profile details",
      goto: "ProfileDetail",
    },
    // {
    //   image: images.termService,
    //   text: "Payment Info",
    //   goto: "Viewbank",
    // },
    {
      image: images.pin_security,
      text: "Pin Security",
      goto: "SecurePin",
    },
    {
      image: images.termService,
      text: "Terms of service",
      goto: "termService",
    },
    {
      image: images.tellFriends,
      text: "Tell a friend",
      goto: "",
    },
    {
      image: images.customerSupport,
      text: "Customer support",
      goto: "",
    },
    {
      image: images.faq,
      text: "FAQs",
      goto: "",
      navigate: "FAQScreen",
    },
    {
      image: images.logout,
      text: "Logout",
      goto: "",
    },
  ];
  const logout = () => {
    console.log("new");
    // dashboardStore.Logmeout = true
    removePersistData("Loggedinstate")
      .then(() => {
        // alert("logged out")
        navigation.navigate("Main", {
          screen: `Login`,
        });
      })

      .catch((err) => {
        console.warn(err);
      });
  };

  const fun = async () => {
    const options = {
      message:
        "Hey🙏, I started using Nigeria’s #1 property app - I thought of you, and you should download it, too.\n \n https://bit.ly/3gXsXdW  \n \n Using Krib, you can buy, sell and rent properties without using agents. You can also make payments and sign rental agreements online. You should definitely check it out. 👇👇👇👇👇👇 \n \n https://bit.ly/3gXsXdW",
      title: "Krib Share",
    };
    const shareResponse = await Share.open(options);
    console.warn(shareResponse);
  };

  let listSettingItems = dataNew.map((dataItems, index) => {
    return (
      <ImageBackground
        resizeMode="stretch"
        source={images.card7}
        style={{
          flex: 1,
          // padding: 10,
          justifyContent: "center",
          alignItems: "center",
          maxWidth: "100%",
          // marginTop: 20,
          // minWidth: "%",
          paddingHorizontal: 20,
          // backgroundColor: "green",
          // paddingVertical: 18,
        }}
        key={index}
      >
        {dataItems.text === "Logout" ? (
          <TouchableOpacity
            style={{
              flex: 1,
              paddingHorizontal: 15,
              paddingVertical: 15,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}
            onPress={() => {
              logout();
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                // minWidth: "%",
              }}
            >
              <Image source={dataItems.image} />
              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={dataItems.text}
                textStyle={{
                  marginBottom: 0,
                  marginTop: 0,
                  lineHeight: 24,
                }}
              />
            </View>
            <Image source={images.arrFowr} />
          </TouchableOpacity>
        ) : dataItems.text === "Tell a friend" ? (
          <TouchableOpacity
            style={{
              flex: 1,
              paddingHorizontal: 15,
              paddingVertical: 15,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}
            onPress={() => {
              fun();
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                // minWidth: "%",
              }}
            >
              <Image source={dataItems.image} />
              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={dataItems.text}
                textStyle={{
                  marginBottom: 0,
                  marginTop: 0,
                  lineHeight: 24,
                }}
              />
            </View>
            <Image source={images.arrFowr} />
          </TouchableOpacity>
        ) : dataItems.text == "Customer support" ? (
          <TouchableOpacity
            style={{
              flex: 1,
              paddingHorizontal: 15,
              paddingVertical: 15,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}
            onPress={() => {
              Communicator("web", "https://wa.me/message/4V2QMP4I2FOUG1");
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                // minWidth: "%",
              }}
            >
              <Image source={dataItems.image} />
              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={dataItems.text}
                textStyle={{
                  marginBottom: 0,
                  marginTop: 0,
                  lineHeight: 24,
                }}
              />
            </View>
            <Image source={images.arrFowr} />
          </TouchableOpacity>
        ) : dataItems.text == "FAQs" ? (
          <TouchableOpacity
            style={{
              flex: 1,
              paddingHorizontal: 15,
              paddingVertical: 15,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}
            onPress={() => navigation.navigate("FAQScreen")}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                // minWidth: "%",
              }}
            >
              <Image source={dataItems.image} />
              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={dataItems.text}
                textStyle={{
                  marginBottom: 0,
                  marginTop: 0,
                  lineHeight: 24,
                }}
              />
            </View>
            <Image source={images.arrFowr} />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={{
              flex: 1,
              paddingHorizontal: 15,
              paddingVertical: 15,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}
            onPress={() =>
              navigation.navigate("Main", {
                screen: `${dataItems?.goto}`,
                params: { userProfileDetails: profileFound },
              })
            }
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                // minWidth: "%",
              }}
            >
              <Image source={dataItems.image} />
              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={dataItems.text}
                textStyle={{
                  marginBottom: 0,
                  marginTop: 0,
                  lineHeight: 24,
                }}
              />
            </View>
            <Image source={images.arrFowr} />
          </TouchableOpacity>
        )}
      </ImageBackground>
    );
  });

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftIcon={true}
        title="Settings"
        // rightIcon
        onLeftPress={() => navigation.goBack()}
      />
      <TouchableOpacity onPress={() => navigation.navigate("FAQScreen")}>
        <Text>FAQScreen</Text>
      </TouchableOpacity>
      {loadingState ? (
        <ActivityIndicator size="large" color={BgColor} />
      ) : (
        <ScrollView style={{ paddingVertical: 15 }}>
          <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
            <Animatable.View animation={"slideInUp"}>
              <View
                style={{
                  paddingHorizontal: 15,
                  justifyContent: "center",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                <ImageBackground
                  source={images.profile}
                  style={{
                    width: 149,
                    height: 110,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Image
                    source={{
                      uri:
                        profileFound?.usersProfile?.avatar ||
                        "https://res.cloudinary.com/vaccinenudge/image/upload/v1594301930/samples/people/smiling-man.jpg",
                    }}
                    style={{
                      width: 70,
                      height: 70,
                      borderRadius: 70 / 2,
                      marginLeft: -10,
                    }}
                  />
                </ImageBackground>
              </View>
              <View style={[{ paddingHorizontal: 15 }]}>
                <CustomText
                  textSize={16}
                  textWeight={"bold"}
                  textcolor={colors.black}
                  displayText={profileFound?.usersProfile?.fullname}
                  textStyle={{
                    marginTop: 10,
                    textAlign: "center",
                    marginBottom: 14,
                  }}
                />

                <View
                  style={[
                    FLEXROW,
                    {
                      marginBottom: 10,
                      justifyContent: "center",
                      flexDirection: "column",
                    },
                  ]}
                >
                  {listSettingItems}
                </View>
              </View>
            </Animatable.View>
          </Animatable.View>
        </ScrollView>
      )}
    </View>
  );
};

export default MySettings;
