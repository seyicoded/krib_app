// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard
} from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import RadioGroup from "react-native-radio-buttons-group";
import DateTimePicker from "@react-native-community/datetimepicker";
import DatePicker from "react-native-date-picker";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import MapView from "react-native-maps";
import {
  PersistLoginRecord,
  GetCompanyFullDetails,
  ShowNotification,
  GetCompanyKey,
  GetPersistData,
  PersistData,
  cloudinaryUpload,
} from "../../utils/UtilityFunctions";
// redux
import { ApplicationState } from "../../redux";

// components
import {
  YOUR_CLOUDINARY_PRESET,
  YOUR_CLOUDINARY_NAME,
  requestCameraPermission,
  YOUR_CLOUDINARY_PRESET_JobBanner,
} from "../../utils/UtilityFunctions";

import NeomorphButton from "../../components/neomorph-btns/neomorph-btn";

import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import DocumentPicker from "react-native-document-picker";
import { KeyboardAvoidContainer } from "../../components/keybord/keybord-avoid-container";

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomMap from "../../components/map/GooglePlacesInput";
import CustomInput from "../../components/customInput/CustomInput";
import CustomDate from "../../components/date/CustomDate";
import CustomTooltip from "../../components/tooltip/tooltip";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import {
  getPropertyType,
  getBuildingAmenities,
  getAppliancesAndFinishings,
  addProperty,
} from "../../services/api";

// util

interface StepRegistrationScreenProps {}
const actionSheeYearBuilttRef = React.createRef();
const actionSheetUnitRef = React.createRef();

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  flexWrap: "wrap",
};
const FLEXROW2: TextStyle = {
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
};
const FLEXROWVERIFY: TextStyle = {
  width: "100%",
  alignItems: "flex-start",
  justifyContent: "flex-start",
  flexDirection: "row",
  // flex: 1,
  marginBottom: 23,
  minHeight: 24,
};
const SETMARGIN: TextStyle = {
  marginRight: 10,
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: 240,
  marginRight: 10,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BTNSTYLE: TextStyle = {
  borderRadius: 5,
  backgroundColor: colors.dark2,
  justifyContent: "center",
  alignItems: "center",
  padding: 15,
  width: "100%",
  marginTop: 40,
};
const CHECKWRAP: TextStyle = {
  flex: 1,
  //  padding: 5,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "42%",
  width: "100%",
  height: 70,
  // minWidth: "48%",
};

const CHECKWRAPIMAGE: TextStyle = {
  width: "100%",
  height: 70,
};

const CHECKWRAP3: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "48%",
  width: "45%",
  height: 70,
  minWidth: "40%",
};
const CHECKWRAP5: TextStyle = {
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  width: "43%",
  height: 120,
  minWidth: "40%",
  marginVertical: 10,
};
const CHECKWRAP2: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 15,
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  maxWidth: "100%",
  minWidth: "100%",
  height: 75,
  marginBottom: 10,
};
const LIKECHECKBOX: TextStyle = {
  flex: 1,
  borderWidth: 1,
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 3,
};
const INTERIOCHECKER: TextStyle = {
  flex: 1,
  borderWidth: 0,
  width: "20%",
  maxWidth: "20%",
  justifyContent: "center",
  alignItems: "center",
  maxHeight: "91%",
  minHeight: "91%",
};
const INPUTWRAP2: ViewStyle = {
  width: "100%",
  paddingHorizontal: 15,
  marginBottom: 20,
};
const RNCHECKBOXSTYLE: ViewStyle = {
  height: 10,
  width: 10,
  borderRadius: 10 / 2,
};
const SETINPUTSTYLE: TextStyle = {
  marginBottom: 10,
};

const StepRegistration = ({ navigation, route }) => {
  const navigations = useNavigation();
  const [stepValue, setStepValue] = useState("Step 1");
  const [enteredlocation, setEnteredlocation] = useState("");
  const [listingTitle, setListingTitle] = useState("");
  const [priceTitle, setPriceTitle] = useState("");
  const [describedProperty, setDescribedProperty] = useState("");
  const [SquareMeter, setSquareMeter] = useState("");
  const [listingprice, setListingprice] = useState(0);
  const [serviceCharge, setServiceCharge] = useState(0);
  const [addressValue, setAddressValue] = useState("");
  const [interiorBedroomSelect, setInteriorBedroomSelect] = useState(0);
  const [interiorBathroomSelect, setInteriorBathroomSelect] = useState(0);
  const [interiorKitchenSelect, setInteriorKitchenSelect] = useState(0);
  const [yearSelect, setYearSelect] = useState("");
  const [unitSelect, setUnitSelect] = useState(0);
  const [selectPropertyType, setSelectPropertyType] = useState("");
  const [fetchPropType, setFetchPropType] = useState(null);
  const [buildingAmenities, setBuildingAmenities] = useState(null);
  const [appliancesAndFinishings, setAppliancesAndFinishings] = useState(null);
  const [propTypeSelected, setPropTypeSelected] = useState(null);
  const [buildingAmenitiesSelected, setBuildingAmenitiesSelected] = useState(
    []
  );
  const [appliancesAndFinishingsSelected, setAppliancesAndFinishingsSelected] =
    useState([]);
  const [loadingState, setLoadingState] = useState(false);
  const [loading2State, setLoading2State] = useState(false);
  const [imageArray, setImageArray] = useState([]);
  const [loopedImageArray, setLoopedImageArray] = useState(null);
  const [fileArray, setFileArray] = useState([]);
  const [uploaderFileName, setFileName] = useState("");
  const [userSelectedDate, setUserSelectedDate] = useState("DD/MM/YY");
  const [userSelectedTime, setUserSelectedTime] = useState("00:00");
  const [processing, setprocessing] = useState(false);
  const [showDate, setShowDate] = useState(false);
  const [showTime, setShowTime] = useState(false);
  const [date, setDate] = useState(new Date(1598051730000));
  const [nonsenseprop, setnonsenseprop] = useState("");
  // const [loopedFIleArray, setLoopedFIleArray] = useState(null);

  const [show, setShow] = useState(false);
  // const [stepSecondPropertyIndex, setStepSecondPropertyIndex] = useState(0);

  const mode = "dark";

  useEffect(() => {
    async function fetAllProperType() {
      // <Myspiner state={true} />;
      try {
        const result = await getPropertyType();
        const { kind, data } = result;

        if (kind === "ok") {
          setFetchPropType(data.data);
        }
      } catch ({ message }) {
        ShowNotification(message);
      }
    }
    fetAllProperType();
  }, []);

  useEffect(() => {
    async function fetBuildingAmenities() {
      // <Myspiner state={true} />;
      try {
        const result = await getBuildingAmenities();
        const { kind, data } = result;

        console.warn(data.data, "Building Amenities");

        if (kind === "ok") {
          setBuildingAmenities(data.data);
        }
      } catch ({ message }) {
        ShowNotification(message);
      }
    }
    fetBuildingAmenities();
  }, []);
  useEffect(() => {
    async function fetchAppliancesAndFinishings() {
      // <Myspiner state={true} />;
      try {
        const result = await getAppliancesAndFinishings();
        const { kind, data } = result;

        console.warn(data.data, "Appliances And Finishings");

        if (kind === "ok") {
          // console.warn(data.data, "anybody");

          setAppliancesAndFinishings(data.data);
        }
      } catch ({ message }) {
        ShowNotification(message);
      }
    }
    fetchAppliancesAndFinishings();
  }, []);
  useEffect(() => {
    // const my = imageArray;
    // const my = (imageArray || []).map((item, index) => {});
    let LoopedattashedImage = (imageArray || []).map((item, index) => {
      return (
        <ImageBackground
          key={index}
          resizeMode="stretch"
          source={{ uri: item }}
          style={[CHECKWRAP5]}
        >
          <TouchableOpacity
            onPress={(e) => removeImage(item)}
            // style={{ position: "absolut", to: 20, right: 70 }}
          >
            <Image source={images.close} style={{ width: 40, height: 40 }} />
          </TouchableOpacity>
        </ImageBackground>
      );
    });
    setLoopedImageArray(LoopedattashedImage);
  }, [imageArray]);
  const ref = useRef();

  const removeImage = (e) => {
    // console.warn(e);

    let array = imageArray; // make a separate copy of the array
    let index = array.indexOf(e);
    console.warn(e, array, index);
    if (index !== -1) {
      array.splice(index, 1);
      setImageArray(array);
    }
  };

  const handleYearBuilt = () => {
    actionSheeYearBuilttRef?.current?.show();
  };
  // console.warn(yearSelect, "ddd");

  const handleYearSelect = (params) => {
    setYearSelect(params.toString());
    actionSheeYearBuilttRef?.current?.hide();
  };
  const handleUnitSelect = (params) => {
    setUnitSelect(params);
    actionSheetUnitRef?.current?.hide();
  };
  const handleInteriorBedRoom = (params) => {
    setInteriorBedroomSelect(params);
  };
  const handleUnit = () => {
    actionSheetUnitRef?.current?.show();
  };
  const handleStepValue = (params) => {
    // if (params === "Step 2") {
    //   setListingprice(788);
    //   console.log("heree");
    // }
    setStepValue(params);
  };
  const handlePropertyType = (params) => {
    setSelectPropertyType(params);
  };
  const handleAddresstype = (params) => {
    // console.warn(params.description, "tomzy adex yung ");
    setAddressValue(params.description);
  };
  const handlegeneralAction = (params, params2) => {
    params(params2);
  };
  const handlleArrayFillold = (params) => {
    setBuildingAmenitiesSelected([...buildingAmenitiesSelected, params]);
  };
  const handlleArrayFill = (params) => {
    if (buildingAmenitiesSelected.includes(params) === false) {
      setBuildingAmenitiesSelected([...buildingAmenitiesSelected, params]);
    } else {
      for (var i = 0; i < buildingAmenitiesSelected.length; i++) {
        if (buildingAmenitiesSelected[i] === params) {
          buildingAmenitiesSelected.splice(i, 1);
          setBuildingAmenitiesSelected([...buildingAmenitiesSelected]);
        }
      }
    }
  };
  const handlleArrayFill2 = (params) => {
    if (appliancesAndFinishingsSelected.includes(params) === false) {
      setAppliancesAndFinishingsSelected([
        ...appliancesAndFinishingsSelected,
        params,
      ]);
    } else {
      for (var i = 0; i < appliancesAndFinishingsSelected.length; i++) {
        if (appliancesAndFinishingsSelected[i] === params) {
          appliancesAndFinishingsSelected.splice(i, 1);
          setAppliancesAndFinishingsSelected([
            ...appliancesAndFinishingsSelected,
          ]);
        }
      }
    }
  };

  const processAddProperty2 = async () => {
    console.warn("sss");
  };
  const processDatePicker = async () => {
    setShowTime(false);
    setShowDate(true);
  };
  const processTimePicker = async () => {
    setShowDate(false);
    setShowTime(true);
  };
  const processAddProperty = async () => {
    console.warn("here ssnow");

    // if (emailOrUsername === "") {
    //   ShowNotification("Email is  required");
    //   return;
    // }
    // if (password === "") {
    //   ShowNotification("Pls enter your password");
    //   return;
    // }
    const buildingAmenitiesSelected1 = buildingAmenitiesSelected.toString();
    const appliancesAndFinishingsSelected1 =
      appliancesAndFinishingsSelected.toString();

    let payload = {
      propertyImages: imageArray,
      propertyDocuments: fileArray,
      BedroomCount: interiorBedroomSelect,
      BathroomCount: interiorBathroomSelect,
      KitchenCount: interiorKitchenSelect,
      slesTypes: selectPropertyType,
      address: addressValue,
      price: listingprice,
      description: describedProperty,
      PropertyType: propTypeSelected,
      YearBuilt: yearSelect,
      // YearBuilt: yearSelect,
      AvailableUnits: unitSelect,
      ServiceCharge: 222,
      BuildingAmenities: buildingAmenitiesSelected1,
      AppliancesNFinishings: appliancesAndFinishingsSelected1,
      scheduleDate: userSelectedDate,
      scheduleTime: userSelectedTime,
      listingTitle: listingTitle,
    };
    // let payload = {
    //   emailAddress: "jinaddavid+00@gmail.com",
    //   password: "hash",
    // };
    // console.warn(appliancesAndFinishingsSelected, "here now");
    console.warn(payload, "here now2");
    setprocessing(true);

    try {
      const result = await addProperty(payload);
      const { kind, data } = result;
      const loginData = data?.data;
      console.warn(result);
      setprocessing(false);

      if (kind === "ok") {
        ShowNotification(
          "Your property has been received. We will contact you shortly.",
          "success"
        );
        // navigation.navigate("TabHome");
        let nextdata = {
          address: addressValue,
          scheduleDate: userSelectedDate,
          scheduleTime: userSelectedTime,
        };
        navigation.navigate("Main", {
          screen: "AddPropertyResponse",
          params: { propertyScheduleData: nextdata },
        });
        // navigation.navigate("Main", {
        //   screen: "TabHome",
        //   params: {
        //     screen: "Home",
        //     params: {
        //       screen: "Profile",
        //     },
        //   },
        // });
      } else {
        const newMsg = data.message;
        const reason = data.reason;
        let ErrorMesssage = `${newMsg} \n ${reason}`;
        ShowNotification(ErrorMesssage, "warning");
      }
    } catch ({ message }) {
      ShowNotification(message, "error");
    }
  };

  const currentYear = new Date().getFullYear();

  let numberCount = [];
  for (let i = 1; i <= 10; i++) {
    numberCount.push(
      <TouchableOpacity
        onPress={() => {
          handleUnitSelect(i);
        }}
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "#ccc",
          paddingVertical: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        {unitSelect === i ? (
          <Image source={images.checktrue} />
        ) : (
          <Image source={images.checkfalse} />
        )}
        {/* <CheckBox
          style={RNCHECKBOXSTYLE}
          value={unitSelect === i ? true : false}
          checkedIcon="dot-circle-o"
          uncheckedIcon="circle-o"
        /> */}
        <CustomText
          textSize={14}
          textWeight={"normal"}
          textcolor={colors.black}
          displayText={`${i} Unit`}
          textStyle={{
            marginLeft: 25,
          }}
        />
      </TouchableOpacity>
    );
  }

  let interiorBedroom = [];
  for (let i = 1; i <= 5; i++) {
    interiorBedroom.push(
      <TouchableOpacity
        onPress={() => {
          handleInteriorBedRoom(i);
        }}
        style={[
          INTERIOCHECKER,
          {
            backgroundColor:
              interiorBedroomSelect === i ? colors.black : colors.transparent,
          },
        ]}
      >
        <CustomText
          textSize={16}
          textWeight={"normal"}
          textcolor={interiorBedroomSelect === i ? colors.white : colors.black}
          displayText={`${i == 5 ? "5+" : i}`}
          textStyle={{
            lineHeight: 24,
          }}
        />
      </TouchableOpacity>
    );
  }

  let interiorBathroom = [];
  for (let i = 1; i <= 5; i++) {
    interiorBathroom.push(
      <TouchableOpacity
        onPress={() => {
          handlegeneralAction(setInteriorBathroomSelect, i);
        }}
        style={[
          INTERIOCHECKER,
          {
            backgroundColor:
              interiorBathroomSelect === i ? colors.black : colors.transparent,
          },
        ]}
      >
        <CustomText
          textSize={16}
          textWeight={"normal"}
          textcolor={interiorBathroomSelect === i ? colors.white : colors.black}
          displayText={`${i == 5 ? "5+" : i}`}
          textStyle={{
            lineHeight: 24,
          }}
        />
      </TouchableOpacity>
    );
  }

  let interiorKitChen = [];
  for (let i = 1; i <= 5; i++) {
    interiorKitChen.push(
      <TouchableOpacity
        onPress={() => {
          handlegeneralAction(setInteriorKitchenSelect, i);
        }}
        style={[
          INTERIOCHECKER,
          {
            backgroundColor:
              interiorKitchenSelect === i ? colors.black : colors.transparent,
          },
        ]}
      >
        <CustomText
          textSize={16}
          textWeight={"normal"}
          textcolor={interiorKitchenSelect === i ? colors.white : colors.black}
          displayText={`${i == 5 ? "5+" : i}`}
          textStyle={{
            lineHeight: 24,
          }}
        />
      </TouchableOpacity>
    );
  }

  const range = (start, stop, step) =>
    Array.from(
      { length: (stop - start) / step + 1 },
      (_, i) => start + i * step
    );
  const converYearValue = range(currentYear, currentYear - 50, -1);

  let perparedYearValue = (converYearValue || []).map(
    (converYearValueItems) => {
      return (
        <TouchableOpacity
          onPress={() => {
            handleYearSelect(converYearValueItems);
          }}
          style={{
            borderBottomWidth: 1,
            borderBottomColor: "#ccc",
            paddingVertical: 10,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          {yearSelect === converYearValueItems.toString() ? (
            <Image source={images.checktrue} />
          ) : (
            <Image source={images.checkfalse} />
          )}

          {/* <CheckBox
            style={RNCHECKBOXSTYLE}
            value={yearSelect === converYearValueItems ? true : false}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
          /> */}
          <CustomText
            textSize={14}
            textWeight={"normal"}
            textcolor={colors.black}
            displayText={`Year ${converYearValueItems}`}
            textStyle={{
              marginLeft: 25,
            }}
          />
        </TouchableOpacity>
      );
    }
  );
  /* 
    
        
           
  */

  let perparedPropType = (fetchPropType || []).map((propTypeItems, index) => {
    return (
      <View
        style={{
          width: (index + 1) % 3 === 0 ? "100%" : "50%",
          justifyContent: "center",
          alignItems: "center",
          marginBottom: 24,
        }}
      >
        <NeomorphButton
          title={propTypeItems?.name}
          onPressHandler={() =>
            handlegeneralAction(setPropTypeSelected, propTypeItems?.name)
          }
          containerStyles={{
            borderColor:
              propTypeSelected === propTypeItems?.name
                ? colors.black
                : colors.transparent,
          }}
        />
      </View>
    );
  });

  /*  <ImageBackground
          resizeMode="stretch"
          source={images.card5}
          style={[CHECKWRAP3]}
        >
          <TouchableOpacity
            onPress={() => handlleArrayFill(buildingtestElement?.name)}
            style={[
              LIKECHECKBOX,
              {
                borderColor:
                  buildingAmenitiesSelected.includes(
                    buildingtestElement?.name
                  ) === true
                    ? colors.black
                    : colors.transparent,
              },
            ]}
          >
            <CustomText
              textSize={16}
              textWeight={"normal"}
              textcolor={colors.black}
              displayText={buildingtestElement?.name}
              textStyle={{
                lineHeight: 24,
                textAlign: "center",
              }}
              numberOfLines={2}
            />
          </TouchableOpacity>
        </ImageBackground>
        */

  let perparedBuildingAmenities = (buildingAmenities || []).map(
    (buildingtestElement) => {
      return (
        <NeomorphButton
          title={buildingtestElement?.name}
          onPressHandler={() => handlleArrayFill(buildingtestElement?.name)}
          containerStyles={{
            borderColor:
              buildingAmenitiesSelected.includes(buildingtestElement?.name) ===
              true
                ? colors.black
                : colors.transparent,
            marginBottom: 20,
          }}
        />
      );
    }
  );
  /*    <ImageBackground
          resizeMode="stretch"
          source={images.card5}
          style={[CHECKWRAP3]}
        >
          <TouchableOpacity
            onPress={() =>
              handlleArrayFill2(appliancesAndFinishingsElement?.name)
            }
            style={[
              LIKECHECKBOX,
              {
                borderColor:
                  appliancesAndFinishingsSelected.includes(
                    appliancesAndFinishingsElement?.name
                  ) === true
                    ? colors.black
                    : colors.transparent,
              },
            ]}
          >
            <CustomText
              textSize={16}
              textWeight={"normal"}
              textcolor={colors.black}
              displayText={appliancesAndFinishingsElement?.name}
              textStyle={{
                lineHeight: 24,
                textAlign: "center",
              }}
              numberOfLines={2}
            />
          </TouchableOpacity>
        </ImageBackground>
        */
  let perparedAppliancesAndFinishings = (appliancesAndFinishings || []).map(
    (appliancesAndFinishingsElement) => {
      return (
        <NeomorphButton
          title={appliancesAndFinishingsElement?.name}
          onPressHandler={() =>
            handlleArrayFill2(appliancesAndFinishingsElement?.name)
          }
          containerStyles={{
            borderColor:
              appliancesAndFinishingsSelected.includes(
                appliancesAndFinishingsElement?.name
              ) === true
                ? colors.black
                : colors.transparent,
            marginBottom: 20,
          }}
        />
      );
    }
  );

  // props

  const actionSheetYearBuiltBoddy = (
    <View style={{ maxHeight: Layout.window.height - 100, paddingTop: 15 }}>
      <ScrollView>
        <View style={{ padding: 20 }}>{perparedYearValue}</View>
      </ScrollView>
    </View>
  );
  const actionSheetUnitBody = (
    <View style={{ padding: 20 }}>{numberCount}</View>
  );
  const AppliancesandFinishings = [
    {
      label: "Air conditioning",
      value: "Air conditioning",
    },
    {
      label: "Tiled flooring",
      value: "Tiled flooring",
    },
    {
      label: "Hardwood flooring",
      value: "Hardwood flooring",
    },
    {
      label: "POP ceiling",
      value: "POP ceiling",
    },
    {
      label: "PVC ceiling",
      value: "PVC ceiling",
    },
    {
      label: "Oven",
      value: "Oven",
    },
    {
      label: "Bath tubs",
      value: "Bath tubs",
    },
    {
      label: "Cabinet & drawers",
      value: "Cabinet & drawers",
    },
    {
      label: "Cooking stove & gas",
      value: " Cooking stove & gas",
    },
    {
      label: "Heat extractor",
      value: "Heat extractor",
    },
    {
      label: "Fire alarm",
      value: "Fire alarm",
    },
    {
      label: "Wifi",
      value: "Wifi",
    },
    {
      label: "Granite tops",
      value: "Granite tops",
    },
    {
      label: "Glass showers",
      value: "Glass showers",
    },
    {
      label: "Washer/dryer",
      value: "Washer/dryer",
    },
  ];
  const BuildingAmenities = [
    {
      label: "Swimming pool",
      value: "Swimming pool",
    },
    {
      label: "Gym/fitness center",
      value: "Gym/fitness center",
    },
    {
      label: "Children playground",
      value: "Children playground",
    },
    {
      label: "Parking lot",
      value: "Parking lot",
    },
    {
      label: "Mall",
      value: "Mall",
    },
    {
      label: "Hospital",
      value: "Hospital",
    },
    {
      label: "School",
      value: "School",
    },
    {
      label: "Business Center",
      value: "Business Center",
    },
    {
      label: "Electricity",
      value: "Electricity",
    },
    {
      label: " Good & accessable roads",
      value: " Good & accessable roads",
    },
    {
      label: "Security",
      value: "Security",
    },
    {
      label: "Street lighting",
      value: "Street lighting",
    },
    {
      label: "Cleaning",
      value: "Cleaning",
    },
    {
      label: "Palto/balcony",
      value: "Palto/balcony",
    },
    {
      label: "Allowance of pets",
      value: "Allowance of pets",
    },
    {
      label: "CCTV Camera",
      value: "CCTV Camera",
    },
  ];
  const attashedImage = [
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726675/samples/Rectangle_uzu3mk.png",
    },
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726676/samples/Rectangle-1_ivkege.png",
    },
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726676/samples/Rectangle-2_glpu9u.png",
    },
    {
      imageUrl:
        "https://res.cloudinary.com/vaccinenudge/image/upload/v1638726676/samples/Rectangle-3_lwa9z5.png",
    },
  ];

  let LoopedBuildingAmenities = BuildingAmenities.map((item, index) => {
    return (
      <ImageBackground
        key={index}
        resizeMode="stretch"
        source={images.card5}
        style={[CHECKWRAP3]}
      >
        <TouchableOpacity
          style={[LIKECHECKBOX, { borderColor: colors.transparent }]}
        >
          <CustomText
            textSize={16}
            textWeight={"normal"}
            textcolor={colors.black}
            displayText={item.label}
            textStyle={{
              lineHeight: 18,
              textAlign: "center",
            }}
          />
        </TouchableOpacity>
      </ImageBackground>
    );
  });
  let LoopedAppliancesandFinishings = AppliancesandFinishings.map(
    (item, index) => {
      return (
        <ImageBackground
          key={index}
          resizeMode="stretch"
          source={images.card5}
          style={[CHECKWRAP3]}
        >
          <TouchableOpacity
            style={[LIKECHECKBOX, { borderColor: colors.transparent }]}
          >
            <CustomText
              textSize={16}
              textWeight={"normal"}
              textcolor={colors.black}
              displayText={item.label}
              textStyle={{
                lineHeight: 18,
                textAlign: "center",
              }}
            />
          </TouchableOpacity>
        </ImageBackground>
      );
    }
  );

  const _selectPhotoTapped = async () => {
    // const options = {
    //   quality: 1.0,
    //   maxWidth: 500,
    //   maxHeight: 500,
    //   storageOptions: {
    //     skipBackup: true
    //   },
    //   mediaType:'photo',
    //   // mediaType:'video',
    // };

    const options = {
      mediaType: "mixed",
    };

    launchImageLibrary(options, async (response) => {
      if (response.didCancel) {
        ShowNotification("User cancelled photo picker", "warning");
      } else if (response.errorMessage) {
        ShowNotification(response.errorMessage, "error");
      } else {
        const imagedata = response?.assets;
        imagedata.map((item, index) => {
          console.warn(item?.fileSize, "sss");
          setLoadingState(true);

          let fileSize = item?.fileSize / 1048576;

          let thissizeInMB = Math.round(fileSize);
          let Selected_image_localPath = { uri: item?.uri };
          ShowNotification("Please Wait", "warning");
          const source = {
            uri: item?.uri,
            type: item?.type,
            name: item?.fileName,
          };

          cloudinaryUpload(source, YOUR_CLOUDINARY_PRESET())
            .then((response) => {
              if (response !== false) {
                setImageArray([...imageArray, response]);
                ShowNotification("Done Saving your File", "success");
              }
              setLoadingState(false);
            })
            .catch((error) => {
              ShowNotification("Error Saving your File", "error");
              setLoadingState(false);
            });
        });
      }
    });
  };
  const onchangedate = (event, selectedDate) => {
    let currentDates = selectedDate + "";
    console.warn(currentDates);
    const myCurrentDate = convert(currentDates);
    console.warn(myCurrentDate);
    setUserSelectedDate(myCurrentDate);
  };
  const processDate = (event, selectedDate) => {
    setShowDate(false);
    let currentDates = selectedDate + "";
    console.warn(currentDates);
    const myCurrentDate = convert(currentDates);
    console.warn(myCurrentDate);
    setUserSelectedDate(myCurrentDate);
  };
  const onchangeTime = (event, selectedDate) => {
    let currentTime = selectedDate.toTimeString();
    console.warn(currentTime);
    const currentDates1 = currentTime.split(" ");
    console.warn(currentTime);

    setUserSelectedTime(currentDates1[0]);
  };
  const processTiime = (selectedDate: any) => {
    setShowTime(false);
    let currentTime = selectedDate.toTimeString();
    // console.warn(currentTime);
    const currentDates1 = currentTime.split(" ");
    // console.warn(currentDates1[0]);
    let newTimeSPlit = currentDates1[0].split(":");
    const NewTime = newTimeSPlit[0] + ":" + newTimeSPlit[1];
    console.warn(NewTime);

    setUserSelectedTime(NewTime);
  };

  const convert = (str) => {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  };

  const _selectFileToUpload = async () => {
    try {
      setLoading2State(true);
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf, DocumentPicker.types.plainText],
      });

      const source = {
        uri: res.uri,
        type: res.type,
        name: res.name,
      };
      cloudinaryUpload(source, YOUR_CLOUDINARY_PRESET())
        .then((response) => {
          if (response !== false) {
            console.warn("FileUrl");
            console.warn(response);
            console.warn("FileUrl_done");
            setFileArray([...fileArray, response]);
            setFileName(res?.name);
            ShowNotification("Done Saving your File", "success");
            setLoading2State(false);
          }
        })
        .catch((error) => {
          ShowNotification(error, "error");
          setLoading2State(false);
        });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        ShowNotification(err);
      } else {
        ShowNotification("An error occured", "error");
      }
      setLoading2State(false);
    }
  };
  const BgColor = colors.yellow;

  /* //  */

  /* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */

  /* </TouchableWithoutFeedback> */

  /* </KeyboardAvoidingView> */

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{ flex: 1 }}
    >
      <View style={[ROOT, { backgroundColor: colors.white2 }]}>
        {stepValue === "Step 1" ? (
          <Header
            leftIcon={true}
            title={`${stepValue} of 5`}
            rightIcon={false}
            onLeftPress={() => navigation.goBack()}
          />
        ) : stepValue === "Step 2" ? (
          <Header
            leftIcon={true}
            title={`${stepValue} of 5`}
            rightIcon={false}
            onLeftPress={() => handleStepValue("Step 1")}
          />
        ) : stepValue === "Step 3" ? (
          <Header
            leftIcon={true}
            title={`${stepValue} of 5`}
            rightIcon={false}
            onLeftPress={() => handleStepValue("Step 2")}
          />
        ) : stepValue === "Step 4" ? (
          <Header
            leftIcon={true}
            title={`${stepValue} of 5`}
            rightIcon={false}
            onLeftPress={() => handleStepValue("Step 3")}
          />
        ) : stepValue === "Step 5" ? (
          <Header
            leftIcon={true}
            title={`${stepValue} of 5`}
            rightIcon={false}
            onLeftPress={() => handleStepValue("Step 4")}
          />
        ) : (
          <></>
        )}

        <KeyboardAvoidContainer style={{ paddingVertical: 15 }}>
          {stepValue === "Step 1" ? (
            <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
              <Animatable.View animation={"slideInUp"}>
                <View style={[{ paddingHorizontal: 15, marginBottom: 20 }]}>

                  <CustomText
                    textSize={16}
                    textWeight={"bold"}
                    textcolor={colors.black}
                    displayText={"All fields are required. Kindly fill all details to proceed to the next step."}
                    textStyle={{ marginBottom: 6, textAlign: "left" }}
                  />

                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Select Property type"}
                    textStyle={{ marginBottom: 6, textAlign: "left" }}
                  />

                  <View style={[FLEXROW, { marginBottom: 10 }]}>
                    
                    <NeomorphButton
                      title="For Sale"
                      onPressHandler={() => handlePropertyType("SALE")}
                      containerStyles={{
                        borderColor:
                          selectPropertyType === "SALE"
                            ? colors.black
                            : colors.transparent,
                      }}
                    />

                    <NeomorphButton
                      title="For Rent"
                      onPressHandler={() => handlePropertyType("RENT")}
                      containerStyles={{
                        borderColor:
                          selectPropertyType === "RENT"
                            ? colors.black
                            : colors.transparent,
                      }}
                    />
                  </View>

                  
                </View>
                
                <View style={INPUTWRAP2}>
                  <CustomInput
                    inputText={"Address"}
                    inputTextWeight={"normal"}
                    inputTextSize={15}
                    inputTextcolor={colors.black}
                    inputTextSty={SETINPUTSTYLE}
                    textInputValue={addressValue}
                    onChangeText={(address1: string) =>
                      handlegeneralAction(setAddressValue, address1)
                    }
                    placeHolderText={""}
                    showIcon={false}
                  />
                </View>
                <View style={INPUTWRAP2}>
                  <CustomInput
                    inputText={"Listing title"}
                    inputTextWeight={"normal"}
                    inputTextSize={15}
                    inputTextcolor={colors.black}
                    inputTextSty={SETINPUTSTYLE}
                    textInputValue={listingTitle}
                    onChangeText={(cvalue: string) =>
                      handlegeneralAction(setListingTitle, cvalue)
                    }
                    placeHolderText={""}
                    showIcon={false}
                  />
                </View>
                
                <View style={[{ paddingHorizontal: 20, marginBottom: 2 }]}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Describe your property"}
                    textStyle={{ marginBottom: 6, textAlign: "left" }}
                  />
                </View>

                <View style={INPUTWRAP2}>
                  <CustomInput
                    inputText={"(Write a sales copy for your property)"}
                    inputTextWeight={"normal"}
                    inputTextSize={15}
                    inputTextcolor={colors.black}
                    multiline={true}
                    inputTextSty={SETINPUTSTYLE}
                    textInputValue={describedProperty}
                    onChangeText={(value: string) =>
                      handlegeneralAction(setDescribedProperty, value)
                    }
                    placeHolderText={""}
                    showIcon={false}
                  />
                </View>
                <View style={[{ paddingHorizontal: 20, marginBottom: 22 }]}>
                  <TouchableOpacity
                    disabled={
                      selectPropertyType === "" ||
                      addressValue === "" ||
                      listingTitle === "" ||
                      describedProperty === ""
                        ? true
                        : false
                    }
                    style={BTNSTYLE}
                    onPress={() => handleStepValue("Step 2")}
                  >
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.white}
                      displayText={"Next"}
                      textStyle={{ marginBottom: 6, textAlign: "center" }}
                    />
                  </TouchableOpacity>
                </View>
              </Animatable.View>
            </Animatable.View>
          ) : stepValue === "Step 2" ? (
            <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
              <Animatable.View animation={"slideInUp"}>
                <View style={[{ paddingHorizontal: 15, marginBottom: 20 }]}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Select Property type"}
                    textStyle={{ marginBottom: 6, textAlign: "left" }}
                  />

                  <View
                    style={[
                      FLEXROW,
                      { marginBottom: 1, justifyContent: "space-around" },
                    ]}
                  >
                    {perparedPropType}
                  </View>
                </View>
                <ImageBackground
                  resizeMode="stretch"
                  source={images.card6}
                  style={[
                    CHECKWRAP,
                    { maxWidth: "100%", height: 75, minWidth: "100%" },
                  ]}
                >
                  <TouchableOpacity
                    onPress={() => {
                      handleYearBuilt();
                    }}
                    style={[
                      LIKECHECKBOX,
                      {
                        borderColor: colors.transparent,
                        flexDirection: "row",
                        justifyContent: "space-between",
                        paddingLeft: 25,
                        paddingRight: 20,
                      },
                    ]}
                  >
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={`Year built  ${
                        yearSelect != "" ? yearSelect : ""
                      }`}
                      textStyle={{
                        lineHeight: 24,
                      }}
                    />

                    <Image source={images.arrDown} style={{ marginTop: 3 }} />
                  </TouchableOpacity>
                </ImageBackground>
                <ImageBackground
                  resizeMode="stretch"
                  source={images.card6}
                  style={[
                    CHECKWRAP,
                    {
                      maxWidth: "100%",
                      height: 75,
                      marginBottom: 10,
                      minWidth: "100%",
                    },
                  ]}
                >
                  <TouchableOpacity
                    onPress={() => {
                      handleUnit();
                    }}
                    style={[
                      LIKECHECKBOX,
                      {
                        borderColor: colors.transparent,
                        flexDirection: "row",
                        justifyContent: "space-between",

                        paddingLeft: 25,
                        paddingRight: 20,
                      },
                    ]}
                  >
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={`Available units ${unitSelect}`}
                      textStyle={{
                        lineHeight: 24,
                      }}
                    />

                    <Image source={images.arrDown} style={{ marginTop: 3 }} />
                  </TouchableOpacity>
                </ImageBackground>

                <View style={INPUTWRAP2}>
                  <CustomInput
                    inputText={"Square meter"}
                    inputTextWeight={"normal"}
                    inputTextSize={15}
                    inputTextcolor={colors.black}
                    inputTextSty={SETINPUTSTYLE}
                    textInputValue={SquareMeter}
                    onChangeText={(value: string) =>
                      handlegeneralAction(setSquareMeter, value)
                    }
                    placeHolderText={""}
                    showIcon={false}
                  />
                </View>
                {nonsenseprop === "" ? null : (
                  <View style={INPUTWRAP2}>
                    <CustomInput
                      inputText={"Listing price????"}
                      inputTextWeight={"normal"}
                      inputTextSize={15}
                      inputTextcolor={colors.black}
                      inputTextSty={SETINPUTSTYLE}
                      // keyboardType={"numeric"}
                      // onChangeText={(value: number) =>
                      //   handlegeneralAction(setListingprice, value)
                      // }
                      placeHolderText={""}
                      showIcon={false}
                    />
                  </View>
                )}

                <View style={INPUTWRAP2}>
                  <CustomInput
                    inputText={"Listing price ₦(Naira)"}
                    inputTextWeight={"normal"}
                    inputTextSize={15}
                    inputTextcolor={colors.black}
                    inputTextSty={SETINPUTSTYLE}
                    keyboardType={"numeric"}
                    textInputValue={listingprice}
                    onChangeText={(value: number) =>
                      handlegeneralAction(setListingprice, value)
                    }
                    placeHolderText={""}
                    showIcon={false}
                    isCurrency={true}
                  />
                </View>
                <View style={INPUTWRAP2}>
                  <CustomInput
                    inputText={"Service charge (for rent, if any) ₦(Naira)"}
                    inputTextWeight={"normal"}
                    inputTextSize={15}
                    inputTextcolor={colors.black}
                    inputTextSty={SETINPUTSTYLE}
                    textInputValue={serviceCharge}
                    onChangeText={(fvalue: number) =>
                      handlegeneralAction(setServiceCharge, fvalue)
                    }
                    placeHolderText={""}
                    showIcon={false}
                    keyboardType={"numeric"}
                    isCurrency={true}
                  />
                </View>

                <View style={[{ paddingHorizontal: 15, marginBottom: 2 }]}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Interior details"}
                    textStyle={{ marginBottom: 10, textAlign: "left" }}
                  />
                </View>
                <View style={[{ paddingHorizontal: 15, marginBottom: 2 }]}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Bedroom"}
                    textStyle={{ textAlign: "left" }}
                  />
                </View>

                <ImageBackground
                  resizeMode="stretch"
                  source={images.card6}
                  style={[CHECKWRAP2]}
                >
                  {interiorBedroom}
                </ImageBackground>
                <View style={[{ paddingHorizontal: 15, marginBottom: 2 }]}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Bathroom"}
                    textStyle={{ textAlign: "left" }}
                  />
                </View>

                <ImageBackground
                  resizeMode="stretch"
                  source={images.card6}
                  style={[CHECKWRAP2]}
                >
                  {interiorBathroom}
                </ImageBackground>
                <View style={[{ paddingHorizontal: 15, marginBottom: 2 }]}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Living Room"}
                    textStyle={{ textAlign: "left" }}
                  />
                </View>

                <ImageBackground
                  resizeMode="stretch"
                  source={images.card6}
                  style={[CHECKWRAP2]}
                >
                  {interiorKitChen}
                </ImageBackground>

                <View style={[{ paddingHorizontal: 20, marginBottom: 22 }]}>
                  <TouchableOpacity
                    disabled={
                      propTypeSelected === null ||
                      yearSelect === "" ||
                      unitSelect === "" ||
                      SquareMeter === "" ||
                      listingprice === 0 ||
                      serviceCharge === 0 ||
                      interiorBedroomSelect === 0 ||
                      interiorBathroomSelect === 0 ||
                      interiorKitchenSelect === 0
                        ? true
                        : false
                    }
                    style={BTNSTYLE}
                    onPress={() => handleStepValue("Step 3")}
                  >
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.white}
                      displayText={"Next"}
                      textStyle={{ marginBottom: 6, textAlign: "center" }}
                    />
                  </TouchableOpacity>
                </View>
                <CustomTooltip
                  actionSheetRef={actionSheeYearBuilttRef}
                  actionSheetBody={actionSheetYearBuiltBoddy}
                />
                <CustomTooltip
                  actionSheetRef={actionSheetUnitRef}
                  actionSheetBody={actionSheetUnitBody}
                />
              </Animatable.View>
            </Animatable.View>
          ) : stepValue === "Step 3" ? (
            <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
              <Animatable.View animation={"slideInUp"}>
                <View style={[{ paddingHorizontal: 15, marginBottom: 20 }]}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Building Amenities"}
                    textStyle={{ marginBottom: 6, textAlign: "left" }}
                  />
                </View>
                <View
                  style={[
                    FLEXROW,
                    { marginBottom: 1, justifyContent: "space-evenly" },
                  ]}
                >
                  {perparedBuildingAmenities}
                </View>
                <View style={[{ paddingHorizontal: 15, marginVertical: 20 }]}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Appliances and Finishings"}
                    textStyle={{ marginBottom: 6, textAlign: "left" }}
                  />
                </View>
                <View
                  style={[
                    FLEXROW,
                    { marginBottom: 1, justifyContent: "space-evenly" },
                  ]}
                >
                  {perparedAppliancesAndFinishings}
                </View>

                <View style={[{ paddingHorizontal: 20, marginBottom: 22 }]}>
                  <TouchableOpacity
                    disabled={
                      buildingAmenitiesSelected.length < 1 ||
                      appliancesAndFinishingsSelected.length < 1
                        ? true
                        : false
                    }
                    style={BTNSTYLE}
                    onPress={() => handleStepValue("Step 4")}
                  >
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.white}
                      displayText={"Next"}
                      textStyle={{ marginBottom: 6, textAlign: "center" }}
                    />
                  </TouchableOpacity>
                </View>
                {/* <CustomTooltip
                actionSheetRef={actionSheeYearBuilttRef}
                actionSheetBody={actionSheetYearBuiltBoddy}
              />
              <CustomTooltip
                actionSheetRef={actionSheetUnitRef}
                actionSheetBody={actionSheetUnitBody}
              /> */}
              </Animatable.View>
            </Animatable.View>
          ) : stepValue === "Step 4" ? (
            <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
              <Animatable.View animation={"slideInUp"}>
                <View style={[{ paddingHorizontal: 15, marginBottom: 40 }]}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Upload property images & documents"}
                    textStyle={{
                      marginBottom: 6,
                      textAlign: "left",
                      lineHeight: 24,
                    }}
                  />
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.dark}
                    displayText={
                      "(Property documents are required for verification. They will not be posted on Krib)."
                    }
                    textStyle={{ textAlign: "left" }}
                  />
                </View>
                <ImageBackground
                  resizeMode="stretch"
                  source={images.bg3}
                  style={{
                    flex: 1,
                    //   width: 395,
                    padding: 10,
                    paddingVertical: 40,
                    justifyContent: "center",
                  }}
                >
                  <View style={[{ paddingHorizontal: 15 }]}>
                    <CustomText
                      textSize={16}
                      textWeight={"bold"}
                      textcolor={colors.black}
                      displayText={"Attach Image"}
                      textStyle={{ textAlign: "center" }}
                    />
                  </View>

                  <View
                    style={[
                      FLEXROW,
                      {
                        marginBottom: 1,
                        justifyContent: "space-evenly",
                        position: "relative",
                      },
                    ]}
                  >
                    {loopedImageArray}
                  </View>

                  <View style={[FLEXROW2]}>
                    <TouchableOpacity onPress={() => _selectPhotoTapped()}>
                      {loadingState ? (
                        <ActivityIndicator size="large" color={BgColor} />
                      ) : (
                        <Image source={images.add} />
                      )}
                    </TouchableOpacity>
                  </View>
                </ImageBackground>

                <ImageBackground
                  resizeMode="stretch"
                  source={images.bg3}
                  style={{
                    flex: 1,
                    //   width: 395,
                    padding: 10,
                    justifyContent: "center",
                    marginTop: 20,
                  }}
                >
                  <View
                    style={[
                      CARDCONTAINER,
                      { justifyContent: "center", alignItems: "center" },
                    ]}
                  >
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={"Attach Document"}
                      textStyle={{ marginBottom: 6, textAlign: "center" }}
                    />

                    <Image source={images.upload} />
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.dark}
                      displayText={uploaderFileName}
                      textStyle={{ marginBottom: 6, textAlign: "center" }}
                    />
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.dark}
                      displayText={""}
                      textStyle={{ marginBottom: 6, textAlign: "center" }}
                    />
                    <TouchableOpacity onPress={() => _selectFileToUpload()}>
                      {loading2State ? (
                        <ActivityIndicator size="large" color={BgColor} />
                      ) : (
                        <Image source={images.upload2} />
                      )}
                    </TouchableOpacity>
                    <CustomText
                      textSize={13}
                      textWeight={"normal"}
                      textcolor={colors.dark}
                      displayText={"Supported file types: docx, xlsx, pdf."}
                      textStyle={{
                        marginBottom: 12,
                        textAlign: "center",
                        marginTop: 35,
                      }}
                    />
                  </View>
                </ImageBackground>

                <View style={[{ paddingHorizontal: 20, marginBottom: 22 }]}>
                  <TouchableOpacity
                    disabled={
                      imageArray.length < 1 || fileArray.length < 1
                        ? true
                        : false
                    }
                    style={BTNSTYLE}
                    onPress={() => handleStepValue("Step 5")}
                  >
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.white}
                      displayText={"Next"}
                      textStyle={{ marginBottom: 6, textAlign: "center" }}
                    />
                  </TouchableOpacity>
                </View>
              </Animatable.View>
            </Animatable.View>
          ) : (
            <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
              <Animatable.View animation={"slideInUp"}>
                <View
                  style={[
                    {
                      paddingHorizontal: 15,
                      marginBottom: 20,
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                    },
                  ]}
                >
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Schedule Consultation"}
                    textStyle={{ marginBottom: 40, textAlign: "left" }}
                  />
                  <View style={FLEXROWVERIFY}>
                    {/*  style={SETMARGIN} */}
                    <Image source={images.verify} />
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={
                        "Meet with Krib team to discuss selling strategies."
                      }
                      textStyle={{
                        textAlign: "left",
                        flexWrap: "wrap",
                        marginLeft: 10,
                        flex: 1,
                      }}
                    />
                  </View>

                  <View style={FLEXROWVERIFY}>
                    {/*  style={SETMARGIN} */}
                    <Image source={images.verify} />
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={
                        "Free architectural and aerial photograhphy to boost your property's appeal"
                      }
                      textStyle={{
                        textAlign: "left",

                        flexWrap: "wrap",
                        marginLeft: 10,
                        flex: 1,
                      }}
                    />
                  </View>

                  {/* <View style={FLEXROWVERIFY}>
                  <Image source={images.verify} style={SETMARGIN} />
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.black}
                    displayText={"We charge a 1% listing fee when you sell."}
                    textStyle={{ textAlign: "left", flex: 1 }}
                  />
                </View> */}

                  <View style={[FLEXROW, { marginBottom: 30, marginTop: 20 }]}>
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={"Calendar"}
                      textStyle={{ textAlign: "left", flex: 1 }}
                    />
                    <ImageBackground
                      resizeMode="stretch"
                      source={images.card5}
                      style={[CHECKWRAP]}
                    >
                      <TouchableOpacity
                        onPress={() => processDatePicker()}
                        style={{
                          justifyContent: "center",
                          alignContent: "center",
                        }}
                      >
                        <CustomText
                          textSize={16}
                          textWeight={"normal"}
                          textcolor={colors.black}
                          displayText={userSelectedDate}
                          textStyle={{ textAlign: "center" }}
                        />
                      </TouchableOpacity>
                    </ImageBackground>
                  </View>
                  <View style={[FLEXROW, { marginBottom: 10 }]}>
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.black}
                      displayText={"Time"}
                      textStyle={{ textAlign: "left", flex: 1 }}
                    />

                    <ImageBackground
                      resizeMode="stretch"
                      source={images.card5}
                      style={[CHECKWRAP]}
                    >
                      <TouchableOpacity
                        disabled={
                          userSelectedDate === "DD/MM/YY" ? true : false
                        }
                        onPress={() => processTimePicker()}
                        style={{
                          justifyContent: "center",
                          alignContent: "center",
                        }}
                      >
                        <CustomText
                          textSize={16}
                          textWeight={"normal"}
                          textcolor={colors.black}
                          displayText={userSelectedTime}
                          textStyle={{ textAlign: "center" }}
                        />
                      </TouchableOpacity>
                    </ImageBackground>
                  </View>
                </View>

                <View style={[{ paddingHorizontal: 20, marginBottom: 22 }]}>
                  <TouchableOpacity
                    style={BTNSTYLE}
                    onPress={() => processAddProperty()}
                  >
                    <CustomText
                      textSize={16}
                      textWeight={"normal"}
                      textcolor={colors.white}
                      displayText={
                        processing === true
                          ? "Processing...."
                          : "Schedule consultation"
                      }
                      textStyle={{ marginBottom: 6, textAlign: "center" }}
                    />
                  </TouchableOpacity>
                </View>
              </Animatable.View>
            </Animatable.View>
          )}
        </KeyboardAvoidContainer>
        {showDate && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode={"date"}
            is24Hour={true}
            style={{
              backgroundColor: 'grey'
            }}
            // display="default"
            display={Platform.OS == "ios" ? "spinner" : "default"}
            onChange={(event, selectedDate) => processDate(event, selectedDate)}
          />
        )}
        {showTime && (
          <DatePicker
            modal
            open={showTime}
            date={date}
            mode="time"
            display={Platform.OS == "ios" ? "spinner" : "default"}
            androidVariant="iosClone"
            onConfirm={(date) => {
              processTiime(date);
            }}
            onCancel={() => {
              setShowTime(false);
            }}
          />
         
        )}
      </View>
    </KeyboardAvoidingView>
  );
};

export default StepRegistration;
