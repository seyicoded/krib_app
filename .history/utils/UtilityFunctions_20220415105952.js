/* eslint-disable react-native/no-inline-styles */
// import axios from 'axios';
// import qs from 'qs';
import Spinner from "react-native-loading-spinner-overlay";
import React, { Component } from "react";
import {
  StyleSheet,
  PixelRatio,
  ActivityIndicator,
  Alert,
  Platform,
  View,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { PermissionsAndroid } from "react-native";
import Snackbar from "react-native-snackbar";
import DocumentPicker from "react-native-document-picker";

// import ToastStyle from '../StyleSheet/ToastStyle';
import AnimatedLoader from "react-native-animated-loader";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import Communications from "react-native-communications";
import RNRestart from 'react-native-restart';


export const getToken = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "Loggedinstate",
      })
      .then(async (ret) => {
        console.log(ret, "Loggedinstate");
        // resolve( ret);
        console.log("jijij");
        if (ret.isAvailable === true) {
          if (ret.storeData.isloggedin === true) {
            var token = await ret.storeData.accessToken;
            console.log(token);
            console.log("token");
            let loginSession = `${token}`;
            console.log(loginSession);
            resolve(loginSession);
            // return
          } else {
            resolve("jinadBABA");
          }
        } else {
          resolve("jinadBABA");
        }
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve("jinadBABA");
            break;
          case "ExpiredError":
            // TODO
            resolve("jinadBABA");
            break;
        }
      });
  });

export const ShowNotification = (Text, type = "success", delay = false) => {
  let bgcolour =
    type === "success" ? "green" : type === "warning" ? "orange" : "orange";
  let textcolour =
    type === "success" ? "#fff" : type === "warning" ? "#000" : "#000";
  if (delay) {
    setTimeout(() => {
      Snackbar.show({
        text: Text,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: bgcolour,
        textColor: textcolour,
        numberOfLines: 3,
      });
    }, 1000);
  } else {
    Snackbar.show({
      text: Text,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: bgcolour,
      textColor: textcolour,
      numberOfLines: 3,
    });
  }
};
export const Communicator = (action, param) => {
  if (param === "") {
    ShowNotification("Not available", "warning", true);
  } else if (action === "") {
    ShowNotification("Empty Action", "warning", true);
  } else {
    if (action === "phone") {
      Communications.phonecall(param, true);
    } else if (action === "web") {
      Communications.web(param);
    } else if (action === "message") {
    }
  }
};


export const YOUR_CLOUDINARY_NAME = () => {
  return "workbrook-hash";
};
export const YOUR_CLOUDINARY_PRESET = () => {
  return "awcal2vq";
};
export const YOUR_CLOUDINARY_PRESET_JobBanner = () => {
  return "dlqiv9hp";
};
export const YOUR_CLOUDINARY_PRESET_Document = () => {
  return "qfhydrgx";
};
export const YOUR_CLOUDINARY_PRESET_JobApplicationFiles = () => {
  return "JobApplicationFiles";
};

export const ReturnPartOfText = (text, length) => {
  let size = length;
  if (text.length < length) {
    size = text.length;
  }
  let capitalize = text.slice(0, 1).toUpperCase() + text.slice(1, size);
  return capitalize;
};
export const myspiner = (state, text = null) => {
  let spin = (
    <View style={styles.Spinnercontainer}>
      <AnimatedLoader
        visible={state}
        // overlayColor="rgba(255,255,255,0.75)"
        overlayColor="rgba(0.00,0.00,0.00,0.1)"
        // overlayColor="000000"
        // source={require("./loader.json")}
        // source={{ uri:'https://assets7.lottiefiles.com/datafiles/QeC7XD39x4C1CIj/data.json'}}
        animationStyle={styles.lottie}
        speed={1}
      />
    </View>
  );
  return spin;
};
export const SkeletonLoader = (state, text = null) => {
  let spin = (
    <SkeletonPlaceholder>
      <View
        style={{
          backgroundColor: "#fff",
          flex: 1,
          justifyContent: "flex-end",
          marginBottom: 0,
          padding: 19,
          height: 140,
        }}
      >
        <View style={styles.displayTitle} />
        <View style={styles.displayTitle} />
        <View style={styles.displayTitle} />
      </View>
    </SkeletonPlaceholder>
  );

  return spin;
};

export const MapLoader = (state, text = null) => {
  let spin = (
    <View style={styles.Spinnercontainer}>
      <Spinner
        visible={state}
        textContent={text ? text : "Please Wait..."}
        textStyle={{ color: "#fff" }}
        animation={"fade"}
        color={"#bd393b"}
        overlayColor={"#000"}
        cancelable={true}
      />
    </View>
  );
  return spin;
};

// export const ShowNotification = (Text, delay = false) => {
//   if (delay) {
//     setTimeout(() => {
//       Snackbar.show({
//         title: Text,
//         duration: Snackbar.LENGTH_LONG,
//       });
//     }, 1000);
//   } else {
//     Snackbar.show({
//       title: Text,
//       duration: Snackbar.LENGTH_LONG,
//     });
//   }
// };

// export const ShowNotification = (Text, type = "success", delay = false) => {
//   let bgcolour =
//     type === "success" ? "green" : type === "warning" ? "orange" : "red";
//   let textcolour =
//     type === "success" ? "#fff" : type === "warning" ? "#000" : "#fff";
//   if (delay) {
//     setTimeout(() => {
//       Snackbar.show({
//         text: Text,
//         duration: Snackbar.LENGTH_LONG,
//         backgroundColor: bgcolour,
//         textColor: textcolour,
//         numberOfLines: 3,
//       });
//     }, 1000);
//   } else {
//     Snackbar.show({
//       text: Text,
//       duration: Snackbar.LENGTH_LONG,
//       backgroundColor: bgcolour,
//       textColor: textcolour,
//       numberOfLines: 3,
//     });
//   }
// };

export const activityspiner = (state) => {
  let spin = (
    <View style={styles.activityIndicatorcontainer}>
      <ActivityIndicator
        animating={state}
        color="#bc2b78"
        size="large"
        style={styles.activityIndicator}
      />
    </View>
  );

  return spin;
};

export const PersistData = async (Mykey, data) => {
  try {
    if (typeof data === "object" && !Array.isArray(data) && data !== null) {
      data = JSON.stringify(data);
    }
    await AsyncStorage.setItem(
      Mykey, // Note: Do not use underscore("_") in key!
      data
    );
  } catch (e) {
    console.warn(e, "error new");
  }
};

export const GetPersistData = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      // console.warn(key);
      // console.warn(value);
      return value;
    } else {
      return false;
    }
  } catch (err) {
    // any exception including data not found
    // goes to catch()
    console.warn(err.message);
    switch (err.name) {
      case "NotFoundError":
        // alert("You have not register before")
        return {};
        break;
      case "ExpiredError":
        // TODO
        return {};
        break;
    }
  }
};
export const loggedinUserdetails = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "Loggedinstate",
      })
      .then((ret) => {
        resolve(ret);
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve({});
            break;
          case "ExpiredError":
            // TODO
            resolve({});
            break;
        }
      });
  });

export const GetCompanyKey = async () => {
  let data = await GetPersistData("UserOrganization");
  if (data !== false) {
    let NewData = data.details;
    console.log(NewData, "NewData");
    //console.log('NewData');
    let companykay = NewData.orgKey;
    return companykay;
  } else {
    return "nill";
  }
};
export const GetCompanyFullDetails = async () => {
  let data = await GetPersistData("UserOrganization");
  console.log(data, "GetCompanyFullDetails");
  if (data !== false) {
    let companydetails = data.details;

    return companydetails;
  } else {
    return null;
  }
};
export const GetcurrentBranch = async () => {
  let Currentbranch = await GetPersistData("MyCurrentBranch");

  if (Currentbranch === false) {
  } else {
    let branch_id = Currentbranch.value;

    return branch_id;
  }
  // 08033374342
  // let branch_id=4;

  /// return branch_id;
};

// export const GetUserType = (key) => {
//   let data = this.GetCurrentUserDetails();
//   let parsedData = JSON.parse(data);

//   let usertype = parsedData.user_type;
//   return usertype;
//   // console.log(parsedData);
//   // console.log('parsedData');
// };

export const getusertype = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "Loggedinstate",
      })
      .then((ret) => {
        resolve(ret);
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve({});
            break;
          case "ExpiredError":
            // TODO
            resolve({});
            break;
        }
      });
  });
export const getLodgedInDetailsProfile = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "loggedinUserdetails",
      })
      .then((ret) => {
        // resolve(ret);
        if (ret.isAvailable === true) {
          resolve(ret.storeData);
        } else {
          resolve(false);
        }
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            //      alert("You have not register before")
            resolve({});
            break;
          case "ExpiredError":
            //    alert("You have not register dd")
            resolve({});
            break;
        }
      });
  });
export const PersistLoginRecord = (profile, logginstate) => {
  let expiring = 1000 * 3600 * 1.5;
  PersistData("loggedinUserdetails", profile);
  PersistData("Loggedinstate", logginstate);
  setTimeout(function () {

    Alert.alert('Allow App Refresh to Login')
    RNRestart.Restart();
  }, 3000);

  // PersistData("Loggedinstate", logginstate, expiring);
};

export const requestCameraPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: "wookbrook Camera Permission",
        message:
          "wookbrook App needs access to your camera " +
          "so you can update your profile picture",
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
};

export const _DocumentPicker = async () => {
  try {
    const res = await DocumentPicker.pick({
      // type: [DocumentPicker.types.pdf, DocumentPicker.types.plainText],
      type: [DocumentPicker.types.pdf],
    });
    console.log(
      res.uri,
      res.type, // mime type
      res.name,
      res.size
    );
    const source = {
      uri: res.uri,
      type: res.type,
      name: res.name,
    };
    return res;
    // this.setState({
    //   documentName: res.name,
    //   isfileSelected: true,
    //   selectedDoc: source
    // })

    // setTimeout(() => {
    //   // this._uploadFileNow(res);
    //   this.cloudinaryUpload(source)

    // }, 1000);
  } catch (err) {
    if (DocumentPicker.isCancel(err)) {
      ShowNotification("Action canceled", true);
      return "cancel";
      // User cancelled the picker, exit any dialogs or menus and move on
    } else {
      // throw err;
      ShowNotification("An error occured", true);
      console.log(err);
      return false;
    }
  }
};
// export const  cloudinaryUpload = async (selectedDoc) => {
export const cloudinaryUpload = async (selectedDoc, upload_preset = "") =>
  new Promise((resolve, reject) => {
    // const { selectedDoc } = this.state;
    // let file=
    // DashboardStore._ToggleProcessing(true)
    let My_upload_preset =
      upload_preset === "" ? YOUR_CLOUDINARY_PRESET_Document() : upload_preset;
    console.log("here man");
    ShowNotification("Uploading your file...", true);

    const data = new FormData();
    data.append("file", selectedDoc);
    data.append("upload_preset", My_upload_preset);
    data.append("cloud_name", "workbrook-hash");
    fetch("https://api.cloudinary.com/v1_1/workbrook-hash/upload", {
      // https://api.cloudinary.com/v1_1/workbrook-hash/image/upload
      method: "post",
      body: data,
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("<>>><<");
        console.log(data);
        let DocUrl = data.secure_url;
        // DashboardStore._updateParameterV2(DocUrl, "resume", "profileupdatedata");
        ShowNotification("Done Uploading ....", true);
        // DashboardStore.__UpdateProfile();
        // this.SavetoDatabase(result)
        // setPhoto(data.secure_url)
        resolve(DocUrl);
        // return DocUrl
      })
      .catch((err) => {
        console.log(err);
        ShowNotification("An Error Occured While Uploading", true);
        Alert.alert("An Error Occured While Uploading");
        resolve(false);
      });
    // }
  });

export const removePersistData = async (key) => {
  try {
    await AsyncStorage.removeItem(key)
  }
  catch (e) {

    // remove error 
  }
  console.log('Done.')
}
// export const getusertypeold = () => {

//  return  loggedinUserdetails().then(
//     (resp) => {
// let isloggedin =resp.isloggedin;
// console.log(resp);
// if(isloggedin===true){
// let usertype = resp.usertype
// console.warn(usertype);
// console.warn('usertype lddkdkdddkdkdkdkdkdkdkdkdkdk');
// // global.usertype= usertype;
// return 'Usertypemi';

// }
// else{
//   return 'notin';
// }

// }
//   )
//   .catch((error) => {
//     console.warn(`get usertpe function ${error}`);
//   });

// }

const styles = StyleSheet.create({
  lottie: {
    width: 100,
    height: 100,
  },

  activityIndicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 80,
  },
  activityIndicatorcontainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 70,
    backgroundColor: "#000000",
  },
  Spinnercontainer: {
    // flex: 1,
    justifyContent: "center",
    textAlign: "center",
    paddingTop: 30,
    marginBottom: 30,
    backgroundColor: "#ecf0f1",
    padding: 8,
    // top: 0, bottom: 0, left: 0, right: 0
    bottom: 50,
  },
  displayTitle: {
    // flex:1,
    paddingTop: 2,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    // backgroundColor: 'rgba(0,0,0,0.4)',
    // paddingLeft:10,
  },
});
