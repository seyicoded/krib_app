/* eslint-disable react-native/no-inline-styles */

import React, { Component } from "react";
import { Text, StyleSheet } from "react-native";
// import { withNavigation } from 'react-navigation';

function CustomText(props: {
  displayText: string;
  textWeight: string;
  textSize: number;
  textcolor: string;
  textStyle: any;
}) {
  const textSize = props.textSize;
  const textWeight = props.textWeight;
  const textcolor = props.textcolor;

  return (
    <Text
      {...props}
      style={[
        // props?.textStyle,
        { color: textcolor, fontWeight: textWeight, fontSize: textSize },
      ]}
    >
      {props.displayText}
    </Text>
  );
}
export default CustomText;

// export default withNavigation(RecruiterLanding);
