import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  useWindowDimensions,
  ViewStyle,
  Platform
} from "react-native";
import React from "react";
import { Shadow, Neomorph } from "react-native-neomorph-shadows";
import { colors } from "../../theme/colors";
import CustomText from "../customText/CustomText";

const NeomorphButton = (props: {
  title: string;
  onPressHandler: () => void;
  containerStyles: any;
}) => {
  let { title, containerStyles, onPressHandler } = props;
  let size: number = 12;

  const { width } = useWindowDimensions();

  return (
    <TouchableOpacity
      style={[
        {
          // borderColor: borderColor,
          borderWidth: 1,
          borderRadius: 3,
          paddingBottom: 3,
          paddingRight: 3,
        },
        {
          shadowColor: 'rgba(0, 0, 0, 0.4)',
          shadowOffset: {width: 1, height: 1},
          shadowOpacity: 1,
          shadowRadius: 4
        },
        containerStyles,
      ]}
      onPress={onPressHandler}
    >
      {
        (Platform.OS == 'ios') ? 
        <>
        <>
          <CustomText          
            textSize={16}
            textWeight={"normal"}
            textcolor={colors.black}
            displayText={title}
            textStyle={{
              lineHeight: 18,
              textAlign :"center",
            }}
            
          />
        </>
      </>
      :
        <Neomorph
        inner
        useArt={(Platform.OS == 'ios') ? true:false}
        darkShadowColor="#FFFFFF"
        style={{
          shadowOffset: { width: -10, height: -10 },
          shadowOpacity: 1,
          shadowRadius: 5,
          borderRadius: 4,
          backgroundColor: colors.white,
          width: (width / 100) * 44,
          height: 48,
        }}
      >
        <Neomorph
          useArt={Platform.OS == 'ios' ? true:false}
          swapShadows
          style={{
            shadowOpacity: 0.3,
            shadowRadius: 4,
            borderRadius: 4,
            backgroundColor: colors.white2,
            width: (width / 100) * 44,
            height: 48,
            marginTop: 2,
            marginLeft: 2,
            // borderColor: "black",
            // borderWidth: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <CustomText
            textSize={16}
            textWeight={"normal"}
            textcolor={colors.black}
            displayText={title}
            textStyle={{
              lineHeight: 18,
              textAlign :"center"
            }}
            
          />
        </Neomorph>
      </Neomorph>
      }
      
    </TouchableOpacity>

    // <TouchableWithoutFeedback>
    //   <View style={[styles.container]}>
    //     <View style={[styles.inner__shadow]}>
    //       <Text>{title}</Text>
    //     </View>
    //   </View>
    // </TouchableWithoutFeedback>
  );
};

export default NeomorphButton;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 47,
    borderRadius: 10,

    shadowColor: "#ffffff",
    shadowOffset: {
      width: 10,
      height: 10,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
    justifyContent: "center",
    alignItems: "center",
    //   backgroundColor: "#ffffff",
  },
  inner__shadow: {
    width: "100%",
    height: "100%",
    borderRadius: 10,

    shadowColor: "#ffffff",
    shadowOffset: {
      width: 0,
      height: -1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,

    justifyContent: "center",
    alignItems: "center",
  },
});
