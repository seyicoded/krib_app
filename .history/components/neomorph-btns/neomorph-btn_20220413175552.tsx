import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  useWindowDimensions,
  ViewStyle,
  Platform
} from "react-native";
import React from "react";
import { Shadow, Neomorph } from "react-native-neomorph-shadows";
import { colors } from "../../theme/colors";
import CustomText from "../customText/CustomText";

const NeomorphButton = (props: {
  title: string;
  onPressHandler: () => void;
  containerStyles: any;
}) => {
  let { title, containerStyles, onPressHandler } = props;
  let size: number = 12;

  const { width } = useWindowDimensions();

  if(Platform.OS == 'android'){
    // start android
    return (
      <TouchableOpacity
        style={[
          {
            // borderColor: borderColor,
            borderWidth: 1,
            borderRadius: 3,
            paddingBottom: 3,
            paddingRight: 3,
          },
          {
            // shadowColor: 'rgba(0, 0, 0, 0.6)',
            shadowOffset: {width: 0.61, height: 0.61},
            shadowOpacity: 0.51,
            shadowRadius: 2.22,
            borderColor: 'rgba(0, 0, 0, 0.5)',
            borderWidth: 1,
            paddingTop: 25,
            paddingBottom: 25,
            backgroundColor: 'rgba(250, 250, 250, 0.73)',
            elevation: 5,
            width: 'auto',
            marginRight: '0.5%',
            marginTop: '0.5%',
            maxWidth: '79.5%',
            minWidth: '46%',
          },
          containerStyles,
        ]}
        onPress={onPressHandler}
      >
        <View style={{backgroundColor: 'none'}}>
          <CustomText          
              textSize={16}
              textWeight={"normal"}
              textcolor={colors.black}
              displayText={title}
              textStyle={{
                lineHeight: 18,
                textAlign :"center",
                backgroundColor: 'transparent',
              }}
              
            />
          </View>
      </TouchableOpacity>
      )
    // end android
  }else{
    return (
      <TouchableOpacity
        style={[
          {
            // borderColor: borderColor,
            borderWidth: 1,
            borderRadius: 3,
            paddingBottom: 3,
            paddingRight: 3,
          },
          {
            // shadowColor: 'rgba(0, 0, 0, 0.6)',
            shadowOffset: {width: 0.61, height: 0.61},
            shadowOpacity: 0.51,
            shadowRadius: 2.22,
            borderColor: 'rgba(0, 0, 0, 0.5)',
            borderWidth: 1,
            paddingTop: 25,
            paddingBottom: 25,
            backgroundColor: 'rgba(250, 250, 250, 0.73)',
            elevation: 5,
            width: 'auto',
            marginRight: '0.5%',
            marginTop: '0.5%',
            maxWidth: '79.5%',
            minWidth: '46%'
          },
          containerStyles,
        ]}
        onPress={onPressHandler}
      >
        
            <CustomText          
              textSize={16}
              textWeight={"normal"}
              textcolor={colors.black}
              displayText={title}
              textStyle={{
                lineHeight: 18,
                textAlign :"center",
                backgroundColor: 'transparent',
              }}
              
            />
          
        
      </TouchableOpacity>
  
      // <TouchableWithoutFeedback>
      //   <View style={[styles.container]}>
      //     <View style={[styles.inner__shadow]}>
      //       <Text>{title}</Text>
      //     </View>
      //   </View>
      // </TouchableWithoutFeedback>
    );
  }

  
};

export default NeomorphButton;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 47,
    borderRadius: 10,

    shadowColor: "#ffffff",
    shadowOffset: {
      width: 10,
      height: 10,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
    justifyContent: "center",
    alignItems: "center",
    //   backgroundColor: "#ffffff",
  },
  inner__shadow: {
    width: "100%",
    height: "100%",
    borderRadius: 10,

    shadowColor: "#ffffff",
    shadowOffset: {
      width: 0,
      height: -1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,

    justifyContent: "center",
    alignItems: "center",
  },
});
