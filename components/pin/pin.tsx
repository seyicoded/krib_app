import * as React from "react";
import { useEffect, useRef, useState } from "react";
// import Modal from "../../components/modal";
// import Header from "../../components/header";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Animated,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
  Keyboard,
} from "react-native";

import * as Animatable from "react-native-animatable";
import { AmountInput } from "../../components/inputs/amountInput";
import { TextField } from "../../components/text-field";

import { translate } from "../../i18n";

// components

// styles
// import { NavigationScreenProps } from "react-navigation";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import Keypad from "../../components/button/keypad";
import md5 from "react-native-md5";
import { ApplicationState } from "../../redux";
import { notify, savePin } from "../../redux/auth";
import { color } from "react-native-reanimated";

// import Header from "../../components/newHeader";

// util

interface DispatchProps {
  savePin: (pin: string) => void;
  notify: (message: string, type: string) => void;
  handlePress;
}

interface StateProps { }

const BUTTON: ViewStyle = {
  backgroundColor: colors.alerzoBlue,
  padding: 15,
  borderRadius: 10,
  borderColor: colors.alerzoBlue,
  borderWidth: 1,
  width: Layout.window.width / 1.5,
  alignSelf: "center",
  marginBottom: 20,
  marginTop: Layout.window.height / 20,
};

const KEYPAD_INPUT: TextStyle = {
  color: colors.greyThree,
  fontSize: 18,
  fontWeight: "bold",
};
const EDIT_BUTTON: ViewStyle = {
  alignSelf: "center",
  alignItems: "center",
  justifyContent: "space-between",
  borderRadius: 10,
  width: Layout.window.height / 3,
  height: 50,
  backgroundColor: colors.alerzoBlue,
  marginBottom: 50,
  flexDirection: "row",
};

const EDIT_BUTTON_TEXT: TextStyle = {
  fontSize: 14,
  fontFamily: fonts.GilmerMedium,
  color: colors.white,
  marginRight: 20,
  textAlign: "center",
  width: "100%",
};

function PinModule(props: {}) {
  const { navigation, savePin, notify, handlePress, handleContinue, headerText = "herder here", bodyText = "hey" } = props;
  // redux
  //   const [dispatch, selectStore] = useReduxStore("auth");
  //   const [dispatchWallet, selectStoreWallet] = useReduxStore("wallet");

  // const [isModalVisible, setModalVisible] = useState(false);
  let otpTextInputOne = useRef(null);
  let otpTextInputTwo = useRef(null);
  let otpTextInputThree = useRef(null);
  let otpTextInputFour = useRef(null);
  let [currentTab, setCurrentTab] = useState(false);
  let [inputOne, setInputOne] = useState("");
  let [inputTwo, setInputTwo] = useState("");
  const [loading, setLoading] = useState(false);

  let [inputThree, setInputThree] = useState("");
  let [inputFour, setInputFour] = useState("");
  let [pinView, setPinView] = useState("firstView");
  const [isModalVisible, setModalVisible] = useState(false);
  const [switchTab, setSwitchTab] = useState("savingAccount");
  const handleSwitchTab = (params) => {
    setSwitchTab(params);
  };

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  useEffect(() => {
    StatusBar.setBarStyle("light-content");
    Platform.OS === "android" && StatusBar.setBackgroundColor(colors.black);
  });
  const toggleScreen = () => {
    setCurrentTab(!currentTab);
  };
  const process = () => { };
  const validatePin = () => {
    if (!loading) {
      setLoading(true);
    }
  };

  return (
    <>
      <View
        style={{
          flex: 1,
          backgroundColor: colors.backgroundTwo,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
        }}
      >
        <View
          style={{
            // height: "100%",
            justifyContent: "space-around",
            minHeight: Layout.window.height - 200,
          }}
        >
          <Image
            style={{
              marginTop: 20,
              alignSelf: "center",
            }}
            source={images.enterPinDivider}
            resizeMode="cover"
            resizeMethod="auto"
          />

          <Text
            style={{
              fontFamily: fonts.GilmerMedium,
              color: colors.greyThree,
              textAlign: "center",
              marginTop: 18,
            }}
          >
            {/* {translate("pin.enterCardPin")} */}
            {headerText}
          </Text>

          <Text
            style={{
              fontFamily: fonts.GilmerMedium,
              color: colors.greyThree,
              textAlign: "center",
              marginTop: 10,
              fontSize: 12,
              width: '60%',
              marginLeft: 'auto',
              marginRight: 'auto',
            }}
          >
            {/* {translate("pin.enterCardText1")} */}
            {bodyText}
          </Text>

          {/* <Text
            style={{
              fontFamily: fonts.GilmerMedium,
              color: colors.greyThree,
              textAlign: "center",
              marginTop: 0,
              fontSize: 12,
            }}
          >
            {translate("pin.enterCardText2")}
          </Text> */}

          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              marginTop: 30,
              marginBottom: 30,
            }}
          >
            <View
              style={{
                width: Layout.window.width / 5.8,
                marginHorizontal: 5,
              }}
            >
              <TextField
                textContentType={"oneTimeCode"}
                clearButtonMode={"never"}
                name="amount"
                keyboardType="number-pad"
                value={inputOne}
                onChangeText={(value: string) => {
                  setInputOne(value);
                  if (value.length === 6) {
                    console.log(value.split(""));
                    let numberArray = value.split("");
                    setInputFour(numberArray[3]);
                    setInputThree(numberArray[2]);
                    setInputTwo(numberArray[1]);
                    setInputOne(numberArray[0]);
                    Keyboard.dismiss();
                  }

                  if (value.length === 1) otpTextInputTwo.current.focus();
                }}
                returnKeyType="next"
                placeholder={`${translate("pinText.pinPlaceholder")}`}
                placeholderTextColor={colors.greyTwo}
                style={{
                  borderColor: colors.backgroundTwo,
                  backgroundColor: colors.backgroundTwo,
                }}
                inputStyle={{
                  color: colors.alerzoBlack,
                  textAlign: "center",
                  width: Layout.window.width / 6,
                }}
                forwardedRef={otpTextInputOne}
              />
            </View>

            <View
              style={{
                width: Layout.window.width / 5.8,
                marginHorizontal: 5,
                backgroundColor: colors.backgroundTwo,
              }}
            >
              <TextField
                maxLength={1}
                clearButtonMode={"never"}
                name="amount"
                keyboardType="number-pad"
                value={inputTwo}
                onChangeText={(value: string) => {
                  setInputTwo(value);
                  if (value.length > 0)
                    return otpTextInputThree.current.focus();
                  if (value.length < 1) return otpTextInputOne.current.focus();
                }}
                returnKeyType="next"
                placeholder={`${translate("pinText.pinPlaceholder")}`}
                placeholderTextColor={colors.greyTwo}
                style={{
                  borderColor: colors.backgroundTwo,
                  backgroundColor: colors.backgroundTwo,
                }}
                inputStyle={{
                  color: colors.alerzoBlack,
                  textAlign: "center",
                  width: Layout.window.width / 6,
                }}
                forwardedRef={otpTextInputTwo}
              />
            </View>

            <View
              style={{
                width: Layout.window.width / 5.8,
                marginHorizontal: 5,
              }}
            >
              <TextField
                maxLength={1}
                clearButtonMode={"never"}
                name="amount"
                keyboardType="number-pad"
                value={inputThree}
                onChangeText={(value: string) => {
                  setInputThree(value);
                  if (value.length > 0) return otpTextInputFour.current.focus();
                  if (value.length < 1)
                    return otpTextInputThree.current.focus();
                }}
                returnKeyType="next"
                placeholder={`${translate("pinText.pinPlaceholder")}`}
                placeholderTextColor={colors.greyTwo}
                style={{
                  borderColor: colors.backgroundTwo,
                  backgroundColor: colors.backgroundTwo,
                }}
                inputStyle={{
                  color: colors.alerzoBlack,
                  textAlign: "center",
                  width: Layout.window.width / 6,
                }}
                forwardedRef={otpTextInputThree}
              />
            </View>

            <View
              style={{
                width: Layout.window.width / 5.8,
                marginHorizontal: 5,
              }}
            >
              <TextField
                maxLength={1}
                clearButtonMode={"never"}
                name="amount"
                keyboardType="number-pad"
                value={inputFour}
                onChangeText={(value: string) => {
                  setInputFour(value);
                  if (value.length > 0) {
                    handleContinue()
                    Keyboard.dismiss();
                    return otpTextInputFour.current.focus();

                  }

                  // if (value.length > 0) return otpTextInputFour.current.focus();
                  if (value.length < 1) return otpTextInputFour.current.focus();
                }}
                returnKeyType="next"
                placeholder={`${translate("pinText.pinPlaceholder")}`}
                placeholderTextColor={colors.greyTwo}
                style={{
                  borderColor: colors.backgroundTwo,
                  backgroundColor: colors.backgroundTwo,
                }}
                inputStyle={{
                  color: colors.alerzoBlack,
                  textAlign: "center",
                  width: Layout.window.width / 6,
                }}
                forwardedRef={otpTextInputFour}
              />
            </View>
          </View>

          <Keypad handlePress={handleContinue} />
        </View>
      </View>
    </>
  );
}

export default PinModule;
