import React, { useState } from "react";
import {
  Button,
  NativeSyntheticEvent,
  NativeTouchEvent,
  View,
  ViewStyle,
  TouchableOpacity,
  Image,
} from "react-native";
import Modal from "react-native-modal";
import { colors, fonts, images } from "../../theme";

const DESTINATIONPICKERWRAP: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  borderWidth: 0,
  borderColor: colors.alerzoGreyFive,
  borderRadius: 10,
  marginTop: 10,
  marginBottom: 20,
  flexWrap: "wrap",
};
const CARDSTY: ViewStyle = {
  padding: 13,
  minWidth: 75,
  maxWidth: "18%",
  borderWidth: 1,
  height: 50,
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  borderRadius: 8,
};
const HEADER_TWO: TextStyle = {
  fontFamily: fonts.GilmerMedium,
  fontSize: 14,
  color: colors.greyThree,
  fontWeight: "bold",
};

function AirtimeCard(props: {
  // handleOnPress: (ev: NativeSyntheticEvent<NativeTouchEvent>) => void;
}) {
  const [selectCard, setselectCard] = useState("");

  const handleSelectedcard = (params) => {
    setselectCard(params);
  };

  return (
    <View style={DESTINATIONPICKERWRAP}>
      <TouchableOpacity
        style={[
          CARDSTY,
          {
            backgroundColor:
              selectCard === "mtn_Icon" ? colors.activeCard : colors.white,
            borderColor:
              selectCard === "mtn_Icon"
                ? colors.activeCardBorder
                : colors.white,
          },
        ]}
        onPress={() => handleSelectedcard("mtn_Icon")}
      >
        <Image style={{ width: 30, height: 30 }} source={images.mtnIcon} />
      </TouchableOpacity>

      <TouchableOpacity
        style={[
          CARDSTY,
          {
            backgroundColor:
              selectCard === "airtel_Icon" ? colors.activeCard : colors.white,
            borderColor:
              selectCard === "airtel_Icon"
                ? colors.activeCardBorder
                : colors.white,
          },
        ]}
        onPress={() => handleSelectedcard("airtel_Icon")}
      >
        <Image style={{ width: 28, height: 33 }} source={images.airtelIcon} />
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          CARDSTY,
          {
            backgroundColor:
              selectCard === "glo_Icon" ? colors.activeCard : colors.white,
            borderColor:
              selectCard === "glo_Icon"
                ? colors.activeCardBorder
                : colors.white,
          },
        ]}
        onPress={() => handleSelectedcard("glo_Icon")}
      >
        <Image style={{ width: 33, height: 33 }} source={images.gloIcon} />
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          CARDSTY,
          {
            backgroundColor:
              selectCard === "mobile_9" ? colors.activeCard : colors.white,
            borderColor:
              selectCard === "mobile_9"
                ? colors.activeCardBorder
                : colors.white,
          },
        ]}
        onPress={() => handleSelectedcard("mobile_9")}
      >
        <Image style={{ width: 21, height: 37 }} source={images.mobile9} />
      </TouchableOpacity>
    </View>
  );
}

export default AirtimeCard;
