import React, { useState } from "react";
import { NativeSyntheticEvent, NativeTouchEvent, View } from "react-native";
import { translate } from "../../i18n";
import { colors, images } from "../../theme";
import StarRating from "react-native-star-rating";

// import { Text, TextStyle, Image, TouchableOpacity } from "react-native";

function StarRatingModule(props: Props) {
  const { navigation, isLoading, signInUserAsync } = props;
  const [starCount, setStarCount] = useState(3.5);

  const onStarRatingPress = (rating) => {
    setStarCount(rating);
  };

  return (
    <StarRating
      disabled={false}
      starSize={10}
      maxStars={5}
      rating={starCount}
      starStyle={{ color: colors.alerzoOrange }}
      selectedStar={(rating) => onStarRatingPress(rating)}
    />
  );
}

export default StarRatingModule;
