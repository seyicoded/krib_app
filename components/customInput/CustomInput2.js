import React, {Component, Fragment} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import accountStore from '../../../stores/Account';

import {Item, Input, Label} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';
const blackColor = accountStore.BlackColor;

export default class GenInputAlt extends Component {
  state = {};

  render() {
    const {selectedItems} = this.state;
    return (
      <View style={{width: '100%'}}>
        

        <Item
          regular
          style={(this, this.props.inputSty || styles.GeneralInputSty)}>
          {/* <Label style={{color: "#000114", fontSize: 12}}>Username</Label> */}
          <Input
            {...this.props}
            // keyboardType={this.props.keyboardType || 'default'}
            // secureTextEntry={this.props.secureTextEntry || false}
            style={this.props.innerInputSTy || styles.GeneralInnerInputSty}
            // placeholder={this.props.placeholderText || ''}
            // onBlur={this.props.handelTextBlur}
          />
          {this.props.showIcone == true ? (
            <TouchableOpacity {...this.props}>
              <Image
                source={{
                  uri:
                    'https://res.cloudinary.com/tomzyadexcloud/image/upload/v1609843981/edvesMobile2/eye.png',
                }}
                style={styles.iconSTy}
              />
            </TouchableOpacity>
          ) : (
            <Fragment />
          )}
        </Item>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  GeneralInputSty: {
    backgroundColor: '#fff',
    borderRadius: 0,
    width: '100%',
    height: 44,
    color: '#333',
    borderWidth: 0,
    borderColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#E2E3FE',
    marginBottom: 16,
  },
  newInputcustomSty: {
    backgroundColor: '#fff',
    borderRadius: 0,
    width: '100%',
    height: 44,
    color: '#333',
    borderWidth: 0,
    borderColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#E2E3FE',
    marginBottom: 16,
  },
  GeneralInnerInputSty: {
    fontSize: 12,
    height: 35,
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    backgroundColor: 'transparent',
    color: blackColor,
  },
  GeneralTextSty: {
    color: '#000114',
    fontSize: 12,
    // marginBottom: 1,
    // marginLeft: 10,
  },
  iconSTy: {
    width: 20,
    height: 20,
  },
});
