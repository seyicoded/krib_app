import React, { Component, Fragment, useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Image,
  TextInput,
  ImageBackground,
} from "react-native";
import CustomText from "../customText/CustomText";
import { colors, fonts, images } from "../../theme";
import { TouchableOpacity } from "react-native-gesture-handler";

function CustomInput(props: {
  inputText: string;
  inputTextWeight: string;
  inputTextSize: number;
  inputTextcolor: string;
  placeHolderText: string;
  inputTextSty: any;
  showIcon: boolean;
  isCurrency?: boolean;
  textInputValue?: string;
  onChangeText?: (data: number) => void;
}) {
  const [value, setValue] = useState("");
  useEffect(() => {
    if (props.textInputValue && props.isCurrency) {
      let ammount: string = props.textInputValue.toString().split(",").join("");
      const commaFormatted = String(ammount).replace(
        /(\d)(?=(\d{3})+(?!\d))/g,
        "$1,"
      );
      setValue(commaFormatted);
    }
    return () => {
      setValue("");
    };
  }, [props.textInputValue]);

  return (
    <View style={{ width: "100%" }}>
      {props.inputText ? (
        <CustomText
          textSize={props.inputTextSize}
          textWeight={props.inputTextWeight}
          textcolor={props.inputTextcolor}
          displayText={props.inputText || ""}
          textStyle={props.inputTextSty || styles.GeneralTextSty}
        />
      ) : (
        <Fragment />
      )}

      <ImageBackground
        resizeMode="stretch"
        source={images.inputCard}
        style={{ width: "100%", height: 64 }}
        // style={(this, this.props.inputSty || styles.GeneralInputSty)}
      >
        {props.isCurrency ? (
          <TextInput
            placeholder={props.placeHolderText}
            value={value}
            style={{ height: "100%", paddingHorizontal: 10 }}
            keyboardType="numeric"
            onChangeText={(val) => {
              let ammount: string = val.split(",").join("");
              const commaFormatted = String(ammount).replace(
                /(\d)(?=(\d{3})+(?!\d))/g,
                "$1,"
              );
              setValue(commaFormatted);
              let ammountNumber = parseInt(ammount);
              props.onChangeText(ammountNumber);
            }}
          />
        ) : (
          <TextInput
            value={props.textInputValue}
            placeholder={props.placeHolderText}
            style={{ height: "100%", paddingHorizontal: 10 }}
            {...props}
          />
        )}

        {props.showIcon == true ? (
          <TouchableOpacity {...props}>
            <Image
              source={{
                uri: "https://res.cloudinary.com/tomzyadexcloud/image/upload/v1609843981/edvesMobile2/eye.png",
              }}
              style={styles.iconSTy}
            />
          </TouchableOpacity>
        ) : (
          <Fragment />
        )}
      </ImageBackground>
    </View>
  );
}
export default CustomInput;

const styles = StyleSheet.create({
  GeneralInputSty: {
    backgroundColor: "#fff",
    borderRadius: 0,
    width: "100%",
    height: 44,
    color: "#333",
    borderWidth: 0,
    borderColor: "#fff",
    borderBottomWidth: 1,
    borderBottomColor: "#E2E3FE",
    marginBottom: 16,
  },
  GeneralInnerInputSty: {
    fontSize: 12,
    height: 35,
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    backgroundColor: "transparent",
  },
  GeneralTextSty: {
    color: "#000114",
    fontSize: 12,
    // marginBottom: 1,
    // marginLeft: 10,
  },
  iconSTy: {
    width: 20,
    height: 20,
  },
});
