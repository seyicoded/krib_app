import React, {Component, Fragment} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import accountStore from '../../../stores/Account';

import {Item, Input, Label} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';
const blackColor = accountStore.BlackColor;

export default class SearchInput extends Component {
  state = {};

  render() {
    const {selectedItems} = this.state;
    return (
      <View style={{width: '100%'}}>
        <Item
          regular
          style={(this, this.props.searchinputSty || styles.GeneralSearchInputSty)}>
            <AntDesign
              name="search1"
              style={styles.iconSTy}
            />
          <Input
            {...this.props}
            style={this.props.innerInputSTy || styles.GeneralInnerSearchInputSty}
          />
        </Item>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    GeneralSearchInputSty: {
    backgroundColor: '#fff',
    borderRadius: 20,
    width: '100%',
    height: 44,
    color: '#333',
    borderWidth: 0,
    borderColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#E2E3FE',
    marginBottom: 16,
    elevation: 5,
    paddingLeft: 15,
    paddingRight: 15,
  },
  GeneralInnerSearchInputSty: {
    fontSize: 12,
    height: 35,
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    backgroundColor: 'transparent',
    color: blackColor,
  },
  GeneralTextSty: {
    color: '#000114',
    fontSize: 12,
    // marginBottom: 1,
    // marginLeft: 10,
  },
  iconSTy: {
    fontSize: 20,
  },
});
