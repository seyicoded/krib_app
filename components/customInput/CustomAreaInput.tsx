import React, { Component, Fragment } from "react";
import {
  StyleSheet,
  View,
  Image,
  TextInput,
  ImageBackground,
} from "react-native";
import CustomText from "../customText/CustomText";
import { colors, fonts, images } from "../../theme";
import { TouchableOpacity } from "react-native-gesture-handler";

function CustomInput(props: {
  inputText: string;
  inputTextWeight: string;
  inputTextSize: number;
  inputTextcolor: string;
  placeHolderText: string;
  inputTextSty: any;
  showIcon: boolean;
}) {
  return (
    <View style={{ width: "100%" }}>
      {props.inputText ? (
        <CustomText
          textSize={props.inputTextSize}
          textWeight={props.inputTextWeight}
          textcolor={props.inputTextcolor}
          displayText={props.inputText || ""}
          textStyle={props.inputTextSty || styles.GeneralTextSty}
        />
      ) : (
        <Fragment />
      )}

      <ImageBackground
        resizeMode="stretch"
        source={images.inputCard}
        style={{ width: "100%", height: 154 }}
      >
        <TextInput
          placeholder={props.placeHolderText}
          multiline={true}
          numberOfLines={4}
          style={{
            height: "100%",
            paddingHorizontal: 20,
          }}
          {...props}
        />
      </ImageBackground>
    </View>
  );
}
export default CustomInput;

const styles = StyleSheet.create({
  GeneralInputSty: {
    backgroundColor: "#fff",
    borderRadius: 0,
    width: "100%",
    height: 44,
    color: "#333",
    borderWidth: 0,
    borderColor: "#fff",
    borderBottomWidth: 1,
    borderBottomColor: "#E2E3FE",
    marginBottom: 16,
  },
  GeneralInnerInputSty: {
    fontSize: 12,
    height: 35,
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    backgroundColor: "transparent",
  },
  GeneralTextSty: {
    color: "#000114",
    fontSize: 12,
    // marginBottom: 1,
    // marginLeft: 10,
  },
  iconSTy: {
    width: 20,
    height: 20,
  },
});
