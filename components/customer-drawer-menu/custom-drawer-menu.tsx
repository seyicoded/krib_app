import React, { useEffect, useState } from "react";
import {
  Image,
  ImageStyle,
  Keyboard,
  StatusBar,
  View,
  ViewStyle,
  ImageBackground,
  TextStyle,
  TouchableOpacity,
  Text,
} from "react-native";
// import {
//     NavigationScreenProps,
//     DrawerItems,
//     DrawerItemsProps
// } from "react-navigation"
import { colors, fonts, images } from "../../theme";
import { Header } from "../header";
// import { Text } from "native-base";
import { Dispatch } from "redux";
import { ApplicationState } from "../../redux";
import { connect } from "react-redux";
import { Layout } from "../../constants";
import { Button } from "../button";
import { translate } from "../../i18n";
// import { IUser, savePin, toggleBiometric } from "../../redux/auth";
// import * as LocalAuthentication from 'expo-local-authentication';
import { removePersistData } from "../../utils/UtilityFunctions";
interface DispatchProps {
  // toggleBiometric: () => void;
  savePin: (pin: string) => void;
}

interface StateProps {
  // userDetails: IUser;
  picture: string;
  walletNumber: string;
  // useBiometric: boolean;
}

// interface BaseProps extends NavigationScreenProps { }

// type CustomDrawerMenuProps = DispatchProps & StateProps & BaseProps;

const ROOT: ViewStyle = {
  height: "100%", // backgroundColor: colors.backgroundTwo,
  width: "100%",
};

const PROFILE_IMAGE_VIEW: ViewStyle = {
  height: 250,
  width: "100%",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
};

const DIVIDER: ViewStyle = {
  height: 8,
  //   backgroundColor: colors.palette.primaryPink
};
const LEVELSTYLE: TextStyle = {
  color: colors.alerzoOrange,
  fontSize: 10,
  fontFamily: fonts.GilmerRegular,
  fontWeight: "bold",
  padding: 3,
  backgroundColor: colors.alerzoOrangeLight,
};
const VERIFY_BUTTON: ViewStyle = {
  borderRadius: 10,
  height: 60,
  backgroundColor: colors.alerzolightRed,
  marginHorizontal: 20,
  marginTop: Layout.window.height / 6,
};
const LEVELDETAILSSTYLE: TextStyle = {
  color: colors.fadeGrey,
  fontSize: 10,
  fontFamily: fonts.GilmerRegular,
  marginLeft: 5,
};
const LEVELDETAILSSTYLE2: TextStyle = {
  color: colors.alerzoBlue,
  fontSize: 10,
  fontFamily: fonts.GilmerRegular,
  marginLeft: 3,
};

const VERIFY_BUTTON_TEXT: TextStyle = {
  fontSize: 14,
  fontFamily: fonts.GilmerMedium,
  color: colors.alerzoRed,
  textAlign: "center",
};
const SDWRAP: TextStyle = {
  width: 30,
  alignItems: "center",
  justifyContent: "center",
  marginRight: 25,
};

const CustomDrawerMenu = ({ navigation, route }) => {
  // const [toggleFingerPrint, setToggleFingerPrint] = useState(false);
  // const [useFingerPrint, setUseFingerPrint] = useState(true);
  // const [hasFingerPrint, setHasFingerPrint] = useState(false);
  // const {
  //   navigation,
  //   // userDetails,
  //   picture,
  //   walletNumber,
  //   // useBiometric,
  //   // toggleBiometric,
  //   savePin,
  // } = props;

  // console.log(hasFingerPrint, "hasFingerPrint");

  // const { first_name, last_name } = userDetails;

  // useEffect(() => {
  //   checkDeviceForHardware();
  // }, []);

  // const checkDeviceForHardware = async () => {
  //   let compatible = await LocalAuthentication.hasHardwareAsync();
  //   console.log(compatible, "<=== compatible");

  //   if (compatible) {
  //     setHasFingerPrint(true);
  //     checkHardwareType();
  //   }
  // };

  // const checkHardwareType = async () => {
  //   let getEnrolledLevelAsync =
  //     await LocalAuthentication.getEnrolledLevelAsync();
  //   console.log(getEnrolledLevelAsync, "getEnrolledLevelAsync");

  //   setUseFingerPrint(getEnrolledLevelAsync === 1);
  //   console.log(useFingerPrint, "useFingerPrint");
  // };

  const logout = () => {
    console.log("new");
    // dashboardStore.Logmeout = true
    removePersistData("Loggedinstate")
      .then(() => {
        // alert("logged out")
        navigation.navigate("Main", {
          screen: `Login`,
        });
      })

      .catch((err) => {
        console.warn(err);
      });
  };

  return (
    <View style={{ flex: 1 }}>
      <StatusBar barStyle={"dark-content"} />
      <View style={ROOT}>
        <View
          style={{
            width: "100%",
            marginTop: 90,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingHorizontal: 20,
              width: " 50%",
            }}
          >
            <Text
              style={{
                // marginTop: 20,
                fontSize: 18,
                color: colors.dark2,
              }}
            >
              Menu
            </Text>
          </View>

          <TouchableOpacity
            style={{ paddingHorizontal: 20 }}
            onPress={() => {
              navigation.closeDrawer();
            }}
          >
            <Image source={images.close} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Main", {
              screen: "TabHome",
              params: {
                screen: "Home",
                params: {
                  screen: "Explore",
                },
              },
            })
          }
          style={{
            marginTop: Layout.window.height / 15,
            marginHorizontal: 20,
            flexDirection: "row",
            width: "85%",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
            }}
          >
            <View style={SDWRAP}>
              <Image source={images.SbExplore} />
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  marginTop: -5,
                  fontFamily: fonts.GilmerMedium,
                  fontSize: 18,
                  color: colors.fadeDark2,
                }}
              >
                Explore
              </Text>
            </View>
          </View>

          {/* <Image source={images.arraowRightProfile} /> */}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Main", {
              screen: "TabHome",
              params: {
                screen: "Home",
                params: {
                  screen: "Message",
                },
              },
            })
          }
          style={{
            // marginTop: Layout.window.height / 15,
            marginHorizontal: 20,
            flexDirection: "row",
            width: "85%",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
            }}
          >
            <View style={SDWRAP}>
              <Image source={images.SbMsg} />
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  marginTop: -5,
                  fontFamily: fonts.GilmerMedium,
                  fontSize: 18,
                  color: colors.fadeDark2,
                }}
              >
                Messaging
              </Text>
            </View>
          </View>

          {/* <Image source={images.arraowRightProfile} /> */}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Main", {
              screen: "TabHome",
              params: {
                screen: "Home",
                params: {
                  screen: "Notifications",
                },
              },
            })
          }
          style={{
            // marginTop: Layout.window.height / 15,
            marginHorizontal: 20,
            flexDirection: "row",
            width: "85%",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
            }}
          >
            <View style={SDWRAP}>
              <Image source={images.SbNoti} />
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  marginTop: -5,
                  fontFamily: fonts.GilmerMedium,
                  fontSize: 18,
                  color: colors.fadeDark2,
                }}
              >
                Notifications
              </Text>
            </View>
          </View>

          {/* <Image source={images.arraowRightProfile} /> */}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Main", {
              screen: "TabHome",
              params: {
                screen: "Home",
                params: {
                  screen: "Profile",
                },
              },
            })
          }
          style={{
            // marginTop: Layout.window.height / 15,
            marginHorizontal: 20,
            flexDirection: "row",
            width: "85%",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
            }}
          >
            <View style={SDWRAP}>
              <Image source={images.SbProfile} />
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  marginTop: -5,
                  fontFamily: fonts.GilmerMedium,
                  fontSize: 18,
                  color: colors.fadeDark2,
                }}
              >
                Profile
              </Text>
            </View>
          </View>

          {/* <Image source={images.arraowRightProfile} /> */}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("Main", { screen: "MySettings" })}
          style={{
            // marginTop: Layout.window.height / 15,
            marginHorizontal: 20,
            flexDirection: "row",
            width: "85%",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
            }}
          >
            <View style={SDWRAP}>
              <Image source={images.SbSet} />
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  marginTop: -5,
                  fontFamily: fonts.GilmerMedium,
                  fontSize: 18,
                  color: colors.fadeDark2,
                }}
              >
                Settings
              </Text>
            </View>
          </View>

          {/* <Image source={images.arraowRightProfile} /> */}
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default CustomDrawerMenu;

// const mapDispatchToProps = (dispatch: Dispatch<any>): DispatchProps => ({
//   toggleBiometric: () => dispatch(toggleBiometric()),
//   savePin: (pin: string) => dispatch(savePin(pin)),
// });

// const mapStateToProps = (state: ApplicationState): StateProps => ({
//   // userDetails: state.auth.user,
//   picture: state.auth.user.profile_image,
//   walletNumber: state.auth.user.account_number,
//   // useBiometric: state.auth.useBiometric,
// });

// export const CustomDrawerMenu = connect<StateProps>(
//   mapStateToProps
//   // mapDispatchToProps
// )(CustomDrawer);
