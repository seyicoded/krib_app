import React from "react";
import {
  KeyboardAvoidingView,
  StyleSheet,
  Platform,
  Dimensions,
  ScrollView
} from "react-native";

export const KeyboardAvoidContainer = (props: {
  children: any;
  style?: any;
}) => {
  const { children, style = {} } = props;

  return (
    <KeyboardAvoidingView
      style={[styles.avoider]}
      behavior={Platform.OS === "iOS" ? "padding" : "height"}
      // enabled
      // keyboardVerticalOffset={0}
      enabled={true}
    >
      <ScrollView>
        {children}
      </ScrollView>
      
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  avoider: {
    flex: 1,
  },
});
