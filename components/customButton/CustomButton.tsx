/* eslint-disable react-native/no-inline-styles */

import React, { Component } from "react";
import { Text, TouchableOpacity, TextStyle } from "react-native";
// import { withNavigation } from 'react-navigation';
import CustomText from "../customText/CustomText";
import { colors } from "../../theme";

function CustomBtn(props: {
  // textStyle: any;
  btnText: string;
  handlePress: any;
}) {
  const BTNSTYLE: TextStyle = {
    borderRadius: 5,
    backgroundColor: colors.dark2,
    justifyContent: "center",
    alignItems: "center",
    padding: 15,
    width: "100%",
    marginTop: 40,
  };
  return (
    <TouchableOpacity style={BTNSTYLE} onPress={() => props.handlePress()}>
      <CustomText
        textSize={16}
        textWeight={"normal"}
        textcolor={colors.white}
        displayText={props.btnText}
        textStyle={{ marginBottom: 6, textAlign: "center" }}
      />
    </TouchableOpacity>
  );
}
export default CustomBtn;

// export default withNavigation(RecruiterLanding);
