import * as React from "react";
import { View, TextInput, TextStyle, ViewStyle, Image } from "react-native";
import { colors, fonts, images } from "../../theme";
import { translate } from "../../i18n";
import { Text } from "../text";
import { TextFieldProps } from "./text-field.props";
import { mergeAll, flatten } from "lodash/fp";
import { Layout } from "../../constants";

// the base styling for the container
const CONTAINER = (
  isInvalid: boolean = false,
  hasBorder: boolean = true,
  isFocuse: boolean = false
): ViewStyle => ({
  borderWidth: hasBorder ? 1 : 0,
  // borderColor: isInvalid ? colors.red : colors.greyTwo,
  borderColor: isFocuse ? "red" : isInvalid ? colors.red : colors.lightgrey,
  // width: Layout.window.width / 1.3,
  borderRadius: 10,
  flexDirection: "row",
  backgroundColor: colors.background,
  // backgroundColor: "red",
  alignItems: "center",
  // borderColor: '#E8EBEE'
  // justifyContent: "center",
  // height: 20,
});

// the base styling for the TextInput
const INPUT: TextStyle = {
  fontFamily: fonts.GilmerMedium,
  color: colors.darkGreen,
  paddingHorizontal: 10,
  // paddingVertical: 16,
  // backgroundColor: colors.white,
  backgroundColor: colors.background,
  height: 48,
  // lineHeight: 10,
  borderRadius: 10,
  // borderColor: '#E8EBEE',
  // borderColor: "#E8EBEE",

  // width: Layout.window.width / 1.5,
  // fontSize: 15
};

const FIELD_VALIDATION = (
  isInvalid: boolean = false,
  showRightAway: boolean = true
): TextStyle => ({
  marginTop: 4,
  // paddingLeft: 18,
  marginBottom: isInvalid ? 15 : 6,
  opacity: isInvalid ? 1 : 0,
  display: showRightAway ? "flex" : isInvalid ? "flex" : "none",
  fontFamily: fonts.GilmerMedium,
  color: colors.alerzoBlack,
  fontSize: 10,
});

const LABEL: TextStyle = {
  marginBottom: 10,
  color: colors.alerzoBlack,
  fontFamily: fonts.GilmerMedium,
  textAlign: "left",
  fontWeight: "400",
};

// currently we have no presets, but that changes quickly when you build your app.
const PRESETS: { [name: string]: ViewStyle } = {
  default: {},
};

const enhance = (style, styleOverride) => {
  return mergeAll(flatten([style, styleOverride]));
};

interface State {}

/**
 * A component which has a label and an input together.
 */
export class TextField extends React.Component<TextFieldProps, State> {
  render() {
    const {
      placeholderTx,
      placeholder,
      labelTx,
      labelTxOptions,
      textContentType,
      label,
      preset = "default",
      style: styleOverride,
      inputStyle: inputStyleOverride,
      forwardedRef,
      isInvalid,
      fieldError,
      padFieldForError = true,
      extraComponent,
      topComponent,
      multiline,
      numberOfLines,
      onTouchEnd,
      keyboardType,
      clearButtonMode,
      // hasImage,
      ...rest
    } = this.props;

    const shouldInvalid = isInvalid && !!fieldError;

    let containerStyle: ViewStyle = {
      ...CONTAINER(shouldInvalid),
      ...PRESETS[preset],
    };
    containerStyle = enhance(containerStyle, styleOverride);

    let inputStyle: TextStyle = INPUT;
    inputStyle = enhance(inputStyle, inputStyleOverride);
    const placeholderText = placeholderTx
      ? translate(placeholderTx)
      : placeholder;

    const labelText = labelTx ? translate(labelTx, labelTxOptions) : label;

    return (
      <>
        <View
          style={{
            width: "90%",
          }}
        >
          {Boolean(labelText) && (
            <Text preset="fieldLabel" text={labelText} style={LABEL} />
          )}
        </View>

        <View style={containerStyle}>
          {/* {hasImage === true ? "" : "" } */}
          {Boolean(topComponent) ? topComponent : null}
          <TextInput
            clearButtonMode={clearButtonMode || "while-editing"}
            placeholder={placeholderText}
            placeholderTextColor={colors.greyTwo}
            underlineColorAndroid={colors.transparent}
            onFocus={this.props.onFocus}
            onBlur={this.props.onBlur}
            multiline={multiline}
            style={inputStyle}
            ref={forwardedRef}
            {...rest}
            numberOfLines={numberOfLines}
            autoCorrect={false}
            textContentType={textContentType}
            onTouchEnd={onTouchEnd}
            keyboardType={keyboardType || "default"}
          />

          {Boolean(extraComponent) ? extraComponent : null}
        </View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "flex-start",
            alignSelf: "flex-start",
            marginLeft: 20,
          }}
        >
          {/* {
            shouldInvalid && (
              <Image
                source={images.padlockBlue}
                style={{
                  marginBottom: 5
                }}
              />
            )
          } */}

          <Text
            preset="fieldError"
            tx={shouldInvalid ? fieldError : null}
            style={FIELD_VALIDATION(isInvalid, padFieldForError)}
          />
        </View>
      </>
    );
  }
}
