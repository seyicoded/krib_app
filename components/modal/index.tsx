import React, { useState } from "react";
import {
  Button,
  NativeSyntheticEvent,
  NativeTouchEvent,
  View,
} from "react-native";
import Modal from "react-native-modal";

function ModalTester(props: {
  handleOnPress: (ev: NativeSyntheticEvent<NativeTouchEvent>) => void;
  isModalVisible: boolean;
  modalBody:
    | boolean
    | React.ReactChild
    | React.ReactFragment
    | React.ReactPortal;
}) {
  return (
    <View style={{ flex: 1 }}>
      <Button title="Show modal" onPress={props.handleOnPress} />

      <Modal isVisible={props.isModalVisible}>{props.modalBody}</Modal>
    </View>
  );
}

export default ModalTester;
