export const icons = {
  personAdd: require("./person-add.png"),
  angleDown: require("./angle-down.png"),
  angleLeft: require("./angle-left.png"),
  angleRight: require("./angle-right.png"),
  angleUp: require("./angle-up.png"),
  arrowLeft: require("./arrow-left.png"),
  arrowRight: require("./arrow-right.png"),
  locationIcon: require("./location-icon.png"),
  notificationIcon: require("./notification-icon.png"),
  menuIcon: require("./menu-icon.png"),
  walletIcon: require("./wallet-icon.png"),
  arrowBackWhite: require("./arrow-back-white.png"),
  homeIcon: require("./navigations/home.png"),
  homeActiveIcon: require("./navigations/home_active.png"),
  accountIcon: require("./navigations/wallet.png"),
  accountActiveIcon: require("./navigations/wallet_active.png"),
  saveIcon: require("./navigations/save.png"),
  moreIcon: require("./navigations/menu.png"),
  moreActiveIcon: require("./navigations/menu_active.png")
 
}

export type IconTypes = keyof typeof icons
