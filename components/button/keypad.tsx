import React, { useState } from "react";
import { NativeSyntheticEvent, NativeTouchEvent, View } from "react-native";
import { translate } from "../../i18n";
import { colors, images } from "../../theme";

import { Text, TextStyle, Image, TouchableOpacity } from "react-native";

const KEYPAD_WRAP: TextStyle = {
  flexDirection: "row",
  flexWrap: "wrap",
  justifyContent: "flex-end",
  width: "100%",
  marginBottom: 30,
};
const INPUT_WRAP: TextStyle = {
  flexDirection: "row",
  flexWrap: "wrap",
  justifyContent: "center",
  alignItems: "center",
  width: "33.333%",
  marginBottom: 20,
};
const KEYPAD_INPUT: TextStyle = {
  color: colors.greyThree,
  fontSize: 18,
  fontWeight: "bold",
};
const BTNSTYLE2: TextStyle = {
  padding: 20,
  backgroundColor: colors.grey4,
  width: 70,
  height: 70,
  borderRadius: 70 / 2,
  alignItems: "center",
  justifyContent: "center",
};

function Keypad(props: Props) {
  const { navigation, isLoading, signInUserAsync, handlePress } = props;

  return (
    <View style={KEYPAD_WRAP}>
      <View style={INPUT_WRAP}>
        <TouchableOpacity style={BTNSTYLE2}>
          <Text style={KEYPAD_INPUT}>{translate("pinText.fig1")}</Text>
        </TouchableOpacity>
      </View>
      <View style={INPUT_WRAP}>
        <TouchableOpacity style={BTNSTYLE2}>
          <Text style={KEYPAD_INPUT}>{translate("pinText.fig2")}</Text>
        </TouchableOpacity>
      </View>
      <View style={INPUT_WRAP}>
        <TouchableOpacity style={BTNSTYLE2}>
          <Text style={KEYPAD_INPUT}>{translate("pinText.fig3")}</Text>
        </TouchableOpacity>
      </View>
      <View style={INPUT_WRAP}>
        <TouchableOpacity style={BTNSTYLE2}>
          <Text style={KEYPAD_INPUT}>{translate("pinText.fig4")}</Text>
        </TouchableOpacity>
      </View>
      <View style={INPUT_WRAP}>
        <TouchableOpacity style={BTNSTYLE2}>
          <Text style={KEYPAD_INPUT}>{translate("pinText.fig5")}</Text>
        </TouchableOpacity>
      </View>
      <View style={INPUT_WRAP}>
        <TouchableOpacity style={BTNSTYLE2}>
          <Text style={KEYPAD_INPUT}>{translate("pinText.fig6")}</Text>
        </TouchableOpacity>
      </View>
      <View style={INPUT_WRAP}>
        <TouchableOpacity style={BTNSTYLE2}>
          <Text style={KEYPAD_INPUT}>{translate("pinText.fig7")}</Text>
        </TouchableOpacity>
      </View>
      <View style={INPUT_WRAP}>
        <TouchableOpacity style={BTNSTYLE2}>
          <Text style={KEYPAD_INPUT}>{translate("pinText.fig8")}</Text>
        </TouchableOpacity>
      </View>
      <View style={INPUT_WRAP}>
        <TouchableOpacity style={BTNSTYLE2}>
          <Text style={KEYPAD_INPUT}>{translate("pinText.fig9")}</Text>
        </TouchableOpacity>
      </View>

      <View style={INPUT_WRAP}>
        <TouchableOpacity style={BTNSTYLE2} onPress={() => handlePress()}>
          <Text style={KEYPAD_INPUT}>{translate("pinText.fig0")}</Text>
        </TouchableOpacity>
      </View>
      <View style={INPUT_WRAP}>
        <TouchableOpacity style={[BTNSTYLE2, { backgroundColor: "transparent" }]} onPress={props.handlePress}>
          <View style={KEYPAD_INPUT}>
            <Image
              source={images.backspace}
              style={{ width: 32, height: 24 }}
            />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Keypad;
