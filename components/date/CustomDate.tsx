import React, { useState } from "react";
import { View, Button, Platform, TouchableOpacity } from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import CustomText from "../customText/CustomText";
import { colors, fonts, images } from "../../theme";

export const DateModule = (props: {
  elmentToshow: string;
  elmentToshowText: string;
  onChange: any;
}) => {
  const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState("");
  const [show, setShow] = useState(false);

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  return (
    <View
      style={{
        height: "100%",
        flex: 1,
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {props.elmentToshow === "date" ? (
        <TouchableOpacity
          onPress={() => showMode("date")}
          style={{
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
          }}
        >
          <CustomText
            textSize={16}
            textWeight={"normal"}
            textcolor={colors.black}
            displayText={props.elmentToshowText}
            textStyle={{ textAlign: "left" }}
          />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity onPress={() => showMode("time")}>
          <CustomText
            textSize={16}
            textWeight={"normal"}
            textcolor={colors.black}
            displayText={props.elmentToshowText}
            textStyle={{ textAlign: "left" }}
          />
        </TouchableOpacity>
      )}

      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={() => props.onChange}
        />
      )}
    </View>
  );
};
export default DateModule;
