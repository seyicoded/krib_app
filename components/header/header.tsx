import * as React from "react";
import { HeaderProps } from "./header.props";
import {
  View,
  ViewStyle,
  TextStyle,
  TouchableOpacity,
  ImageStyle,
  Platform,
  StatusBar,
  SafeAreaView,
  Image,
} from "react-native";
import { Icon } from "../icon";
import { Text } from "../text";
import { fonts, colors, images } from "../../theme";
import { translate } from "../../i18n/";
import { Layout } from "../../constants";

export const HEADER_HEIGHT =
  Platform.OS === "ios" ? Layout.window.height / 7 : Layout.window.height / 10;

const ROOT: ViewStyle = {
  flexDirection: "row",
  backgroundColor: colors.white2,
  alignItems: "center",
  justifyContent: "space-between",
  height: HEADER_HEIGHT,
  marginTop: Platform.OS === "ios" ? 0 : StatusBar.currentHeight,
};

const TITLE: TextStyle = {
  textAlign: "center",
  fontFamily: fonts.GilmerBold,
  fontSize: 20,
  color: colors.yellow,
  // marginBottom: 10,
  // lineHeight: 18,
};

const TITLE_MIDDLE: ViewStyle = {
  flex: 1,
  justifyContent: "center",
  // position: "absolute",
  width: "100%",
  height: "100%",
  // bottom: 5,
  //left: 0,
  zIndex: 1,
  marginTop: 20,
};

const SIDE: ViewStyle = {
  alignItems: "center",
  justifyContent: "center",
  zIndex: 3,
  height: 55,
  minWidth: 55,
  marginTop: 20,
};

const SIDE_LEFT: ViewStyle = {
  ...SIDE,
  marginLeft: 10,
};

const SIDE_TEXT: TextStyle = {
  letterSpacing: 0.2,
  lineHeight: 16,
  color: colors.dark,
  fontSize: 16,
};

const SIDE_RIGHT: ViewStyle = { ...SIDE };

const SIDE_ICON: ImageStyle = {
  height: 20,
  width: 20,
  // tintColor: colors.alerzoGreen,
  marginTop: Platform.OS === "ios" ? 0 : 50,
  // backgroundColor: 'red'
};

const SIDE_ICON_CONTAINER: ImageStyle = {
  // paddingHorizontal: 5
};

const SIDE_BLANK: ViewStyle = {
  width: 40,
};

export class Header extends React.PureComponent<
  HeaderProps,
  { props: { rightIconSource: any } }
> {
  renderLeft = () => {
    const {
      leftView,
      leftIcon,
      leftText,
      leftTx,
      leftBody,
      onLeftPress,
      leftIconStyle = {},
      navigation,
      isStrict = false,
      // navigation: { goBack }
    } = this.props;

    if (leftView) {
      return isStrict === true ? (
        leftView
      ) : (
        <TouchableOpacity
          onPress={onLeftPress}
          style={{
            height: 45,
            marginTop: 13,
          }}
        >
          <Image source={images.drawer} />
        </TouchableOpacity>
      );
    } else if (leftText || leftTx) {
      return (
        <TouchableOpacity
          onPress={onLeftPress ? onLeftPress : () => goBack()}
          style={SIDE_LEFT}
        >
          <Text
            text={leftTx ? translate(leftTx) : leftText}
            style={SIDE_TEXT}
            preset="link"
          />
        </TouchableOpacity>
      );
    } else if (leftIcon) {
      return (
        <TouchableOpacity
          onPress={onLeftPress ? onLeftPress : () => goBack()}
          style={[leftBody || SIDE_LEFT]}
        >
          <Image source={images.back} />
        </TouchableOpacity>
      );
    } else {
      return <View style={SIDE_BLANK} />;
    }
  };

  renderRight = () => {
    const {
      rightView,
      rightIcon,
      rightText,
      rightTx,
      onRightPress,
      isStrict = false,
      rightIconStyle = {},
    } = this.props;

    if (rightView) {
      return isStrict === true ? (
        rightView
      ) : (
        <TouchableOpacity
          // onPress={() => navigation.goBack()}
          style={{
            height: 45,
            marginTop: 13,
          }}
        >
          <Image source={images.search} />
        </TouchableOpacity>
      );
    } else if (rightText || rightTx) {
      return (
        <TouchableOpacity onPress={onRightPress} style={SIDE_RIGHT}>
          <Text
            text={rightTx ? translate(rightTx) : rightText}
            style={SIDE_TEXT}
            preset="link"
          />
        </TouchableOpacity>
      );
    } else if (rightIcon) {
      return (
        <TouchableOpacity onPress={onRightPress} style={SIDE_RIGHT}>
          <Image source={images.settings} />
        </TouchableOpacity>
      );
    } else {
      return <View style={SIDE_BLANK} />;
    }
  };

  render() {
    const { titleTx, titleStyle, title, middleView } = this.props;

    return (
      <SafeAreaView style={{ ...ROOT, ...this.props.style }}>
        <StatusBar
          barStyle={"dark-content"}
          translucent
          backgroundColor={colors.lightBlueTwo}
        />
        {this.renderLeft()}

        {title && title.length > 0 && (
          <View style={TITLE_MIDDLE}>
            <Text
              style={{ ...TITLE, ...titleStyle }}
              text={title || translate(titleTx)}
            ></Text>
          </View>
        )}

        {middleView && middleView}

        {this.renderRight()}
      </SafeAreaView>
    );
  }
}
