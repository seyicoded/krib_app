import * as React from "react";
import { formatAmount, formatCurrency } from "../../utils/formatters";

import { Image, View } from "react-native";
import { colors, fonts, images } from "../../theme";
import CustomText from "../customText/CustomText";

const Naira: React.FC<{
  amount: string | number;
  boldnaira?: any;
  regularnaira?: any;
  greennaira?: any;
  rednaira?: any;
  nairaStyle?: string;
  amountColor?: string;
  amountWeight?: string;
  amountSize?: number;
}> = ({
  amount,
  boldnaira,
  regularnaira,
  greennaira,
  rednaira,
  amountColor,
  amountSize,
  amountWeight,
  nairaStyle,
}) => {
    return (
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <View>
          {boldnaira ? (
            <Image
              source={images.naira}
              style={{ width: 15, height: 13, marginRight: 3 }}
            />
          ) : greennaira ? (
            <CustomText
              textSize={16}
              textWeight={"normal"}
              textcolor={colors.green}
              displayText={"₦"}
            />
          ) : rednaira ? (
            <CustomText
              textSize={16}
              textWeight={"normal"}
              textcolor={colors.red}
              displayText={"₦"}
            />
          ) : regularnaira ? (
            <CustomText
              textSize={16}
              textWeight={"normal"}
              textcolor={colors.black}
              displayText={"₦"}
            />
          ) : null}
        </View>
        <CustomText
          textSize={amountSize || 16}
          textWeight={amountWeight || "normal"}
          textcolor={amountColor || colors.black}
          // displayText={formatCurrency(amount || 0)}
          displayText={formatAmount(amount || 0)}
        />
      </View>
    );
  };

export default Naira;
