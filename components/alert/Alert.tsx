// react
import * as React from "react";
import { useEffect, useState, useRef } from "react";

// react-native
import {
  StatusBar,
  View,
  ViewStyle,
  Platform,
  Image,
  Text,
  TouchableOpacity,
  TextStyle,
} from "react-native";

// third-party
// import { NavigationScreenProps } from "react-navigation";
import Modal from "react-native-modal";

// redux

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";

const ROOT: ViewStyle = {
  height: "100%",
  width: Layout.window.width,
  backgroundColor: colors.background,
};

const ROW: ViewStyle = {
  marginTop: 10,
  marginHorizontal: 20,
  flexDirection: "row",
  justifyContent: "space-between",
  shadowColor: colors.shadow,
  shadowOffset: { width: 0, height: 0 },
  shadowOpacity: 0.03,
  elevation: 1,
  alignItems: "center",
  paddingHorizontal: 15,
  paddingVertical: 20,
  backgroundColor: colors.white,
  borderRadius: 10,
};

const HEADER: TextStyle = {
  color: colors.greyThree,
  fontSize: 18,
  fontFamily: fonts.GilmerMedium,
  marginTop: 30,
};

const BODY: TextStyle = {
  color: colors.greyThree,
  fontSize: 14,
  fontFamily: fonts.GilmerMedium,
  marginTop: 12,
};

const VERIFY_BUTTON: ViewStyle = {
  borderRadius: 10,
  height: 48,
  backgroundColor: colors.transparentBlue,
  marginTop: Layout.window.height / 10,
  width: Layout.window.width / 1.3,
};

const VERIFY_BUTTON_TEXT: TextStyle = {
  fontSize: 14,
  fontFamily: fonts.GilmerMedium,
  color: colors.alerzoBlue,
  textAlign: "center",
};

const BACK_BUTTON: ViewStyle = {
  borderRadius: 10,
  height: 48,
  backgroundColor: colors.alerzoBlack,
  width: Layout.window.width / 1.3,
  marginTop: Layout.window.height / 10,
};

const BACK_BUTTON_TEXT: TextStyle = {
  fontSize: 14,
  fontFamily: fonts.GilmerMedium,
  color: colors.white,
  textAlign: "center",
};

function AlertsComp(props) {
  const [showModal, setShowModal] = useState(true);

  return (
    <Modal
      isVisible={props.showModal}
      style={{ alignItems: "center" }}
      onBackdropPress={() => { }}
    >
      <View
        style={{
          height: Layout.window.height / 2,
        }}
      >
        <View
          style={{
            backgroundColor: "#FFFFFF",
            height: Layout.window.height / 2,
            width: Layout.window.width / 1.1,
            padding: 20,
            borderRadius: 10,
            alignItems: "center",
          }}
        >
          <Image source={images.correct2} style={{ width: 100, height: 100 }} />

          <Text style={HEADER}>{props.title} </Text>

          <Text style={BODY}>{props.body}</Text>

          <Button
            style={[BACK_BUTTON]}
            textStyle={BACK_BUTTON_TEXT}
            text={"close"}
            onPress={props.actions}
          />
        </View>
      </View>
    </Modal>
    // </View>
  );
}
export default AlertsComp;

// export const AlertsScreen = Alerts;
