import ActionSheet from "react-native-actions-sheet";
import React, { createRef } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Feather from "react-native-vector-icons/Feather";

const App = (props: { actionSheetRef: any; actionSheetBody: any }) => {
  return (
    <View
      style={{
        justifyContent: "center",
        flex: 1,
      }}
    >
      <ActionSheet ref={props.actionSheetRef}>
        {props.actionSheetBody}
      </ActionSheet>
    </View>
  );
};

export default App;
