export const fonts = {
  GilmerLight: 'Poppins-Light',
  GilmerBold: 'Poppins-Bold',
  GilmerHeavy: 'Poppins-ExtraBold',
  GilmerMedium: 'Poppins-Medium',
  GilmerOutline: 'Poppins-SemiBold',
  GilmerRegular: 'Poppins-Regular',
};

export type FontKeys = keyof typeof fonts;
