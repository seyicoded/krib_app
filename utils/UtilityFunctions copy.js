/* eslint-disable react-native/no-inline-styles */
// import axios from 'axios';
// import qs from 'qs';
import Spinner from "react-native-loading-spinner-overlay";
import React, { Component } from "react";
import {
  StyleSheet,
  PixelRatio,
  ActivityIndicator,
  Alert,
  Platform,
  View,
} from "react-native";
import { PermissionsAndroid } from "react-native";
import Snackbar from "react-native-snackbar";

// import ToastStyle from '../StyleSheet/ToastStyle';
import AnimatedLoader from "react-native-animated-loader";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

export const getToken = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "Loggedinstate",
      })
      .then(async (ret) => {
        console.log(ret, "Loggedinstate");
        // resolve( ret);
        console.log("jijij");
        if (ret.isAvailable === true) {
          if (ret.storeData.isloggedin === true) {
            var token = await ret.storeData.accessToken;
            console.log(token);
            console.log("token");
            let loginSession = `${token}`;
            console.log(loginSession);
            resolve(loginSession);
            // return
          } else {
            resolve("jinadBABA");
          }
        } else {
          resolve("jinadBABA");
        }
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve("jinadBABA");
            break;
          case "ExpiredError":
            // TODO
            resolve("jinadBABA");
            break;
        }
      });
  });

export const ReturnPartOfText = (text, length) => {
  let size = length;
  if (text.length < length) {
    size = text.length;
  }
  let capitalize = text.slice(0, 1).toUpperCase() + text.slice(1, size);
  return capitalize;
};
export const myspiner = (state, text = null) => {
  let spin = (
    <View style={styles.Spinnercontainer}>
      <AnimatedLoader
        visible={state}
        // overlayColor="rgba(255,255,255,0.75)"
        overlayColor="rgba(0.00,0.00,0.00,0.1)"
        //overlayColor="000000"
        // source={require("./loader.json")}
        // source={{ uri:'https://assets7.lottiefiles.com/datafiles/QeC7XD39x4C1CIj/data.json'}}
        animationStyle={styles.lottie}
        speed={1}
      />
    </View>
  );
  return spin;
};
export const SkeletonLoader = (state, text = null) => {
  let spin = (
    <SkeletonPlaceholder>
      <View
        style={{
          backgroundColor: "#fff",
          flex: 1,
          justifyContent: "flex-end",
          marginBottom: 0,
          padding: 19,
          height: 140,
        }}
      >
        <View style={styles.displayTitle} />
        <View style={styles.displayTitle} />
        <View style={styles.displayTitle} />
      </View>
    </SkeletonPlaceholder>
  );

  return spin;
};

export const MapLoader = (state, text = null) => {
  let spin = (
    <View style={styles.Spinnercontainer}>
      <Spinner
        visible={state}
        textContent={text ? text : "Please Wait..."}
        textStyle={{ color: "#fff" }}
        animation={"fade"}
        color={"#bd393b"}
        overlayColor={"#000"}
        cancelable={true}
      />
    </View>
  );
  return spin;
};

export const ShowNotification = (Text, delay = false) => {
  if (delay) {
    setTimeout(() => {
      Snackbar.show({
        title: Text,
        duration: Snackbar.LENGTH_LONG,
      });
    }, 1000);
  } else {
    Snackbar.show({
      title: Text,
      duration: Snackbar.LENGTH_LONG,
    });
  }
};

export const activityspiner = (state) => {
  let spin = (
    <View style={styles.activityIndicatorcontainer}>
      <ActivityIndicator
        animating={state}
        color="#bc2b78"
        size="large"
        style={styles.activityIndicator}
      />
    </View>
  );

  return spin;
};

export const PersistData = (Mykey, data) => {
  console.warn(Mykey, data);
  global.storage.save({
    key: Mykey, // Note: Do not use underscore("_") in key!
    data: {
      isAvailable: true,
      storeData: data,
    },

    // if not specified, the defaultExpires will be applied instead.
    // if set to null, then it will never expire.
    expires: null,
  });
};

export const GetPersistData = (key) =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: key,
      })
      .then((ret) => {
        if (ret.isAvailable === true) {
          // return ret.storeData;
          console.log(ret);
          resolve(ret.storeData);
        } else {
          // return false;
          resolve(false);
        }
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve({});
            break;
          case "ExpiredError":
            // TODO
            resolve({});
            break;
        }
      });
  });
export const loggedinUserdetails = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "Loggedinstate",
      })
      .then((ret) => {
        resolve(ret);
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve({});
            break;
          case "ExpiredError":
            // TODO
            resolve({});
            break;
        }
      });
  });

export const GetCompanyKey = async () => {
  let data = await GetPersistData("UserOrganization");
  if (data !== false) {
    let NewData = data.details;
    console.log(NewData, "NewData");
    //console.log('NewData');
    let companykay = NewData.orgKey;
    return companykay;
  } else {
    return "nill";
  }
};
export const GetCompanyFullDetails = async () => {
  let data = await GetPersistData("UserOrganization");
  console.log(data, "GetCompanyFullDetails");
  if (data !== false) {
    let companydetails = data.details;

    return companydetails;
  } else {
    return null;
  }
};
export const GetcurrentBranch = async () => {
  let Currentbranch = await GetPersistData("MyCurrentBranch");

  if (Currentbranch === false) {
  } else {
    let branch_id = Currentbranch.value;

    return branch_id;
  }
  // 08033374342
  // let branch_id=4;

  /// return branch_id;
};

// export const GetUserType = (key) => {
//   let data = this.GetCurrentUserDetails();
//   let parsedData = JSON.parse(data);

//   let usertype = parsedData.user_type;
//   return usertype;
//   // console.log(parsedData);
//   // console.log('parsedData');
// };

export const getusertype = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "Loggedinstate",
      })
      .then((ret) => {
        resolve(ret);
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve({});
            break;
          case "ExpiredError":
            // TODO
            resolve({});
            break;
        }
      });
  });
export const getLodgedInDetailsProfile = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "loggedinUserdetails",
      })
      .then((ret) => {
        // resolve(ret);
        if (ret.isAvailable === true) {
          resolve(ret.storeData);
        } else {
          resolve(false);
        }
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            //      alert("You have not register before")
            resolve({});
            break;
          case "ExpiredError":
            //    alert("You have not register dd")
            resolve({});
            break;
        }
      });
  });
export const PersistLoginRecord = (profile, logginstate) => {
  let expiring = 1000 * 3600 * 1.5;
  PersistData("loggedinUserdetails", profile);
  PersistData("Loggedinstate", logginstate, expiring);
};

// export const getusertypeold = () => {

//  return  loggedinUserdetails().then(
//     (resp) => {
// let isloggedin =resp.isloggedin;
// console.log(resp);
// if(isloggedin===true){
// let usertype = resp.usertype
// console.warn(usertype);
// console.warn('usertype lddkdkdddkdkdkdkdkdkdkdkdkdk');
// // global.usertype= usertype;
// return 'Usertypemi';

// }
// else{
//   return 'notin';
// }

// }
//   )
//   .catch((error) => {
//     console.warn(`get usertpe function ${error}`);
//   });

// }

const styles = StyleSheet.create({
  lottie: {
    width: 100,
    height: 100,
  },

  activityIndicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 80,
  },
  activityIndicatorcontainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 70,
    backgroundColor: "#000000",
  },
  Spinnercontainer: {
    // flex: 1,
    justifyContent: "center",
    textAlign: "center",
    paddingTop: 30,
    marginBottom: 30,
    backgroundColor: "#ecf0f1",
    padding: 8,
    // top: 0, bottom: 0, left: 0, right: 0
    bottom: 50,
  },
  displayTitle: {
    // flex:1,
    paddingTop: 2,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    // backgroundColor: 'rgba(0,0,0,0.4)',
    // paddingLeft:10,
  },
});
