// import 'react-native-gesture-handler';
// import { registerRootComponent } from 'expo';
import * as React from 'react'
import {Platform} from 'react-native'

// import App from './App';

// // registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// // It also ensures that whether you load the app in Expo Go or in a native build,
// // the environment is set up appropriately
// registerRootComponent(App);

//Key: AAAAwt9BG18:APA91bGYMJXHfYkwkswCwVvzDJFsGRB6Ws-GC5Oj9W6owGMtgNZxOWIv_4t2chtX3_ZFve6k40XIx_WDpsJ17BFPmoIM8VaGQDynWtfs6g0YhZRsTbvokkzBFtONOwidwjYRFKO1sHoG

import { AppRegistry } from "react-native";
import App from "./App";
import Splash from "./Splash";
import { name as appName } from "./app.json";

if(Platform.OS === 'android'){
    AppRegistry.registerComponent(appName, () => App);  
}else{
    AppRegistry.registerComponent(appName, () => Splash);
}

