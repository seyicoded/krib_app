// react
import React, { useEffect, useState, useRef } from "react";

// react-native
import {
  StatusBar,
  View,
  ViewStyle,
  Platform,
  Image,
  Text,
  TouchableOpacity,
  TextStyle,
  ImageBackground,
} from "react-native";

// third-party
import { connect } from "react-redux";
import { Dispatch } from "redux";

// redux

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Header } from "../../components/header";
import { translate } from "../../i18n";
import { Button } from "../../components/button";

// util

interface DispatchProps { }

interface StateProps { }

interface MyFormValues { }



const ROOT: ViewStyle = {
  height: "100%",
  width: Layout.window.width,
  backgroundColor: colors.alerzoBlack,
};

const VERIFY_BUTTON: ViewStyle = {
  borderRadius: 10,
  height: 48,
  backgroundColor: colors.alerzoBlue,
  marginTop: Layout.window.height / 3,
  marginHorizontal: 20,
};

const VERIFY_BUTTON_TEXT: TextStyle = {
  fontSize: 14,
  fontFamily: fonts.GilmerMedium,
  color: colors.white,
  textAlign: "center",
};

const ComingSoon = (props: Props) => {
  const { navigation } = props;
  const [bvn, setBVN] = useState("");
  const [dob, setDOB] = useState("");
  const startDateRef = useRef(null);

  let [isDark, setIsDark] = useState(false);





  let bvnRef = useRef();
  let dobRef = useRef();

  return (
    <View
      style={[ROOT]}
    >
      <ImageBackground
        source={images.successBKG}
        style={{
          height: Layout.window.height / 3,
          marginVertical: Layout.window.height / 20,
          justifyContent: "center",
          alignItems: "center",
        }}
        imageStyle={{
          borderRadius: 10,
        }}
        resizeMethod="auto"
        resizeMode="stretch"
      >
        {/* <Image source={images.checkIcon} /> */}

        <Text
          style={{
            marginTop: 20,
            color: colors.white,
            fontFamily: fonts.GilmerMedium,
          }}
        >
          Something cool is coming here soon
        </Text>
      </ImageBackground>

      {/* <Button
        onPress={() => navigation.navigate("homepage")}
        style={VERIFY_BUTTON}
        textStyle={VERIFY_BUTTON_TEXT}
        tx={"success.backHome"}
      /> */}
    </View>
  );
};


export const ComingSoonScreen = ComingSoon;
