// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  TextInput,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import {
  PersistLoginRecord,
  GetCompanyFullDetails,
  ShowNotification,
  GetCompanyKey,
  GetPersistData,
  PersistData,
} from "../../utils/UtilityFunctions";
// redux
import { ApplicationState } from "../../redux";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomInput from "../../components/customInput/CustomInput";
import CustomButton from "../../components/customButton/CustomButton";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { Forgetpassword } from "../../services/api";

// util

interface SignUpModuleScreenProps { }

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const BODY: ViewStyle = {
  marginTop: 20,
};
const INPUTWRAP: ViewStyle = {
  width: "100%",
  justifyContent: "center",
  marginTop: 20,
  //   backgroundColor: "green",
};
const INPUTWRAP2: ViewStyle = {
  width: "100%",
  paddingHorizontal: 20,
  marginBottom: 20,

  //   backgroundColor: "green",
};
const SETINPUTSTYLE: TextStyle = {
  marginBottom: 10,
};

const SignUpModule = ({ navigation, route }) => {
  const navigations = useNavigation();
  const [emailOrUsername, setEmailOrUsername] = useState("");
  const [processing, setprocessing] = useState(false);


  const mode = "dark";
  // const processLogin = async () => {
  const processForgetPassword = async () => {
    if (emailOrUsername === "") {
      ShowNotification("Email is  required", "warning");
      return;
    }
    let payload = {
      emailAddress: emailOrUsername,
      callbackurl: "http://localhost:3000/dashboard",
    };
    try {
      setprocessing(true)

      const result = await Forgetpassword(payload);
      const { kind, data } = result;
      const loginData = data?.data;
      console.warn(data, "omoloja");
      setprocessing(false)

      if (kind === "ok") {
        ShowNotification("successful");
        navigation.navigate("Main", {
          screen: "ResetPassword",
          params: { userEmail: emailOrUsername },
        });
        // navigation.navigate({screen:"ResetPassword", params: { userEmail: emailOrUsername }});
      } else {
        const newMsg = "account not authorise, pls check you credential";
        ShowNotification(newMsg, "error");
      }
    } catch ({ message }) {
      ShowNotification(JSON.stringify(message), "error");
    }
  };
  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftIcon={true}
        title="Forgot Password"
        onLeftPress={() => navigation.goBack()}
      />
      <CustomText
        textSize={15}
        textWeight={"normal"}
        textcolor={colors.black}
        displayText={
          "Enter your email in the text box to receive password reset instructions."
        }
        textStyle={{
          textAlign: "left",
          marginBottom: 0,
          marginLeft: 20,
          marginTop: 15,
        }}
      />
      <ScrollView
        contentContainerStyle={{
          justifyContent: "center",
        }}
        style={{
          paddingVertical: 15,
        }}
      >
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View
              style={{
                justifyContent: "flex-start",
                minHeight: Layout.window.height,
                // minHeight: "100%",
                flex: 1,
              }}
            >
              <View style={INPUTWRAP2}>
                <CustomInput
                  inputText={"Email address"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  onChangeText={(value: string) => {
                    setEmailOrUsername(value);
                  }}
                  placeHolderText={"Email address"}
                  showIcon={false}
                  keyboardType={"email-address"}

                />
              </View>

              <View style={{ paddingHorizontal: 15 }}>
                <CustomButton
                  btnText={processing === true ? "Processing...." : "Request Reset"}

                  handlePress={() => {
                    processForgetPassword();
                  }}
                />
              </View>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
    </View>
  );
};

export default SignUpModule;
