import { StyleSheet, Text, View, ViewStyle, ScrollView } from "react-native";
import React from "react";
import { useNavigation } from "@react-navigation/native";
import CustomText from "../../components/customText/CustomText";
import { Header } from "../../components/header";
import { Layout } from "../../constants";
import { colors } from "../../theme";
import Accordion from "@gapur/react-native-accordion";

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,

  // backgroundColor: colors.white,
};

const data: {
  title: string;
  des?: string;
  list?: string[];
}[] = [
  {
    title: "How do I sell/rent out my property?",
    des: "",
    list: [
      "1. On the explore page, scroll down to the sell/lease card, click Get Started",
      "2. Fill in the details of the Property",
      "Choose the property type, whether it is for sale or rent",
      "'\u2B24' Enter the address of the property",
      "'\u2B24' Enter the listing title. E.g “New 5 bedroom apartment for rent in Wuye”",
      "'\u2B24' Describe your property. Here, you are required to write a captivating sales copy for your property. Click next",
      "'\u2B24' On the next page, choose whether it is a residential, commercial, or office property",
      "'\u2B24' Choose the year it was built, the number of available units.",
      "'\u2B24' Enter the price of the property and service charge (if any)",
      "'\u2B24' Select the number of bedrooms, bathrooms, kitchens in the property",
      "'\u2B24' On the next page, select the amenities in the building, appliances and fittings",
      "'\u2B24' On the next page, attach property images and documents. Click next",
      "'\u2B24' On the final page, schedule consultation by clicking the date and time our team should come to verify and capture your property.",
      "1. Click finish",
    ],
  },
  {
    title: "How do I buy/rent a property?",
    des: " ",
    list: [
      "1. On the explore page, you will find the sale and rent cards, to view property details and price insights, click the dots on the property images.",
      "2. When you find the property you want to buy or rent, Scroll down to the bottom of the page and click Message seller. As soon as they receive your message, they should respond.",
      "3. Discuss, negotiate and schedule a tour with the seller or landlord.",
      "4. To make payment for the property, click reserve this property for me.",
      "5. You can either make payment with your card or bank transfer.",
      "6. If you are making a payment with your card, enter your card number, expiry date, CVV, and card pin. Click continue. When payment is complete, you will receive an email confirmation.",
      "7. Within 1 hour, you will receive your rental agreement in your email for you to sign. After the landlord signs, you will be able to download the rental agreement to your device.",
    ],
  },
  {
    title: "How do I receive money when I sell/rent out my property?",
    des: "Once a client makes the payment for a property, within 1 hour you will receive the rental agreement. After signing, the money in the account you registered on your krib profile.",
    list: [],
  },
  {
    title: "How do I add my account details?",
    des: "",
    list: [
      "1. On the explore page, click profile.",
      "2. On your profile page, click payment info",
      "3 .Select your bank, enter your account number and password. Click Add Bank.",
      "4. Your withdrawal account will be displayed.",
    ],
  },
  {
    title:
      "Do I need to upload my property documents to sell or rent out on Krib?",
    des: "Yes. This is necessary for us to verify if you are the owner of the property you want to sell or rent out. Your property will go live on the mobile app once we have verified the documents.",
    list: [],
  },
  {
    title: "What do I need to open a Krib account?",
    des: "To open a krib account, you must have access to a smartphone. Afterward, you can proceed to download the app on Google Play Store for Android and App Store for iPhone users.",
    list: [],
  },
  {
    title: "Can I sell/rent out more than one property on Krib?",
    des: "Yes. Companies and individuals with multiple properties can list their properties on Krib. To do this, go to your profile, click add new listings. Follow the above steps for listing your property on Krib.",
    list: [],
  },
  {
    title: "Can I pay directly to the landlord or seller?",
    des: "We strongly disallow this. Even though all properties on Krib are verified, we only take responsibility for properties that payments were made through Krib. No extra costs are incurred for paying through Krib. Also, this is to guarantee the safety of your money as your landlord/seller receives the payment after both parties have signed all agreements.",
    list: [],
  },
  {
    title: "Can I edit a property’s details after listing?",
    des: "Yes, you can. Please reach out to our customer service via WhatsApp on +234 703 616 4880 or Email; hello@krib.ng and we will update the details for you.",
    list: [],
  },
  {
    title:
      "Can I view the contact details of the property owner or purchaser within the app?",
    des: "No. We value the privacy of all our users. All communication takes place via chat within the Krib App.",
    list: [],
  },
  {
    title:
      "How do I sell/rent my property if I am unavailable to show the property to prospective buyers/renters?",
    des: "They can tour the property virtually through the images. Alternatively, they can go view the property in your absence. We encourage that you have security personnel or someone you trust to show them the property.",
    list: [],
  },
  {
    title:
      "Do I need my BVN or any means of Identification to open an account?",
    des: "You do not need any means of identification or BVN to open a Krib account",
    list: [],
  },
  {
    title:
      "You do not need any means of identification or BVN to open a Krib account",
    des: "Krib is a platform that connects property owners directly to the buyers or tenants. We do not allow agents within the application. If for any reason, we find that you are an agent, your property will not be listed on Krib.",
    list: [],
  },
];

const FAQScreen = () => {
  const navigation = useNavigation();

  return (
    <View style={[ROOT]}>
      <Header
        leftIcon={true}
        title="FAQs"
        // rightIcon
        onLeftPress={() => navigation.goBack()}
      />
      <ScrollView contentContainerStyle={styles.container}>
        <CustomText
          displayText="Krib Help Center"
          textWeight="500"
          textSize={20}
          textcolor="black"
          textStyle={{}}
        />
        <CustomText
          displayText="Get answers to all your questions about Krib"
          textWeight="500"
          textSize={20}
          textcolor="black"
          textStyle={{}}
        />

        {data.map((val, i) => {
          return (
            <Accordion
              headerTitle={val.title}
              headerTitleStyle={{ color: colors.black, fontSize: 16 }}
              style={{
                backgroundColor: colors.white2,

                // shadowColor: "transparent",
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.2,
                shadowRadius: 1.41,

                elevation: 2,
                marginVertical: 10,
              }}
              showButton={false}
              buttonStyle={{
                height: 20,
                justifyContent: "center",
              }}
            >
              <Text>{val?.des}</Text>
              {val.list?.map((list, i) => {
                return <Text>{list}</Text>;
              })}
            </Accordion>
          );
        })}
      </ScrollView>
    </View>
  );
};

export default FAQScreen;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingVertical: 15,
  },
});
