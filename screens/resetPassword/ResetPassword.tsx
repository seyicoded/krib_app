// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  TextInput,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";

import useReduxStore from "../../utils/hooks/useRedux";

// redux

// components
import {
  PersistLoginRecord,
  GetCompanyFullDetails,
  ShowNotification,
  GetCompanyKey,
  GetPersistData,
  PersistData,
} from "../../utils/UtilityFunctions";
// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomInput from "../../components/customInput/CustomInput";
import CustomButton from "../../components/customButton/CustomButton";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { ResetPassword } from "../../services/api";

// util

interface ResetPasswordModuleScreenProps { }

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const BODY: ViewStyle = {
  marginTop: 20,
};
const INPUTWRAP: ViewStyle = {
  width: "100%",
  justifyContent: "center",
  marginTop: 20,
  //   backgroundColor: "green",
};
const INPUTWRAP2: ViewStyle = {
  width: "100%",
  paddingHorizontal: 20,
  marginBottom: 20,

  //   backgroundColor: "green",
};
const SETINPUTSTYLE: TextStyle = {
  marginBottom: 10,
};

const ResetPasswordModule = ({ navigation, route }) => {
  const { userEmail } = route.params;
  const navigations = useNavigation();
  const [codeReceived, setCodeReceived] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [comfirmPassword, setComfirmPassword] = useState("");
  const [processing, setprocessing] = useState(false);


  const mode = "dark";

  const processResetPassword = async () => {
    if (codeReceived === "") {
      ShowNotification("Code is  required", "warning");
      return;
    }
    if (newPassword === "") {
      ShowNotification("Pls enter your new password", "warning");
      return;
    }
    if (comfirmPassword === "") {
      ShowNotification("Pls confirm your password", "warning");
      return;
    }
    if (comfirmPassword !== newPassword) {
      ShowNotification("Password not matched", "error");
      return;
    }
    let payload = {
      verificationcode: codeReceived,
      emailAddress: userEmail,
      password: newPassword,
    };
    // console.warn("here now");
    try {
      setprocessing(true)

      const result = await ResetPassword(payload);
      const { kind, data } = result;
      const loginData = data?.data;
      console.warn(data);
      setprocessing(false)

      if (kind === "ok") {
        ShowNotification("Password reset successful.");
        navigation.navigate("Login");
      } else {
        // const newMsg = "account not authorise, pls check you credential";
        const newMsg = data.message;
        ShowNotification(newMsg, "error");
      }
    } catch ({ message }) {
      ShowNotification(JSON.stringify(message), "error");
      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };

  // props

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftIcon={true}
        title="Password Reset"
        onLeftPress={() => navigation.goBack()}
      />
      <CustomText
        textSize={15}
        textWeight={"normal"}
        textcolor={colors.black}
        displayText={"A 6-digit reset code has been sent to your email. Enter the code and your new password."}
        textStyle={{
          textAlign: "left",
          marginBottom: 0,
          marginLeft: 20,
          marginTop: 15,
        }}
      />
      <ScrollView
        contentContainerStyle={{
          justifyContent: "center",
        }}
        style={{
          paddingVertical: 15,
        }}
      >
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View
              style={{
                justifyContent: "flex-start",
                minHeight: Layout.window.height,
                // minHeight: "100%",
                flex: 1,
              }}
            >
              <View style={INPUTWRAP2}>
                <CustomInput
                  inputText={"Enter code"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  onChangeText={(value: string) => {
                    setCodeReceived(value);
                  }}
                  placeHolderText={"Enter code"}
                  showIcon={false}
                />
              </View>

              <View style={INPUTWRAP2}>
                <CustomInput
                  inputText={"New password"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  secureTextEntry
                  onChangeText={(value: string) => {
                    setNewPassword(value);
                  }}
                  placeHolderText={"New password"}
                  showIcon={false}
                />
              </View>
              <View style={INPUTWRAP2}>
                <CustomInput
                  inputText={"Confirm password"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  secureTextEntry
                  onChangeText={(value: string) => {
                    setComfirmPassword(value);
                  }}
                  placeHolderText={"Confirm password"}
                  showIcon={false}
                />
              </View>

              <View style={{ paddingHorizontal: 15 }}>
                <CustomButton
                  btnText={processing === true ? "Processing...." : "Reset Password"}

                  handlePress={() => {
                    processResetPassword();
                    // navigation.navigate("Login");
                  }}
                />
              </View>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
    </View>
  );
};

export default ResetPasswordModule;
