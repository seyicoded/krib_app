// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";

// redux
import { ApplicationState } from "../../redux";
import RNMonnify from "@monnify/react-native-sdk";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import AlertModal from "../../components/alert/Alert";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { GetPersistData, ShowNotification } from "../../utils/UtilityFunctions";
import { PurchaseProperty } from "../../services/api";

import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import DialogInput from 'react-native-dialog-input';

export default function ShortLetInfo({ navigation, route }) {
  const { propertyData } = route.params;
  const [propertiesHolder, setPropertiesHolder] = useState(propertyData);
  const [startDate, setstartDate] = useState('2022-05-16');
  const [showDialog1, setshowDialog1] = useState(false);
  const [days, setdays] = useState('0');
  const [daysDisplayString, setdaysDisplayString] = useState('{}');
  const [daysMap, setdaysMap] = useState([]);
  const [amount, setamount] = useState(propertiesHolder?.price);
  const [loginProfile, setLoginProfile] = useState({});
  const dayRef = useRef([])
  // const [startDateHolder, setstartDateHolder] = useState(JSON.parse(`{"${startDate}": {marked: true, dotColor: '#50cebb'}}`));
  // console.log(JSON.stringify(
  // {
  //     first: {marked: true, dotColor: '#50cebb'}
  // }
  // ));

  // console.log(JSON.parse(`{"${startDate}": {"marked": true, "dotColor": "#50cebb"}}`))
  // console.log(JSON.parse('{"first":"Jane","last":"Doe"}'))


  useEffect(()=>{
    // var date_object = (new Date());
    // var month = ((date_object.getMonth() + 1).toString().length < 2) ? '0'+(date_object.getMonth() + 1) : (date_object.getMonth() + 1);
    // var day = ((date_object.getDate()).toString().length < 2) ? '0'+(date_object.getDate()) : (date_object.getDate());
    // date = date_object.getFullYear() + '-' + month + '-' + day;
    // setstartDate(date)
    // console.log(date)

    GetUserLogin();
  }, [])

  const GetUserLogin = async () => {
    try {
      let userToken = await GetPersistData("Loggedinstate");
      let loggedinUserdetails = await GetPersistData("loggedinUserdetails");
      loggedinUserdetails = JSON.parse(loggedinUserdetails);

      console.log(loggedinUserdetails.profile, "loggedinUserdetails.profile ");
      setLoginProfile(loggedinUserdetails?.profile);
    } catch (err) {}
  };

  const ReservePropNew = async (refrence) => {
    if (refrence === "") {
      ShowNotification("Payment refrence  is  required", "warning");
      return;
    }
    let propertyId = propertiesHolder.id;
    let payload = {
      propertyId: propertyId,
      transactionReference: refrence,
      days: daysMap.join(', '),
      time: (new Date(startDate)).toDateString()
    };
    // let payload = {
    //   emailAddress: "jinaddavid+00@gmail.com",
    //   password: "hash",
    // };
    // console.warn("here now");
    try {
      const result = await PurchaseProperty(payload);
      console.log(result, "resultresultresult");

      const { kind, data } = result;
      // const loginData = data?.data;
      // console.warn(result);

      if (kind === "ok") {
        setshowModal(true);
        // alert("please show success modal ");
        // console.log(data, "datadatadatadata");

        // ShowNotification("login hsuccessfully");
        // PersistData("UserToken", loginData?.token);
        // let logintoken_datas = {
        //   accessToken: loginData?.token,
        //   isloggedin: true,
        //   usertype: "normal",
        // };
        // PersistLoginRecord(loginData, logintoken_datas);
        // PersistData("MyProfile", profile);
        // PersistData("userId", data.data);
        // PersistData("Userdata", data.data);
        // dispatch(signInUserSuccess())
        // dispatch(setUserDetails(data.data))
        // navigation.navigate("TabHome");
        // return first_name !== data.data.first_name ? dispatch(savePin('')) : ''
      } else {
        const newMsg =
          data.message || "An error occure while recording the payment.";
        // const newMsg = data.message + data.reason;
        ShowNotification(newMsg, "error");
        // dispatch(notify(`${data.message}`, 'danger'))
        // dispatch(signInUserFailure())
      }
    } catch ({ message }) {
      let newMsg = "An error occure while recording the payment";
      ShowNotification(newMsg, "error");

      // ShowNotification(message);
      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };

  return (
    <ScrollView style={styles.container}>
      <Text >{''}</Text>
      <Header
        leftIcon={true}
        rightText={'\n'}
        onLeftPress={() => navigation.goBack()}
      />

      <Text style={{marginTop: 20}}>{'\n'}</Text>
      
      <Text style={[styles.text, {marginLeft: 20, marginBottom: 4}]}>
        Select from Calendar
      </Text>

      <Calendar 
        onDayPress={(day) => {
          // console.log(day)
          setstartDate(day.dateString)
          // toggle day into list
          var daysMapinstance = dayRef.current;
          var newdaysMapinstance = [];
          var count = 0;

          var hasAlready = false;
          for (let index = 0; index < daysMapinstance.length; index++) {
            const element = daysMapinstance[index];

            if(element != day.dateString){
              newdaysMapinstance[count] = element;
              count++;
            }else{
              hasAlready = true;
            }
          }

          if(hasAlready){
            console.log('hasAlready'+count)
          }else{
            // console.log('hasAlready')
            newdaysMapinstance[daysMapinstance.length] = day.dateString;
          }

          setdays(newdaysMapinstance.length)
          setdaysMap(newdaysMapinstance);
          dayRef.current = newdaysMapinstance;
          // console.log(newdaysMapinstance);

          // set calendar string viewer
          var s = "{";
          newdaysMapinstance.forEach((element, index) => {
            if(newdaysMapinstance.length == 1){
              s += `"${element}": {"marked": true, "dotColor": "#50cebb"}`;
            }else{
              if((newdaysMapinstance.length - 1) == index){
                s += `"${element}": {"marked": true, "dotColor": "#50cebb"}`;
              }else{
                s += `"${element}": {"marked": true, "dotColor": "#50cebb"},`;
              }
              
            }

            // console.log(index)
            
          });
          s += '}';
          console.log(s)
          setdaysDisplayString(s);

          setamount(parseInt(propertiesHolder?.price) * parseInt(newdaysMapinstance.length))
          // daysDisplayString
        }}
        markingType="period"
        // markedDates={JSON.parse(`{"${startDate}": {"marked": true, "dotColor": "#50cebb"}}`)}
        markedDates={JSON.parse(daysDisplayString)}
        theme={{
          backgroundColor: colors.white2,
          calendarBackground: colors.white2,
          textDayHeaderFontWeight: 'bold',
          textMonthFontWeight: 'bold',
          textMonthFontWeight: '600',
        }}
        />

      <Text style={{marginTop: 2}}>{'\n'}</Text>
      <View style={{padding: 20, paddingTop: 10}}>

        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 14}}>
          <Text style={styles.text}>Book dates:</Text>  
          <View>
            {
              daysMap.map((item, indexx) => <Text key={indexx} style={[styles.text, {marginBottom: 4, textAlign: 'right'}]}>{(new Date(item)).toDateString()}</Text>  )
            }
            {/* <Text style={styles.text}>{(new Date(startDate)).toDateString()}</Text>   */}
          </View>
        </View>

        {/* <Text style={styles.text}>Book start date: {(new Date(startDate)).toDateString()}</Text> */}

        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 14}}>
          <Text style={styles.text}>Number of days you staying:</Text>  
          <Text style={styles.text}><TouchableOpacity onPress={()=>{return; setshowDialog1(true)}}><Text style={{fontWeight: "500", borderColor: 'rgba(0, 0, 0, 0.45)', borderWidth: 1, padding: 4, borderRadius: 4, elevation: 3}}>{days} { ( parseInt(days) == 1 ) ? 'day':'days'}</Text></TouchableOpacity></Text>  
        </View>


        {/* <Text style={styles.text}>Number of days you staying: <TouchableOpacity onPress={()=>setshowDialog1(true)}><Text style={{fontWeight: "500", borderColor: 'rgba(0, 0, 0, 0.45)', borderWidth: 1, padding: 4, borderRadius: 4, elevation: 3}}>{days} days</Text></TouchableOpacity></Text> */}

        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 14}}>
          <Text style={styles.text}>Total amount:</Text>  
          <Text style={styles.text}><Text style={{fontWeight: 'bold'}}>
            <Naira
              amount={amount}
              amountWeight={"bold"}
              boldnaira
            />
          </Text></Text>  
        </View>

        {/* <Text style={styles.text}>Total amount: 
          <Text style={{fontWeight: 'bold'}}>
            <Naira
              amount={amount}
              amountWeight={"bold"}
              boldnaira
            />
          </Text>
        </Text> */}

        <TouchableOpacity onPress={()=>{
          // logic to process
          // start

          // console.log(daysMap.join(', '))
          // return;

          console.log(loginProfile, "loginProfile");
          let customerName = loginProfile?.usersProfile?.fullname;
          let customerEmail = loginProfile?.username;
          console.log(propertiesHolder, "propertiesHolder");
          let propertyPrice = propertiesHolder.price;
          let paymentDescription = "Krib Property Payment For Shortlet @"+days+" days";
          // alert(customerName)
          // alert(propertyPrice)
          RNMonnify.initializePayment({
            amount: amount,
            customerName: customerName,
            customerEmail: customerEmail,
            paymentReference: "" + Math.floor(Math.random() * 1000000000 + 1),
            paymentDescription: paymentDescription,
            currencyCode: "NGN",
            incomeSplitConfig: [],
            paymentMethods: ["CARD", "ACCOUNT_TRANSFER"],
          })
            .then((response) => {
              console.log(response, "lllll");
              let refrence = response.transactionReference;
              let transactionStatus = response.transactionStatus;
              if (transactionStatus === "CANCELLED") {
                // ShowNotification("Payment cancelled", "warning");
                return;
              }
              ReservePropNew(refrence);
              // card charged successfully, get reference here
            })
            .catch((error) => {
              console.log(error); // error is a javascript Error object
              console.log(error.message);
              console.log(error.code);
              alert(error.message);
            });

          // stop
        }} style={styles.next_container}>
          <Text style={styles.next}>Next</Text>
        </TouchableOpacity>
      </View>

      <DialogInput isDialogVisible={showDialog1}
            title={"Number of days"}
            message={"Enter Days Booking"}
            hintInput ={"e.g, 5"}
            textInputProps={{autoCorrect:false}}
            submitInput={ (inputText) => {
              setdays(inputText);
              setshowDialog1(false)
              // SET PRICE
              setamount(parseInt(propertiesHolder?.price) * parseInt(inputText))
            } }
            closeDialog={ () => {setshowDialog1(false)}}>
      </DialogInput>
      

    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white2,
  },
  text: {
    fontSize: 16,
    fontWeight: "500",
    opacity: 0.8,
  },
  next: {
    fontSize: 16,
    fontWeight: "600",
    color: '#F1F1F1',
  },
  next_container: {
    width: '100%',
    backgroundColor: '#001D38',
    color: '#F1F1F1',
    alignItems: 'center',
    padding: 16,
    marginTop: 90,
    borderRadius: 8,
    elevation: 3,
    shadowColor: 'rgba(0, 29, 56, 0.5)',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4
  }
});