// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";

// redux
import { ApplicationState } from "../../redux";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";

// util

interface SellersProfileScreenProps { }

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: 240,
  marginRight: 10,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BTNSTYLE: TextStyle = {
  borderRadius: 5,
  backgroundColor: colors.dark2,
  justifyContent: "center",
  alignItems: "center",
  padding: 15,
  width: "100%",
  marginTop: 40,
};

const SellersProfile = ({ navigation, route }) => {
  const navigations = useNavigation();
  const { propertyScheduleData } = route.params;
  console.warn(propertyScheduleData, "property schedule data");

  const mode = "dark";

  // props

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      {/* <Header
        leftIcon={true}
        title="Profile"
        rightIcon
        onLeftPress={() => navigation.goBack()}
      /> */}
      <ScrollView>
        <CustomText
          textSize={16}
          textWeight={"500"}
          textcolor={colors.black}
          displayText={"We’ve received your request!"}
          textStyle={{ marginTop: 64, textAlign: "center" }}
        />
        <CustomText
          textSize={16}
          textWeight={"normal"}
          textcolor={colors.fadeDark}
          displayText={"We are working on scheduling your"}
          textStyle={{ marginTop: 14, textAlign: "center" }}
        />
        <CustomText
          textSize={16}
          textWeight={"normal"}
          textcolor={colors.fadeDark}
          displayText={"consultation with Oyin"}
          textStyle={{ marginTop: 0, textAlign: "center" }}
        />
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View
              style={{
                paddingHorizontal: 15,
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
              }}
            >
              {/* <ImageBackground
                source={images.profile}
                style={{
                  width: 149,
                  height: 110,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              > */}
              <Image
                source={images.oyin}
                style={{
                  width: 120,
                  height: 120,
                  borderRadius: 120 / 2,
                  // marginLeft: -10,
                }}
              />
              {/* </ImageBackground> */}
            </View>
            <View style={[{ paddingHorizontal: 15 }]}>
              <CustomText
                textSize={16}
                textWeight={"bold"}
                textcolor={colors.black}
                displayText={"Oyinkansola Tella"}
                textStyle={{ marginTop: 14, textAlign: "center" }}
              />

              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.dark}
                displayText={"Client Service Personnel"}
                textStyle={{ marginBottom: 6, textAlign: "center" }}
              />

              <View
                style={[
                  FLEXROW,
                  { justifyContent: "space-between", marginTop: 20 },
                ]}
              >
                <View style={{ width: "50%" }}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Address"}
                    textStyle={{ marginBottom: 6 }}
                  />
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.dark}
                    displayText={propertyScheduleData?.address}
                    textStyle={{ marginBottom: 6 }}
                  />
                </View>
                <View style={{ width: "50%" }}>
                  <CustomText
                    textSize={16}
                    textWeight={"500"}
                    textcolor={colors.black}
                    displayText={"Time"}
                    textStyle={{ marginBottom: 6 }}
                  />
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.dark}
                    displayText={`${propertyScheduleData?.scheduleDate} ${propertyScheduleData?.scheduleTime}`}
                    textStyle={{ marginBottom: 6 }}
                  />
                </View>
              </View>
              <CustomText
                textSize={20}
                textWeight={"500"}
                textcolor={colors.black}
                displayText={"What’s next? "}
                textStyle={{ marginBottom: 31, marginTop: 33 }}
              />
              <View
                style={[
                  FLEXROW,
                  {
                    marginTop: 5,
                    alignItems: "flex-start",
                    justifyContent: "flex-start",
                  },
                ]}
              >
                <View>
                  <Image
                    source={images.img1}
                    style={{ width: 10, height: 11 }}
                  />
                </View>
                <View>
                  <Image source={images.call} />
                  <View style={{ width: "90%" }}>
                    <CustomText
                      textSize={14}
                      textWeight={"normal"}
                      textcolor={colors.dark}
                      displayText={
                        "We will call you within 1 hour to confirm your in- home consultation and provide you with tips on how to prepare your property for capturing. "
                      }
                      textStyle={{
                        marginBottom: 6,
                        paddingHorizontal: 15,
                        lineHeight: 21,
                        textAlign: "justify",
                      }}
                    />
                  </View>
                </View>
              </View>
              <View
                style={[
                  FLEXROW,
                  {
                    marginTop: 5,
                    alignItems: "flex-start",
                    justifyContent: "flex-start",
                  },
                ]}
              >
                <View>
                  <Image
                    source={images.img2}
                    style={{ width: 10, height: 11 }}
                  />
                </View>
                <View>
                  <Image source={images.house} />
                  <View style={{ width: "90%" }}>
                    <CustomText
                      textSize={14}
                      textWeight={"normal"}
                      textcolor={colors.dark}
                      displayText={
                        "Your personnel will access your home’s condition, do a proper valuation of your property and capture your listing."
                      }
                      textStyle={{
                        marginBottom: 6,
                        paddingHorizontal: 15,
                        lineHeight: 21,
                        textAlign: "justify",
                      }}
                    />
                  </View>
                </View>
              </View>
              <View
                style={[
                  FLEXROW,
                  {
                    marginTop: 5,
                    alignItems: "flex-start",
                    justifyContent: "flex-start",
                  },
                ]}
              >
                <View>
                  <Image
                    source={images.img3}
                    style={{ width: 10, height: 11 }}
                  />
                </View>
                <View>
                  <Image source={images.comment} />
                  <View style={{ width: "90%" }}>
                    <CustomText
                      textSize={14}
                      textWeight={"normal"}
                      textcolor={colors.dark}
                      displayText={
                        "Your property goes live on Krib mobile application! Receive messages and close home sale/ lease faster with your purchaser."
                      }
                      textStyle={{
                        marginBottom: 6,
                        paddingHorizontal: 15,
                        lineHeight: 21,
                        textAlign: "justify",
                      }}
                    />
                  </View>
                </View>
              </View>

              <TouchableOpacity
                style={[BTNSTYLE, { marginBottom: 20 }]}
                onPress={() => {
                  navigation.navigate("Explore");
                }}
              >
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.white}
                  displayText={"Finish"}
                  textStyle={{ textAlign: "center" }}
                />
              </TouchableOpacity>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
    </View>
  );
};

export default SellersProfile;
