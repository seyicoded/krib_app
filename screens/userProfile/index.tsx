// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";

// redux
import { ApplicationState } from "../../redux";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import { GetPersistData } from "../../utils/UtilityFunctions";
import { getviewedproperty } from "../../services/api";
import SellersProfile from "../sellerProfile";
import BuyersProfile from "../buyerProfile";
import ThankYou from "../thankYou";

// util

interface userProfileScreenProps {}

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: "100%",
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BTNSTYLE: TextStyle = {
  borderRadius: 5,
  backgroundColor: colors.dark2,
  justifyContent: "center",
  alignItems: "center",
  padding: 15,
  width: "100%",
  marginTop: 40,
};

const userProfile = ({ navigation, route }) => {
  const navigations = useNavigation();
  const [userDataGotten, setUserDataGotten] = useState(null);
  const [viewedPropertys, setViewedProperty] = useState([]);
  const [listedProperty, setListedProperty] = useState([]);
  const [reservedProperty, setReservedProperty] = useState([]);
  const [loginState, setLoginState] = useState(false);
  const [ShowSellerProfile, setShowSellerProfile] = useState(false);

  const mode = "dark";

  useEffect(() => {
    async function fetAllUserdata() {
      let userData = await GetPersistData("loggedinUserdetails");
      const newdata = JSON.parse(userData);
      console.warn(newdata, "ash new");
      setUserDataGotten(newdata?.profile);
    }
    fetAllUserdata();
    GetUserLogin();
  }, []);

  // useEffect(() => {
  //   // alert("hyey")

  // }, []);
  async function fetchViewedProperty() {
    // setLoadingState(true);
    // <Myspiner state={true} />;
    try {
      const result = await getviewedproperty();
      const { kind, data } = result;

      console.warn(data, "get viewed property");

      if (kind === "ok") {
        const newViewProperties = data?.data?.viewProperties;
        const newPropertyReserved = data?.data?.propertyReserved;
        const newListedProperty = data?.data?.listedProperty;
        const pendingProperty = data?.data?.pendingProperty;
        // console.warn(dd.length);listedPropeerty
        let showSellerprofile =
          pendingProperty?.length > 0 || newListedProperty?.length > 0
            ? true
            : false;

        setViewedProperty(newViewProperties);
        setListedProperty(newListedProperty);
        setReservedProperty(newPropertyReserved);
        setShowSellerProfile(showSellerprofile);

        // setLoadingState(false);
      }
    } catch ({ message }) {
      console.warn(message);

      // ShowNotification(message);
    }
  }

  const GetUserLogin = async () => {
    try {
      let userToken = await GetPersistData("Loggedinstate");
      let loggedinUserdetails = await GetPersistData("loggedinUserdetails");
      loggedinUserdetails = JSON.parse(loggedinUserdetails);

      console.log(loggedinUserdetails.profile, "loggedinUserdetails.profile ");
      // setLoginProfile(loggedinUserdetails?.profile);

      let ss = JSON.parse(userToken);
      console.log(ss, "login stateeee");
      if (ss?.isloggedin === true) {
        setLoginState(ss?.isloggedin);
        // setLoginState(false);
        fetchViewedProperty();
      } else {
        setLoginState(false);
      }
      console.log(ss, "login details ");
    } catch (err) {}
  };

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      {/* <SellersProfile /> */}
      {/* <BuyersProfile /> */}

      {loginState ? (
        ShowSellerProfile ? (
          <SellersProfile />
        ) : (
          <BuyersProfile />
          // <SellersProfile />
        )
      ) : (
        <ThankYou />
      )}
    </View>
  );
};

export default userProfile;
