// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import {
  PersistLoginRecord,
  GetPersistData,
  myspiner as Myspiner,
  SkeletonLoader,
  ShowNotification,
} from "../../utils/UtilityFunctions";
// redux
import { ApplicationState } from "../../redux";
import Addbank from "./addBank";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import {
  getProperty as apiGetProperty,
  getPropertyWithAuth as apiGetPropertyWithAuth,
  recordviewedProperty,
} from "../../services/api";
import RNMonnify from "@monnify/react-native-sdk";
// util

interface BankViewScreenProps { }

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};
const MAKEHEADER: ViewStyle = {
  width: "100%",
  height: 200,
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: colors.dark2,
};
const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  paddingHorizontal: 20,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDCONTAINER2: TextStyle = {
  //   justifyContent: "flex-end",
  // height: "100%",
  width: "100%",
  padding: 10,
  paddingHorizontal: 10,
  //   backgroundColor: "red",
  flexDirection: "row",
  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: "100%",
  borderRadius: 6,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BgColor = "#EE8727";

const BankView = ({ navigation, route }) => {
  const { userProfileDetails } = route.params;
  console.warn(userProfileDetails, "property data");
  const navigations = useNavigation();
  const [allPropertiesData, setAllpropertiesdata] = useState(null);
  const [propertiesHolderSale, setPropertiesHolderSale] = useState(null);
  const [propertiesHolderRent, setPropertiesHolderRent] = useState(null);
  const [loginState, setLoginState] = useState(false);
  const [loadingState, setLoadingState] = useState(false);
  const [loginProfile, setLoginProfile] = useState({});

  const mode = "dark";

  useEffect(() => {
    // GetPersistData("Loggedinstate")
    // console.warn(userToken[0]?.["isloggedin"]);

    GetUserLogin();
    // fetchMyAPI();
  }, []);
  const fetchMyAPI = async (status: boolean) => {
    setLoadingState(true);

    try {
      let result;
      if (status) {
        result = await apiGetPropertyWithAuth();
      } else {
        result = await apiGetProperty();
      }
      console.log(result, "resultresult");

      const { kind, data } = result;

      // console.warn(data.data, "first page");
      setLoadingState(false);

      if (kind === "ok") {
        setAllpropertiesdata(data.data);
      }
    } catch ({ message }) {
      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };
  const GetUserLogin = async () => {
    try {
      let userToken = await GetPersistData("Loggedinstate");
      let loggedinUserdetails = await GetPersistData("loggedinUserdetails");
      loggedinUserdetails = JSON.parse(loggedinUserdetails);

      console.log(loggedinUserdetails.profile, "loggedinUserdetails.profile ");
      setLoginProfile(loggedinUserdetails?.profile);

      let ss = JSON.parse(userToken);
      console.log(ss, "login stateeee");
      if (ss?.isloggedin === true) {
        setLoginState(ss?.isloggedin);
        // fetchMyAPI(true);
      } else {
        setLoginState(false);
        // fetchMyAPI(false);
      }
      console.log(ss, "login details ");
    } catch (err) { }
  };
  // useEffect(() => {
  //   let propertiesHolderSale = (allPropertiesData || []).map(
  //     (propertiesItems, index) => {
  //       return (
  //         <>
  //           {propertiesItems?.slesTypes === "SALE" ? (
  //             <TouchableOpacity key={index}>
  //               <ImageBackground
  //                 resizeMode="stretch"
  //                 source={images.bg2}
  //                 style={{
  //                   flex: 1,
  //                   //   width: 395,
  //                   padding: 10,
  //                   justifyContent: "center",
  //                 }}
  //               >
  //                 <View style={CARDCONTAINER}>
  //                   <Image
  //                     source={{
  //                       uri: propertiesItems?.propertyImages[0]?.url,
  //                     }}
  //                     style={CARDIMAGE}
  //                   />
  //                   <View style={FULLWIDTH}>
  //                     <CustomText
  //                       textSize={16}
  //                       textWeight={"normal"}
  //                       textcolor={colors.black}
  //                       displayText={propertiesItems?.address}
  //                       textStyle={{ marginBottom: 5, marginTop: 20 }}
  //                     />
  //                     <CustomText
  //                       textSize={16}
  //                       textWeight={"normal"}
  //                       textcolor={colors.black}
  //                       displayText={propertiesItems?.description}
  //                       textStyle={{ marginBottom: 5 }}
  //                     />
  //                     <Naira
  //                       amount={propertiesItems.price}
  //                       amountWeight={"bold"}
  //                       boldnaira
  //                     />
  //                   </View>
  //                 </View>
  //                 <TouchableOpacity
  //                   style={MOREBTN}
  //                   onPress={() => {
  //                     loginState === true
  //                       ? processPropertyView(propertiesItems)
  //                       : navigation.navigate("Main", {
  //                         screen: "ThankYou",
  //                       });
  //                   }}
  //                 >
  //                   <Image source={images.more} />
  //                 </TouchableOpacity>
  //               </ImageBackground>
  //             </TouchableOpacity>
  //           ) : (
  //             <></>
  //           )}
  //         </>
  //       );
  //     }
  //   );
  //   let propertiesHolderRent = (allPropertiesData || []).map(
  //     (propertiesItems, index1) => {
  //       return (
  //         <>
  //           {propertiesItems?.slesTypes !== "SALE" ? (
  //             <TouchableOpacity key={index1}>
  //               <ImageBackground
  //                 resizeMode="stretch"
  //                 source={images.bg2}
  //                 style={{
  //                   flex: 1,
  //                   //   width: 395,
  //                   padding: 10,
  //                   justifyContent: "center",
  //                 }}
  //               >
  //                 <View style={CARDCONTAINER}>
  //                   <Image
  //                     source={{
  //                       uri: propertiesItems?.propertyImages[0]?.url,
  //                     }}
  //                     style={CARDIMAGE}
  //                   />
  //                   <View style={FULLWIDTH}>
  //                     <CustomText
  //                       textSize={16}
  //                       textWeight={"normal"}
  //                       textcolor={colors.black}
  //                       displayText={propertiesItems?.address}
  //                       textStyle={{ marginBottom: 5, marginTop: 20 }}
  //                     />
  //                     <CustomText
  //                       textSize={16}
  //                       textWeight={"normal"}
  //                       textcolor={colors.black}
  //                       displayText={propertiesItems?.description}
  //                       textStyle={{ marginBottom: 5 }}
  //                     />
  //                     <Naira
  //                       amount={propertiesItems.price}
  //                       amountWeight={"bold"}
  //                       boldnaira
  //                     />
  //                   </View>
  //                 </View>
  //                 <TouchableOpacity
  //                   style={MOREBTN}
  //                   // onPress={() => {
  //                   //   navigation.navigate("Main", {
  //                   //     screen: "ViewDetails",
  //                   //     params: { propertyData: propertiesItems },
  //                   //   });
  //                   // }}
  //                   onPress={() => {
  //                     loginState === true
  //                       ? processPropertyView(propertiesItems)
  //                       : navigation.navigate("Main", {
  //                         screen: "ThankYou",
  //                       });
  //                   }}
  //                 >
  //                   <Image source={images.more} />
  //                 </TouchableOpacity>
  //               </ImageBackground>
  //             </TouchableOpacity>
  //           ) : (
  //             <></>
  //           )}
  //         </>
  //       );
  //     }
  //   );
  //   // console.warn(propertiesHolderRent, "what we have here ");

  //   setPropertiesHolderSale(propertiesHolderSale);
  //   setPropertiesHolderRent(propertiesHolderRent);
  // }, [allPropertiesData]);

  // const processPropertyView = async (datas) => {
  //   // console.warn(datas, "ddd");
  //   const getPropertyId = datas.id;
  //   const payload = {
  //     propertyId: getPropertyId,
  //   };
  //   try {
  //     let result;
  //     result = await recordviewedProperty(payload);

  //     // console.warn(result, "resultresult");

  //     const { kind, data } = result;

  //     // console.warn(data.data, "first page");
  //     // setLoadingState(false);

  //     if (kind === "ok") {
  //       navigation.navigate("Main", {
  //         screen: "ViewDetails",
  //         params: { propertyData: datas },
  //       });
  //     } else {
  //       const err = data?.message;
  //       // console.warn(err);
  //       navigation.navigate("Main", {
  //         screen: "ViewDetails",
  //         params: { propertyData: datas },
  //       });

  //       // ShowNotification(err, "error");
  //     }
  //   } catch ({ message }) {
  //     // console.warn(message);
  //     // ShowNotification({ message });
  //     // dispatch(signInUserFailure())
  //     // dispatch(notify(`${message}`, 'danger'))
  //   }
  // };

  // props
  let dates = userProfileDetails?.bankDetail?.updatedAt.split("T");
  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftIcon={true}
        onLeftPress={() => navigation.goBack()}
        title={
          userProfileDetails?.bankDetail === null
            ? "Add Bank"
            : "Withdrawal Account"
        }
      />

      <ScrollView style={{ paddingVertical: 15, marginTop: 15 }}>
        {userProfileDetails?.bankDetail === null ? (
          // && Object.keys(userProfileDetails?.bankDetail).length === 0 && userProfileDetails?.bankDetail.constructor === Object ?
          <View style={{ flex: 1 }}>
            {/* <View
              style={[
                FLEXROW,
                {
                  justifyContent: "center",
                  alignItems: "center",
                  // ju
                  width: "100%",
                  marginTop: 10,
                  flex: 1,
                  marginTop: 10,
                },
              ]}
            >
              <CustomText
                textSize={15}
                textWeight={"bold"}
                textcolor={colors.dark}
                displayText={"No bank added yet, Pls add your bank"}
                textStyle={{ textAlign: "left" }}
              />
            </View> */}
            <Addbank navigation={navigation} route={route} hideHeader={true} />
          </View>
        ) : (
          <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
            <Animatable.View animation={"slideInUp"}>
              <View style={{ width: "100%", flex: 1 }}>
                <ImageBackground
                  resizeMode="stretch"
                  source={images.ccWrap}
                  style={{
                    flex: 1,
                    //   width: 395,
                    paddingHorizontal: 30,
                    // justifyContent: "center",
                    marginBottom: 15,
                    minHeight: 210,

                    //   backgroundColor: "#F2F2F2",
                    //   borderRadius: 10,
                    //   elevation: 1,
                    //   marginVertical: 15,
                  }}
                >
                  <TouchableOpacity style={CARDCONTAINER2}>
                    <View style={{ flex: 1 }}>
                      <View
                        style={[
                          FLEXROW,
                          {
                            justifyContent: "space-between",
                            width: "100%",
                            marginTop: 10,
                          },
                        ]}
                      >
                        <CustomText
                          textSize={16}
                          textWeight={"bold"}
                          textcolor={colors.black}
                          displayText={
                            userProfileDetails?.bankDetail?.AccountName
                          }
                          textStyle={{ textAlign: "left" }}
                        />
                      </View>
                      <View
                        style={[
                          // FLEXROW,
                          {
                            // justifyContent: "space-between",
                            width: "100%",
                            marginTop: 20,
                          },
                        ]}
                      >
                        <CustomText
                          textSize={14}
                          textWeight={"bold"}
                          textcolor={colors.black}
                          displayText={userProfileDetails?.bankDetail?.BankName}
                          textStyle={{ textAlign: "left" }}
                        />
                        <CustomText
                          textSize={14}
                          textWeight={"bold"}
                          textcolor={colors.dark}
                          displayText={
                            userProfileDetails?.bankDetail?.AccountNumber
                          }
                          textStyle={{ textAlign: "left", marginTop: 8 }}
                        />
                        {/* <CustomText
                          textSize={14}
                          textWeight={"normal"}
                          textcolor={colors.dark}
                          displayText={`Recent Update: ${dates}`}
                          textStyle={{ textAlign: "left" }}
                        /> */}
                      </View>
                    </View>
                    <Image
                      source={images.cc}
                    // style={{ width: 120, height: 100 }}
                    />
                  </TouchableOpacity>
                  <View style={{ paddingHorizontal: 10 }}>
                    <CustomText
                      textSize={14}
                      textWeight={"normal"}
                      textcolor={colors.dark}
                      displayText={`Recent Update: ${dates}`}
                      textStyle={{ textAlign: "left" }}
                    />
                  </View>
                </ImageBackground>
              </View>
            </Animatable.View>
          </Animatable.View>
        )}
      </ScrollView>
      <View style={{ alignItems: "flex-end" }}>
        <TouchableOpacity
          onPress={() => navigation.navigate("Main", { screen: "Addbank" })}
          style={{ padding: 20 }}
        >
          <Image source={images.plusIcon} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BankView;
