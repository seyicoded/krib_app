// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  CheckBox,
  RadioButton,
} from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import MapView from "react-native-maps";
import {
  PersistLoginRecord,
  GetCompanyFullDetails,
  ShowNotification,
  GetCompanyKey,
  GetPersistData,
  PersistData,
  cloudinaryUpload,
} from "../../utils/UtilityFunctions";
// redux
import { ApplicationState } from "../../redux";

// components
import {
  YOUR_CLOUDINARY_PRESET,
  YOUR_CLOUDINARY_NAME,
  requestCameraPermission,
  YOUR_CLOUDINARY_PRESET_JobBanner,
} from "../../utils/UtilityFunctions";

import { launchCamera, launchImageLibrary } from "react-native-image-picker";

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomMap from "../../components/map/GooglePlacesInput";
import CustomInput from "../../components/customInput/CustomInput";
import CustomDate from "../../components/date/CustomDate";
import CustomTooltip from "../../components/tooltip/tooltip";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import {
  verifyAccount,
  addBank,
  getBankList,
  updateProfile,
} from "../../services/api";
import SelectDropdown from "react-native-select-dropdown";

// util

interface ProfileRegistrationScreenProps {}
const actionSheeYearBuilttRef = React.createRef();
const actionSheetUnitRef = React.createRef();

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  flexWrap: "wrap",
};
const FLEXROW2: TextStyle = {
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
};
const FLEXROWVERIFY: TextStyle = {
  width: "100%",
  alignItems: "flex-start",
  justifyContent: "flex-start",
  flexDirection: "row",
  flex: 1,
  marginBottom: 23,
};
const SETMARGIN: TextStyle = {
  marginRight: 10,
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: 240,
  marginRight: 10,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BTNSTYLE: TextStyle = {
  borderRadius: 5,
  backgroundColor: colors.dark2,
  justifyContent: "center",
  alignItems: "center",
  padding: 15,
  width: "100%",
  marginTop: 40,
};
const CHECKWRAP: TextStyle = {
  flex: 1,
  padding: 10,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "42%",
  width: "48%",
  height: 70,
  minWidth: "48%",
};
const CHECKWRAP3: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "48%",
  width: "45%",
  height: 70,
  minWidth: "40%",
};
const CHECKWRAP5: TextStyle = {
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  width: "43%",
  height: 120,
  minWidth: "40%",
  marginVertical: 10,
};
const CHECKWRAP2: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 15,
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  maxWidth: "100%",
  minWidth: "100%",
  height: 75,
  marginBottom: 10,
};
const LIKECHECKBOX: TextStyle = {
  flex: 1,
  borderWidth: 1,
  width: "97%",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 3,
};
const INTERIOCHECKER: TextStyle = {
  flex: 1,
  borderWidth: 0,
  width: "20%",
  maxWidth: "20%",
  justifyContent: "center",
  alignItems: "center",
  maxHeight: "91%",
  minHeight: "91%",
};
const INPUTWRAP2: ViewStyle = {
  width: "100%",
  paddingHorizontal: 15,
  // marginBottom: 20,
};
const RNCHECKBOXSTYLE: ViewStyle = {
  height: 5,
  width: 10,
  borderRadius: 10 / 2,
};
const SETINPUTSTYLE: TextStyle = {
  marginBottom: 10,
};

const ProfileRegistration = (props: {
  navigation: any;
  route: any;
  hideHeader: boolean;
}) => {
  const navigations = useNavigation();
  // const routeHeader = props;
  // console.warn(routeHeader);

  const [stepValue, setStepValue] = useState("Step 1");
  const [enteredlocation, setEnteredlocation] = useState("");
  const [listingTitle, setListingTitle] = useState("");
  const [priceTitle, setPriceTitle] = useState("");
  const [describedProperty, setDescribedProperty] = useState("");
  const [SquareMeter, setSquareMeter] = useState("");
  const [listingprice, setListingprice] = useState(0);
  const [bankList, setBankList] = useState([]);
  const [formatBankList, serFormatBankList] = useState([]);
  const [bankSelect, setBankSelect] = useState("");
  const [userAccountNumber, setAccountNumber] = useState("");
  const [userBankCode, setBankCode] = useState("");
  const [userBankName, setUserBankName] = useState("");
  const [userPassword, setPassword] = useState("");
  const [profileFound, setProfileFound] = useState(null);
  const [buttontext, SetButtonText] = useState("Add Bank");
  const [hideHeaderElement, SetHideHeaderElement] = useState(
    props.hideHeader || false
  );
  const [show, setShow] = useState(false);
  const mode = "dark";

  const ref = useRef();

  useEffect(() => {
    async function fetchAllBank() {
      try {
        const result = await getBankList();
        const { kind, data } = result;

        if (kind === "ok") {
          setBankList(data?.data);
          // processBankListGotten();
          // setLoadingState(false);
        }
      } catch ({ message }) {
        ShowNotification(message);
      }
    }
    fetchAllBank();
  }, []);

  useEffect(() => {
    bankList.forEach(function (arrayItem) {
      // serFormatBankList([...formatBankList, arrayItem?.name]);
      serFormatBankList((formatBankList) => [
        ...formatBankList,
        arrayItem?.name,
      ]);
    });
  }, [bankList]);
  const processBankListGotten = () => {
    // const newList = bankList;
    // newList?.map((item, index) => {
    //   serFormatBankList([...formatBankList, item?.name]);
    // });
    bankList.forEach(function (arrayItem) {
      // serFormatBankList([...formatBankList, arrayItem?.name]);
      serFormatBankList((formatBankList) => [
        ...formatBankList,
        arrayItem?.name,
      ]);
    });
  };

  const verifyUserAccount = async () => {
    if (userAccountNumber === "") {
      ShowNotification("Account Number is  Required", "warning");
      return;
    }

    setUserBankName("Loading...");

    let payload = {
      accountNumber: userAccountNumber,
      bankCode: userBankCode,
    };
    try {
      const result = await verifyAccount(payload);
      const { kind, data } = result;
      const loginData = data?.data;

      if (kind === "ok") {
        // navigation.navigate("TabHome");
        setUserBankName(loginData?.accountName);
      } else {
        setUserBankName(loginData?.accountName);
        const newMsg = data.message;
        ShowNotification(newMsg, "warning");
      }
    } catch ({ message }) {
      setUserBankName("");
      setAccountNumber("");

      ShowNotification(message, "warning");
    }
  };

  const processAddbank = async () => {
    if (userAccountNumber === "") {
      ShowNotification("Account Number is  Required", "warning");
      return;
    }
    if (userPassword === "") {
      ShowNotification("Pls enter your Password", "warning");
      return;
    }
    SetButtonText("Processing...");

    let payload = {
      accountNumber: userAccountNumber,
      bankCode: userBankCode,
      password: userPassword,
    };
    try {
      const result = await addBank(payload);
      const { kind, data } = result;
      const loginData = data?.data;

      if (kind === "ok") {
        // navigation.navigate("TabHome");
        SetButtonText("Add Bank");
        ShowNotification("Bank Added Successfully", "success");
        props.navigation.navigate("Main", { screen: "Viewbank" });
      } else {
        SetButtonText("Add Bank");
        const newMsg = data.message;
        ShowNotification(newMsg, "warning");
      }
    } catch ({ message }) {
      SetButtonText("Add Bank");
      ShowNotification(message, "danger");
    }
  };

  // const processSelectBank = (selectedItem, index) => {
  //   const banks = bankList;
  //   const bankDetails = banks[index];
  //   setBankCode(bankDetails?.code);
  //   setBankSelect(selectedItem);
  // };
  const handleBankBuilt = () => {
    actionSheeYearBuilttRef?.current?.show();
  };
  const handlegeneralAction = (params, params2) => {
    params(params2);
  };

  const countries = ["Egypt", "Canada", "Australia", "Ireland"];
  console.warn(formatBankList, "let see");

  const handleBankSelect = (selectedItem, index) => {
    console.warn(index);

    const banks = bankList;
    const bankDetails = banks[index];
    setBankCode(bankDetails?.code);
    setBankSelect(selectedItem);
    // setYearSelect(params.toString());
    actionSheeYearBuilttRef?.current?.hide();
  };

  let perparedBankValue = (formatBankList || []).map(
    (converBankValueItems, index) => {
      return (
        <TouchableOpacity
          onPress={() => {
            handleBankSelect(converBankValueItems, index);
          }}
          style={{
            borderBottomWidth: 1,
            borderBottomColor: "#ccc",
            paddingVertical: 10,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          {bankSelect === converBankValueItems ? (
            <Image source={images.checktrue} />
          ) : (
            <Image source={images.checkfalse} />
          )}

          {/* <CheckBox
            style={RNCHECKBOXSTYLE}
            // value={yearSelect === converBankValueItems ? true : false}
          /> */}
          <CustomText
            textSize={14}
            textWeight={"normal"}
            textcolor={colors.black}
            displayText={`${converBankValueItems}`}
            textStyle={{
              marginLeft: 25,
            }}
          />
        </TouchableOpacity>
      );
    }
  );

  const actionSheetYearBuiltBoddy = (
    <View style={{ maxHeight: Layout.window.height - 100, paddingTop: 15 }}>
      <ScrollView>
        {<View style={{ padding: 20 }}>{perparedBankValue}</View> || (
          <View>
            <CustomText
              textSize={14}
              textWeight={"normal"}
              textcolor={colors.black}
              displayText={"....."}
              textStyle={{
                marginLeft: 25,
              }}
            />
          </View>
        )}
      </ScrollView>
    </View>
  );

  // props

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      {hideHeaderElement === false && (
        <Header
          leftIcon={true}
          title={"Add Bank"}
          rightIcon={false}
          onLeftPress={() => props.navigation.goBack()}
        />
      )}

      <ScrollView style={{ paddingVertical: 15, paddingHorizontal: 15 }}>
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View style={{ display: "flex" }}>
              {/* <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.black}
                displayText={"Select Bank"}
                textStyle={{
                  marginHorizontal: 10,
                  marginBottom: 6,
                  textAlign: "left",
                }}
              /> */}
              {/* <ImageBackground
                resizeMode="stretch"
                source={images.inputCard}
                style={{
                  width: "95%",
                  height: 64,
                  justifyContent: "center",
                  // alignItems: "center",
                  alignSelf: "center",
                }}
              > */}
              {/* <SelectDropdown
                  data={formatBankList}
                  onSelect={(selectedItem, index) => {
                    processSelectBank(selectedItem, index);
                  }}
                  buttonTextAfterSelection={(selectedItem, index) => {
                    // text represented after item is selected
                    // if data array is an array of objects then return selectedItem.property to render after item is selected
                    return selectedItem;
                  }}
                  rowTextForSelection={(item, index) => {
                    // text represented for each item in dropdown
                    // if data array is an array of objects then return item.property to represent item in dropdown
                    return item;
                  }}
                  buttonStyle={{
                    width: "85%",
                    flexDirection: "row",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    backgroundColor: "transparent",
                  }}
                  buttonTextStyle={{
                    textAlign: "left",
                    fontSize: 12,
                  }}
                /> */}
              {/* </ImageBackground> */}
              <CustomText
                textSize={16}
                textWeight={"normal"}
                textcolor={colors.black}
                displayText={"Select Bank"}
                textStyle={{
                  marginHorizontal: 10,
                  marginBottom: 6,
                  textAlign: "left",
                  marginLeft: 12,
                }}
              />
              <ImageBackground
                resizeMode="stretch"
                source={images.inputCard}
                style={{
                  width: "96%",
                  height: 64,
                  justifyContent: "center",
                  // alignItems: "center",
                  alignSelf: "center",
                  marginLeft: 10,
                }}
                // style={(this, this.props.inputSty || styles.GeneralInputSty)}
              >
                <TouchableOpacity
                  onPress={() => {
                    handleBankBuilt();
                  }}
                  style={[
                    LIKECHECKBOX,
                    {
                      borderColor: colors.transparent,
                      flexDirection: "row",
                      justifyContent: "space-between",
                      paddingLeft: 15,
                    },
                  ]}
                >
                  <CustomText
                    textSize={13}
                    textWeight={"normal"}
                    textcolor={colors.black}
                    displayText={`${bankSelect}`}
                    // displayText={`Year built  ${yearSelect != "" ? yearSelect : ''}`}
                    textStyle={{
                      lineHeight: 18,
                    }}
                  />
                </TouchableOpacity>
              </ImageBackground>
              <View style={[INPUTWRAP2, { display: "none" }]}>
                <CustomInput
                  editable={false}
                  inputText={"bank code"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  onChangeText={(value: number) =>
                    handlegeneralAction(setBankCode, value)
                  }
                  placeHolderText={""}
                  showIcon={false}
                  value={userBankCode}
                />
              </View>
              <View style={INPUTWRAP2}>
                <CustomInput
                  editable={userBankCode === "" ? false : true}
                  inputText={"Account Number"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  keyboardType={"numeric"}
                  maxLength={10}
                  onChangeText={(value: number) =>
                    handlegeneralAction(setAccountNumber, value)
                  }
                  onBlur={() => verifyUserAccount()}
                  placeHolderText={""}
                  showIcon={false}
                  value={profileFound?.fullname}
                />
              </View>
              <View style={{ alignItems: "flex-end" }}>
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.black}
                  displayText={userBankName}
                  textStyle={{
                    marginHorizontal: 15,
                    marginBottom: 6,
                    textAlign: "center",
                  }}
                />
              </View>

              <View style={INPUTWRAP2}>
                <CustomInput
                  editable={userAccountNumber === "" ? false : true}
                  inputText={"Password"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  onChangeText={(value: number) =>
                    handlegeneralAction(setPassword, value)
                  }
                  placeHolderText={""}
                  showIcon={false}
                  secureTextEntry={true}
                  value={profileFound?.phoneNumber}
                />
              </View>
            </View>

            <View style={[{ paddingHorizontal: 20, marginBottom: 22 }]}>
              <TouchableOpacity
                style={BTNSTYLE}
                onPress={() => processAddbank()}
                disabled={buttontext === "Add Bank" ? false : true}
              >
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.white}
                  displayText={buttontext}
                  textStyle={{ marginBottom: 6, textAlign: "center" }}
                />
              </TouchableOpacity>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
      <CustomTooltip
        actionSheetRef={actionSheeYearBuilttRef}
        actionSheetBody={actionSheetYearBuiltBoddy}
      />
    </View>
  );
};

export default ProfileRegistration;
