// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  CheckBox,
  RadioButton,
} from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import SmoothPinCodeInput from "react-native-smooth-pincode-input";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";
import MapView from "react-native-maps";
import {
  PersistLoginRecord,
  GetCompanyFullDetails,
  ShowNotification,
  GetCompanyKey,
  GetPersistData,
  PersistData,
  cloudinaryUpload,
} from "../../utils/UtilityFunctions";
// redux
import { ApplicationState } from "../../redux";

// components
import {
  YOUR_CLOUDINARY_PRESET,
  YOUR_CLOUDINARY_NAME,
  requestCameraPermission,
  YOUR_CLOUDINARY_PRESET_JobBanner,
} from "../../utils/UtilityFunctions";

import { launchCamera, launchImageLibrary } from "react-native-image-picker";

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomMap from "../../components/map/GooglePlacesInput";
import CustomInput from "../../components/customInput/CustomInput";
import CustomDate from "../../components/date/CustomDate";
import CustomTooltip from "../../components/tooltip/tooltip";
import NumberCarousel from "../../components/carousel/carousel";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import {
  getPropertyType,
  addBank,
  getProfile,
  updateProfile,
} from "../../services/api";

// util

interface SecurePinScreenProps {}
const actionSheeYearBuilttRef = React.createRef();
const actionSheetUnitRef = React.createRef();

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  flexWrap: "wrap",
};
const FLEXROW2: TextStyle = {
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
};
const FLEXROWVERIFY: TextStyle = {
  width: "100%",
  alignItems: "flex-start",
  justifyContent: "flex-start",
  flexDirection: "row",
  flex: 1,
  marginBottom: 23,
};
const SETMARGIN: TextStyle = {
  marginRight: 10,
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  //   backgroundColor: "red",

  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: 240,
  marginRight: 10,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 90,
};
const BTNSTYLE: TextStyle = {
  borderRadius: 5,
  backgroundColor: colors.dark2,
  justifyContent: "center",
  alignItems: "center",
  padding: 15,
  width: "100%",
  marginTop: 40,
};
const CHECKWRAP: TextStyle = {
  flex: 1,
  padding: 10,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "42%",
  width: "48%",
  height: 70,
  minWidth: "48%",
};
const CHECKWRAP3: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  // maxWidth: "48%",
  width: "45%",
  height: 70,
  minWidth: "40%",
};
const CHECKWRAP5: TextStyle = {
  padding: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
  width: "43%",
  height: 120,
  minWidth: "40%",
  marginVertical: 10,
};
const CHECKWRAP2: TextStyle = {
  flex: 1,
  padding: 10,
  paddingHorizontal: 15,
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  maxWidth: "100%",
  minWidth: "100%",
  height: 75,
  marginBottom: 10,
};
const LIKECHECKBOX: TextStyle = {
  flex: 1,
  borderWidth: 1,
  width: "97%",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 3,
};
const INTERIOCHECKER: TextStyle = {
  flex: 1,
  borderWidth: 0,
  width: "20%",
  maxWidth: "20%",
  justifyContent: "center",
  alignItems: "center",
  maxHeight: "91%",
  minHeight: "91%",
};
const INPUTWRAP2: ViewStyle = {
  width: "100%",
  paddingHorizontal: 15,
  marginBottom: 20,
};
const RNCHECKBOXSTYLE: ViewStyle = {
  height: 5,
  width: 10,
  borderRadius: 10 / 2,
};
const SETINPUTSTYLE: TextStyle = {
  marginBottom: 10,
};

const SecurePin = ({ navigation, route }) => {
  const navigations = useNavigation();
  const [stepValue, setStepValue] = useState("Step 1");
  const [enteredlocation, setEnteredlocation] = useState("");
  const [listingTitle, setListingTitle] = useState("");
  const [priceTitle, setPriceTitle] = useState("");
  const [describedProperty, setDescribedProperty] = useState("");
  const [SquareMeter, setSquareMeter] = useState("");
  const [listingprice, setListingprice] = useState(0);
  const [stateInputText, setStateInputText] = useState("");
  const [userAccountNumber, setAccountNumber] = useState("");
  const [userBankCode, setBankCode] = useState("");
  const [userPassword, setPassword] = useState("");
  const [profileFound, setProfileFound] = useState(null);
  const [buttontext, SetButtonText] = useState("Save");
  const [show, setShow] = useState(false);
  const mode = "dark";

  const ref = useRef();

  const processAddbank = async () => {
    if (userAccountNumber === "") {
      ShowNotification("Account Number is  Required");
      return;
    }
    if (userBankCode === "") {
      ShowNotification("Pls enter your Bank Code");
      return;
    }
    if (userPassword === "") {
      ShowNotification("Pls enter your Password");
      return;
    }
    SetButtonText("Processing...");

    let payload = {
      accountNumber: userAccountNumber,
      bankCode: userBankCode,
      password: userPassword,
    };
    try {
      const result = await addBank(payload);
      const { kind, data } = result;
      const loginData = data?.data;

      if (kind === "ok") {
        // navigation.navigate("TabHome");
        SetButtonText("Save");
        navigation.navigate("Main", { screen: "Viewbank" });
      } else {
        SetButtonText("Save");
        const newMsg = data.message;
        ShowNotification(newMsg);
      }
    } catch ({ message }) {
      SetButtonText("Save");
      ShowNotification(message);
    }
  };

  const handlegeneralAction = (params, params2) => {
    params(params2);
  };

  // props

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftIcon={true}
        title={"Pin & Security"}
        // rightIcon
        onLeftPress={() => navigation.goBack()}
      />

      <ScrollView style={{ paddingVertical: 15, paddingHorizontal: 15 }}>
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View>
              <View style={INPUTWRAP2}>
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.black}
                  displayText={
                    "Secure your account and payment information from unauthorized access with a 4 digit - PIN."
                  }
                  textStyle={{ marginBottom: 6 }}
                />
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.black}
                  displayText={"Create a 4-digit PIN"}
                  textStyle={{
                    marginTop: 50,
                    marginBottom: 26,
                    textAlign: "center",
                  }}
                />
                <View style={{ alignItems: "center" }}>
                  <SmoothPinCodeInput
                    value={stateInputText}
                    onTextChange={(code) => setStateInputText(code)}
                    cellSize={50}
                    codeLength={4}
                    cellStyle={{
                      borderWidth: 2,
                      borderRadius: 5,
                      marginLeft: 8,
                      marginRight: 8,
                    }}
                    // onFulfill={this._checkCode}
                    // onBackspace={this._focusePrevInput}
                  />
                </View>
              </View>
            </View>

            <View style={[{ paddingHorizontal: 20, marginBottom: 22 }]}>
              <TouchableOpacity
                style={BTNSTYLE}
                onPress={() => processAddbank()}
                disabled={buttontext === "Save" ? false : true}
              >
                <CustomText
                  textSize={16}
                  textWeight={"normal"}
                  textcolor={colors.white}
                  displayText={buttontext}
                  textStyle={{ marginBottom: 6, textAlign: "center" }}
                />
              </TouchableOpacity>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
    </View>
  );
};

export default SecurePin;
