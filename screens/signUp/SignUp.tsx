// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  TextInput,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { userSignUp } from "../../services/api";

import useReduxStore from "../../utils/hooks/useRedux";

// redux
import {
  PersistLoginRecord,
  GetCompanyFullDetails,
  ShowNotification,
  GetCompanyKey,
  GetPersistData,
  PersistData,
} from "../../utils/UtilityFunctions";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import CustomInput from "../../components/customInput/CustomInput";
import CustomButton from "../../components/customButton/CustomButton";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";

// util

interface SignUpModuleScreenProps { }

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const BODY: ViewStyle = {
  marginTop: 20,
};
const INPUTWRAP: ViewStyle = {
  width: "100%",
  justifyContent: "center",
  marginTop: 20,
  //   backgroundColor: "green",
};
const INPUTWRAP2: ViewStyle = {
  width: "100%",
  paddingHorizontal: 20,
  marginBottom: 20,

  //   backgroundColor: "green",
};
const SETINPUTSTYLE: TextStyle = {
  marginBottom: 10,
};

const SignUpModule = ({ navigation, route }) => {
  const navigations = useNavigation();
  const [fullname, setFullname] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const [password, setPassword] = useState("");
  const [processing, setprocessing] = useState(false);

  const mode = "dark";

  const processSignUp = async () => {
    if (fullname === "") {
      ShowNotification("Fullname is  required", "warning");
      return;
    }
    if (userEmail === "") {
      ShowNotification("Email is  required", "warning");
      return;
    }
    if (phoneNumber === "") {
      ShowNotification("Phone Number is  required", "warning");
      return;
    }
    if (password === "") {
      ShowNotification("please enter your password", "warning");
      return;
    }
    let payload = {
      emailAddress: userEmail,
      password: password,
      fullname: fullname,
      phoneNumber: phoneNumber,
      callbackurl: "http://localhost:3000/dashboard",
    };
    setprocessing(true)

    try {
      const result = await userSignUp(payload);
      const { kind, data } = result;
      const loginData = data?.data;
      console.warn(data);
      setprocessing(false)

      if (kind === "ok") {
        ShowNotification("A 6-digit verification code has been sent to your email.");
        navigation.navigate("Main", {
          screen: "UserVerification",
          params: { userEmail: userEmail },
        });
      } else {
        // const newMsg = "account not authorise, pls check you credential";
        const newMsg = data.message + data.reason;
        ShowNotification(newMsg);
      }
    } catch ({ message }) {
      setprocessing(false)

      ShowNotification(message);
    }
  };

  // props

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftIcon={false}

        title="Sign up" />
      <CustomText
        textSize={15}
        textWeight={"normal"}
        textcolor={colors.black}
        displayText={"Create a free account to sell, buy or rent properties."}
        textStyle={{
          textAlign: "left",
          marginBottom: 0,
          marginLeft: 20,
          marginTop: 15,
        }}
      />
      <ScrollView
        contentContainerStyle={{
          justifyContent: "center",
        }}
        style={{
          paddingVertical: 15,
        }}
      >
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View
              style={{
                justifyContent: "center",
                minHeight: Layout.window.height,
                // minHeight: "100%",
                flex: 1,
              }}
            >
              <View style={INPUTWRAP2}>
                <CustomInput
                  inputText={"Full name"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  onChangeText={(value: string) => {
                    setFullname(value);
                  }}
                  placeHolderText={"Full name"}
                  showIcon={false}
                />
              </View>

              <View style={INPUTWRAP2}>
                <CustomInput
                  inputText={"Email address"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  onChangeText={(value: string) => {
                    setUserEmail(value);
                  }}
                  placeHolderText={"Email address"}
                  showIcon={false}
                  keyboardType={"email-address"}
                  autoCapitalize='none'
                />
              </View>

              <View style={INPUTWRAP2}>
                <CustomInput
                  inputText={"Phone number"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  onChangeText={(value: string) => {
                    setPhoneNumber(value);
                  }}
                  placeHolderText={"Phone numbers"}
                  showIcon={false}
                  keyboardType={"phone-pad"}
                />
              </View>
              <View style={INPUTWRAP2}>
                <CustomInput
                  inputText={"Password"}
                  inputTextWeight={"normal"}
                  inputTextSize={15}
                  inputTextcolor={colors.black}
                  inputTextSty={SETINPUTSTYLE}
                  onChangeText={(value: string) => {
                    setPassword(value);
                  }}
                  placeHolderText={"Password"}
                  secureTextEntry
                  showIcon={false}
                />
              </View>

              <View style={{ paddingHorizontal: 15, paddingTop: 30 }}>
                <CustomButton
                  btnText={processing === true ? "Processing...." : "Create account"}
                  handlePress={() => {
                    processSignUp();
                    // navigation.navigate("TabHome");
                    // navigation.navigate("Main", { screen: "TabHome" });
                  }}
                />
              </View>
              <View
                style={{
                  paddingHorizontal: 15,
                  paddingVertical: 30,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <CustomText
                  textSize={16}
                  textWeight={"bold"}
                  textcolor={colors.black}
                  displayText={"Already have an account? "}
                  textStyle={{ textAlign: "right", marginBottom: 90 }}
                />
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("Login");
                  }}
                >
                  <CustomText
                    textSize={16}
                    textWeight={"600"}
                    textcolor={colors.yellow}
                    displayText={"Login"}
                    textStyle={{ textAlign: "right", marginBottom: 90 }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
    </View>
  );
};

export default SignUpModule;
