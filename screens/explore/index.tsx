// react
import * as React from "react";
import { useEffect, useRef, useState } from "react";

// react-native
import {
  ImageStyle,
  StatusBar,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  Platform,
  Appearance,
  FlatList,
  Animated,
  AppState,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  Dimensions
} from "react-native";

// third-party
import * as Animatable from "react-native-animatable";
import { translate } from "../../i18n";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";
import useReduxStore from "../../utils/hooks/useRedux";

import {
  PersistLoginRecord,
  GetPersistData,
  myspiner as Myspiner,
  SkeletonLoader,
  ShowNotification,
} from "../../utils/UtilityFunctions";
// redux
import { ApplicationState } from "../../redux";

// components

// styles
import { Layout } from "../../constants";
import { colors, fonts, images } from "../../theme";
import { Button } from "../../components/button";
import { Header } from "../../components/header";
import CustomText from "../../components/customText/CustomText";
import Naira from "../../components/Naira/Naira";
import { useFocusEffect } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import {
  getProperty as apiGetProperty,
  getPropertyWithAuth as apiGetPropertyWithAuth,
  recordviewedProperty,
} from "../../services/api";
import RNMonnify from "@monnify/react-native-sdk";

import { AppEventsLogger } from "react-native-fbsdk-next";
import analytics from '@react-native-firebase/analytics';
import NumberCarousel from "../../components/carousel/carousel";
import SimplePaginationDot from "../../components/carousel/indicator";
import Carousel from "react-native-anchor-carousel";
// util

const { width: windowWidth } = Dimensions.get("window");

interface OnboardingScreenProps {}

const ROOT: ViewStyle = {
  //   height: "100%",
  flex: 1,
  width: Layout.window.width,
  // backgroundColor: colors.white,
};

const APP_LOGO: ImageStyle = {
  alignSelf: "center",
  marginTop: Layout.window.height / 10,
};

const BODY: ViewStyle = {
  marginTop: 20,
};

const FLEXROW: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
};
const CARDCONTAINER: TextStyle = {
  //   justifyContent: "flex-end",
  height: "100%",
  width: "100%",
  padding: 10,
  paddingHorizontal: 20,
  // backgroundColor: "red",

  justifyContent: "space-evenly",
  marginTop: 15,
};
const CARDIMAGE: TextStyle = {
  height: 179,
  width: "100%",
  borderRadius: 6,
};
const FULLWIDTH: TextStyle = {
  width: "100%",
};

const MOREBTN: TextStyle = {};

/*
const MOREBTN: TextStyle = {
  position: "absolute",
  right: 20,
  bottom: 20,
};
*/

const GETSTARTED: TextStyle = {
  borderRadius: 24,
  borderColor: colors.dark2,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  width: 120,
};
const BgColor = "#EE8727";

const Onboarding = ({ navigation, route }) => {
  const navigations = useNavigation();
  const [allPropertiesData, setAllpropertiesdata] = useState([]);
  const [propertiesHolderSale, setPropertiesHolderSale] = useState([]);
  const [propertiesHolderSaleShow, setPropertiesHolderSaleShow] = useState(null);
  const [propertiesHolderRent, setPropertiesHolderRent] = useState([]);
  const [propertiesHolderShortLet, setPropertiesHolderShortLet] = useState([]);
  const [propertiesHolderRentShow, setPropertiesHolderRentShow] = useState(null);
  const [propertiesHolderShortLetShow, setPropertiesHolderShortLetShow] = useState(null);
  const [loginState, setLoginState] = useState(false);
  const [loadingState, setLoadingState] = useState(false);
  const [loginProfile, setLoginProfile] = useState({});
  const [indicatorSales, setindicatorSales] = useState(0);
  const [indicatorRent, setindicatorRent] = useState(0);
  const [indicatorShortlet, setindicatorShortlet] = useState(0);

  const mode = "dark";

  useEffect(() => {
    // GetPersistData("Loggedinstate")
    // console.warn(userToken[0]?.["isloggedin"]);

    GetUserLogin();
    // fetchMyAPI();
  }, []);
  const fetchMyAPI = async (status: boolean) => {
    setLoadingState(true);

    try {
      let result;
      if (status) {
        // Alert.alert('a')
        // result = await apiGetPropertyWithAuth();
        result = await apiGetProperty();
      } else {
        result = await apiGetProperty();
      }
      // console.log(result, "resultresult");

      const { kind, data } = result;

      // console.warn(data.data, "first page");
      setLoadingState(false);

      if (kind === "ok") {
        setAllpropertiesdata(data.data);
      }
    } catch ({ message }) {
      // console.warn(message);
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };
  const GetUserLogin = async () => {
    try {
      let userToken = await GetPersistData("Loggedinstate");
      let loggedinUserdetails = await GetPersistData("loggedinUserdetails");
      loggedinUserdetails = JSON.parse(loggedinUserdetails);

      // console.log(loggedinUserdetails.profile, "loggedinUserdetails.profile ");
      setLoginProfile(loggedinUserdetails?.profile);

      let ss = JSON.parse(userToken);
      // console.log(ss, "login stateeee");
      if (ss?.isloggedin === true) {
        setLoginState(ss?.isloggedin);
        fetchMyAPI(true);
      } else {
        setLoginState(false);
        fetchMyAPI(false);
      }
      // console.log(ss, "login details ");
    } catch (err) {}
  };
  useEffect(() => {


    // for sale
    // var salesData = (allPropertiesData || []).map((propertiesItems, index)=>{
    //   if(propertiesItems?.slesTypes === "SALE"){
    //     return {
    //       index: index,
    //       propertiesItems: propertiesItems,
    //       url: propertiesItems?.propertyImages[0]?.url,
    //       address: propertiesItems?.address,
    //       description: propertiesItems?.description,
    //       price: propertiesItems.price
    //     }
    //   }
      
    // })

    var salesData = [];
    var count = 0;

    for (let index = 0; index < allPropertiesData.length; index++) {
      const propertiesItems = allPropertiesData[index];
      if(propertiesItems?.slesTypes === "SALE"){
        salesData[count] =  {
          index: index,
          propertiesItems: propertiesItems,
          url: propertiesItems?.propertyImages[0]?.url,
          address: propertiesItems?.address,
          description: propertiesItems?.description,
          price: propertiesItems.price
        }
        count++;
      }
    }
    setPropertiesHolderSaleShow(salesData)

    // for rent
    var RentData = [];
    var count = 0;

    for (let index = 0; index < allPropertiesData.length; index++) {
      const propertiesItems = allPropertiesData[index];
      if(propertiesItems?.slesTypes === "RENT"){
        RentData[count] =  {
          index: index,
          propertiesItems: propertiesItems,
          url: propertiesItems?.propertyImages[0]?.url,
          address: propertiesItems?.address,
          description: propertiesItems?.description,
          price: propertiesItems.price
        }
        count++;
      }
    }
    setPropertiesHolderRentShow(RentData)

    // for shortlet
    var ShortLetData = [];
    var count = 0;

    for (let index = 0; index < allPropertiesData.length; index++) {
      const propertiesItems = allPropertiesData[index];
      if(propertiesItems?.slesTypes === "SHORTLET"){
        ShortLetData[count] =  {
          index: index,
          propertiesItems: propertiesItems,
          url: propertiesItems?.propertyImages[0]?.url,
          address: propertiesItems?.address,
          description: propertiesItems?.description,
          price: propertiesItems.price
        }
        count++;
      }
    }
    setPropertiesHolderShortLetShow(ShortLetData)


    
    // ignore
    // let propertiesHolderRent = (allPropertiesData || []).map(
    //   (propertiesItems, index1) => {
    //     return (
    //       <>
    //         {propertiesItems?.slesTypes === "RENT" ? (
    //           <TouchableOpacity
    //             style={{ width: 390 }}
    //             key={index1}
    //             onPress={() => {
    //               loginState === true
    //                 ? processPropertyView(propertiesItems)
    //                 : navigation.navigate("Main", {
    //                     screen: "ThankYou",
    //                   });
    //             }}
    //           >
    //             <ImageBackground
    //               resizeMode="stretch"
    //               source={images.bg2}
    //               style={{
    //                 flex: 1,
    //                 //   width: 395,
    //                 padding: 10,
    //                 justifyContent: "center",
    //               }}
    //             >
    //               <View style={[CARDCONTAINER]}>
    //                 <Image
    //                   source={{
    //                     uri: propertiesItems?.propertyImages[0]?.url,
    //                   }}
    //                   style={CARDIMAGE}
    //                 />
    //                 <View style={FULLWIDTH}>
    //                   <View style={FULLWIDTH}>
    //                     <CustomText
    //                       textSize={16}
    //                       textWeight={"normal"}
    //                       textcolor={colors.black}
    //                       displayText={propertiesItems?.address}
    //                       textStyle={{ marginBottom: 5, marginTop: 20 }}
    //                       numberOfLines={1}
    //                     />
    //                     <CustomText
    //                       textSize={16}
    //                       textWeight={"normal"}
    //                       textcolor={colors.black}
    //                       displayText={propertiesItems?.description}
    //                       textStyle={{ marginBottom: 2 }}
    //                       numberOfLines={2}
    //                     />
    //                   </View>
    //                   <View style={[FULLWIDTH, FLEXROW, {justifyContent: 'flex-start', marginBottom: 6}]}>
    //                     <Naira
    //                       amount={propertiesItems.price}
    //                       amountWeight={"bold"}
    //                       boldnaira
    //                     />
    //                     <Text style={{fontSize: 16, fontWeight: '400'}}>/Per Year</Text>
    //                   </View>
    //                 </View>
    //               </View>
    //             </ImageBackground>
    //           </TouchableOpacity>
    //         ) : (
    //           <></>
    //         )}
    //       </>
    //     );
    //   }
    // );

    // start
    // ignore
    // let propertiesHolderShortLet = (allPropertiesData || []).map(
    //   (propertiesItems, index1) => {
    //     return (
    //       <>
    //         {propertiesItems?.slesTypes === "SHORTLET" ? (
    //           <TouchableOpacity
    //             style={{ width: 390 }}
    //             key={index1}
    //             onPress={() => {
    //               loginState === true
    //                 ? processPropertyView(propertiesItems)
    //                 : navigation.navigate("Main", {
    //                     screen: "ThankYou",
    //                   });
    //             }}
    //           >
    //             <ImageBackground
    //               resizeMode="stretch"
    //               source={images.bg2}
    //               style={{
    //                 flex: 1,
    //                 //   width: 395,
    //                 padding: 10,
    //                 justifyContent: "center",
    //               }}
    //             >
    //               <View style={[CARDCONTAINER]}>
    //                 <Image
    //                   source={{
    //                     uri: propertiesItems?.propertyImages[0]?.url,
    //                   }}
    //                   style={CARDIMAGE}
    //                 />
    //                 <View style={FULLWIDTH}>
    //                   <View style={FULLWIDTH}>
    //                     <CustomText
    //                       textSize={16}
    //                       textWeight={"normal"}
    //                       textcolor={colors.black}
    //                       displayText={propertiesItems?.address}
    //                       textStyle={{ marginBottom: 5, marginTop: 20 }}
    //                       numberOfLines={1}
    //                     />
    //                     <CustomText
    //                       textSize={16}
    //                       textWeight={"normal"}
    //                       textcolor={colors.black}
    //                       displayText={propertiesItems?.description}
    //                       textStyle={{ marginBottom: 2 }}
    //                       numberOfLines={2}
    //                     />
    //                   </View>
    //                   <View style={[FULLWIDTH, FLEXROW, {justifyContent: 'flex-start', marginBottom: 6}]}>
    //                     <Naira
    //                       amount={propertiesItems.price}
    //                       amountWeight={"bold"}
    //                       boldnaira
    //                     />
    //                     <Text style={{fontSize: 16, fontWeight: '400'}}>/Per night</Text>
    //                   </View>
    //                 </View>
    //               </View>
    //             </ImageBackground>
    //           </TouchableOpacity>
    //         ) : (
    //           <></>
    //         )}
    //       </>
    //     );
    //   }
    // );
    // end
    // console.warn(propertiesHolderRent, "what we have here ");

    // setPropertiesHolderSale(propertiesHolderSale);
    // setPropertiesHolderRent(propertiesHolderRent);
    // setPropertiesHolderShortLet(propertiesHolderShortLet)
  }, [allPropertiesData]);

  const processPropertyView = async (datas) => {
    console.warn(datas, "ddd");
    const getPropertyId = datas.id;
    const payload = {
      propertyId: getPropertyId,
    };

    try{
      // ShowNotification('aaa')
      // Alert.alert('aaa')
      (async()=>{
        try{
          console.log('aaars')
          // Alert.alert('aaars')
          // ShowNotification('hellwoorld')
          var loggedinUserdetails = await GetPersistData("loggedinUserdetails");
          loggedinUserdetails = JSON.parse(loggedinUserdetails);
  
        console.log(loggedinUserdetails.profile.username, "loggedinUserdetails.profile ");
  
          // AppEventsLogger.logEvent(`Property ${datas.listingTitle} viewed by ${loggedinUserdetails.profile.username}`);
          AppEventsLogger.logEvent('property_clicked', {
            by: loggedinUserdetails.profile.username,
            property: datas.listingTitle,
            onDate: (new Date()).toString(),
            OS: Platform.OS
          });
  
          await analytics().logEvent('property_clicked', {
            by: loggedinUserdetails.profile.username,
            property: datas.listingTitle,
            onDate: (new Date()).toString(),
            OS: Platform.OS
          })
  
          // await analytics().logEvent('basketpp', {
          //   id: 3745092,
          //   item: 'mens grey t-shirt',
          //   description: ['round neck', 'long sleeved'],
          //   size: 'L',
          // })
  
          // Alert.alert('aaa')
  
          // await analytics().logSelectContent({
          //   content_type: 'clothing',
          //   item_id: 'abcd',
          // })
  
        }catch(e){
          // console.log(e);
          // setTimeout(()=>console.log(e), 6000)
          // Alert.alert("Error", "Something went wrong");
        }
      })()
    }catch(e){
      // Alert.alert('AN errror occurred')
    }

    try {
      let result;
      result = await recordviewedProperty(payload);

      console.warn(result, "resultresult");

      const { kind, data } = result;

      // console.warn(data.data, "first page");
      // setLoadingState(false);

      if (kind === "ok") {
        // ShowNotification('VIEWED', "success");
        navigation.navigate("Main", {
          screen: "ViewDetails",
          params: { propertyData: datas },
        });
      } else {
        const err = data?.message;
        // ShowNotification(err, "error");
        console.warn(err);
        navigation.navigate("Main", {
          screen: "ViewDetails",
          params: { propertyData: datas },
        });
      }
    } catch ({ message }) {
      console.warn(message);
      // ShowNotification({ message });
      // dispatch(signInUserFailure())
      // dispatch(notify(`${message}`, 'danger'))
    }
  };

  // props

  return (
    <View style={[ROOT, { backgroundColor: colors.white2 }]}>
      <Header
        leftView={true}
        // rightView={true}
        onLeftPress={() => {
          navigation.openDrawer();
        }}
      />
      <ScrollView style={{ paddingVertical: 15, marginTop: 15 }}>
        <Animatable.View animation={"fadeIn"} style={BODY} duration={1000}>
          <Animatable.View animation={"slideInUp"}>
            <View style={{ paddingHorizontal: 15 }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Login");
                  // navigation.navigate('Main', {
                  //   screen: 'TabHome',
                  //   params: {
                  //     screen: 'Home',
                  //     params: {
                  //       screen: 'Message',
                  //     },
                  //   },
                  // });
                }}
              >
                <CustomText
                  textSize={25}
                  textWeight={"normal"}
                  textcolor={colors.dark}
                  displayText={"Discover your"}
                  textStyle={{ marginTop: 0 }}
                />
              </TouchableOpacity>

              <CustomText
                textSize={25}
                textWeight={"bold"}
                textcolor={colors.yellow}
                displayText={"new home"}
                textStyle={{ marginTop: 0 }}
              />
              <CustomText
                textSize={18}
                textWeight={"bold"}
                textcolor={colors.dark}
                displayText={"Find your dream home!"}
                textStyle={{ marginTop: 15, marginBottom: 26 }}
              />
            </View>
            <View style={{ width: "100%" }}>
              <View style={[FLEXROW, { paddingHorizontal: 20, width: "100%" }]}>
                <View style={{ width: "40%" }}>
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.yellow}
                    displayText={"Buy a home"}
                    textStyle={{ textAlign: "left" }}
                  />
                </View>
                <View style={{ width: "40%" }}>
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.dark}
                    displayText={"SALE"}
                    textStyle={{ textAlign: "right" }}
                  />
                </View>
              </View>
              <View style={{ width: "100%" }}>
                {loadingState ? (
                  <ActivityIndicator size="large" color={BgColor} />
                ) : (
                  // propertiesHolderSale
                  // propertiesHolderSaleShow
                  // start
                  <View style={{width: '100%'}}>
        <Carousel
          style={{aspectRatio: 1, flexGrow: 0, marginBottom: 2,}}
          data={propertiesHolderSaleShow}
          renderItem={({item, index})=>{
            // const { uri, title, content } = item;

            // console.log(item)
            return (
              <TouchableOpacity
                style={{ width: '100%', height: '100%' }}
                key={index}
                onPress={() => {
                  loginState === true
                    ? processPropertyView(item.propertiesItems)
                    : navigation.navigate("Main", {
                        screen: "ThankYou",
                      });
                }}
              >
                <ImageBackground
                  resizeMode="stretch"
                  source={images.bg2}
                  style={{
                    flex: 1,
                    //   width: 395,
                    padding: 10,
                    justifyContent: "center",
                  }}
                >
                  <View style={[CARDCONTAINER]}>
                    <Image
                      source={{
                        uri: item.url,
                      }}
                      style={CARDIMAGE}
                    />

                    <View style={FULLWIDTH}>
                      <View style={FULLWIDTH}>
                        <CustomText
                          textSize={16}
                          textWeight={"normal"}
                          textcolor={colors.black}
                          displayText={item.address}
                          textStyle={{ marginBottom: 5, marginTop: 20 }}
                          numberOfLines={1}
                        />
                        <CustomText
                          textSize={16}
                          textWeight={"normal"}
                          textcolor={colors.black}
                          displayText={item.description}
                          textStyle={{ marginBottom: 2 }}
                          numberOfLines={2}
                        />
                      </View>
                      <View style={[FULLWIDTH, FLEXROW, {justifyContent: 'flex-start', marginBottom: 6}]}>
                        <Naira
                          amount={item.price}
                          amountWeight={"bold"}
                          boldnaira
                        />
                        <Text style={{fontSize: 16, fontWeight: '400'}}>/Outright</Text>
                        <Text >{'\n'}{'\n'}{'\n'}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            )
          }}
          itemWidth={0.8 * windowWidth}
          inActiveOpacity={0.2}
          containerWidth={windowWidth}
          onScrollEnd={(item, index)=> setindicatorSales(index)}
          keyExtractor={(item: any, index: number) => index.toString()}
          // ref={carouselRef}
        />
        
        <SimplePaginationDot
          currentIndex={indicatorSales}
          length={propertiesHolderSaleShow?.length}
        />
        <Text style={{padding: 8, textAlign: 'center', fontSize: 13, opacity: 0.6, marginBottom: 20}}>Swipe right to view more properties</Text>
      </View>
                  // end
                )}
              </View>

              {/* <TouchableOpacity
              // onPress={() => {
              //   navigation.navigate("ViewDetails");
              // }}
              >
                <ImageBackground
                  resizeMode="stretch"
                  source={images.bg2}
                  style={{
                    flex: 1,
                    //   width: 395,
                    padding: 10,
                    justifyContent: "center",
                  }}
                >
                  <View style={CARDCONTAINER}>
                    <Image
                      source={{
                        uri: "https://res.cloudinary.com/vaccinenudge/image/upload/v1594301923/sample.jpg",
                      }}
                      style={CARDIMAGE}
                    />
                    <View style={FULLWIDTH}>
                      <CustomText
                        textSize={16}
                        textWeight={"normal"}
                        textcolor={colors.black}
                        displayText={"Life camp, Abuja"}
                        textStyle={{ marginBottom: 5, marginTop: 20 }}
                      />
                      <CustomText
                        textSize={16}
                        textWeight={"normal"}
                        textcolor={colors.black}
                        displayText={"New 5 bedroom terrace duplex"}
                        textStyle={{ marginBottom: 5 }}
                      />
                      <Naira
                        amount={88000000}
                        amountWeight={"bold"}
                        boldnaira
                      />
                    </View>
                  </View>
                  <TouchableOpacity
                    style={MOREBTN}
                    onPress={() => {
                      navigation.navigate("Main", { screen: "ViewDetails" });
                    }}
                  >
                    <Image source={images.more} />
                  </TouchableOpacity>
                </ImageBackground>
              </TouchableOpacity> */}
            </View>
            <View style={{ width: "100%" }}>
              <View style={[FLEXROW, { paddingHorizontal: 20, width: "100%" }]}>
                <View style={{ width: "40%" }}>
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.yellow}
                    displayText={"Rent a home"}
                    textStyle={{ textAlign: "left" }}
                  />
                </View>
                <View style={{ width: "40%" }}>
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.dark}
                    displayText={"RENT"}
                    textStyle={{ textAlign: "right" }}
                  />
                </View>
              </View>
              <View style={{ width: "100%" }}>
                {loadingState ? (
                  <ActivityIndicator size="large" color={BgColor} />
                ) : (
                  // propertiesHolderRent
                  // start
                  <View style={{width: '100%'}}>
        <Carousel
          style={{aspectRatio: 1, flexGrow: 0, marginBottom: 2,}}
          data={propertiesHolderRentShow}
          renderItem={({item, index})=>{
            // const { uri, title, content } = item;

            // console.log(item)
            return (
              <TouchableOpacity
                style={{ width: '100%', height: '100%' }}
                key={index}
                onPress={() => {
                  loginState === true
                    ? processPropertyView(item.propertiesItems)
                    : navigation.navigate("Main", {
                        screen: "ThankYou",
                      });
                }}
              >
                <ImageBackground
                  resizeMode="stretch"
                  source={images.bg2}
                  style={{
                    flex: 1,
                    //   width: 395,
                    padding: 10,
                    justifyContent: "center",
                  }}
                >
                  <View style={[CARDCONTAINER]}>
                    <Image
                      source={{
                        uri: item.url,
                      }}
                      style={CARDIMAGE}
                    />

                    <View style={FULLWIDTH}>
                      <View style={FULLWIDTH}>
                        <CustomText
                          textSize={16}
                          textWeight={"normal"}
                          textcolor={colors.black}
                          displayText={item.address}
                          textStyle={{ marginBottom: 5, marginTop: 20 }}
                          numberOfLines={1}
                        />
                        <CustomText
                          textSize={16}
                          textWeight={"normal"}
                          textcolor={colors.black}
                          displayText={item.description}
                          textStyle={{ marginBottom: 2 }}
                          numberOfLines={2}
                        />
                      </View>
                      <View style={[FULLWIDTH, FLEXROW, {justifyContent: 'flex-start', marginBottom: 6}]}>
                        <Naira
                          amount={item.price}
                          amountWeight={"bold"}
                          boldnaira
                        />
                        <Text style={{fontSize: 16, fontWeight: '400'}}>/Per year</Text>
                        <Text >{'\n'}{'\n'}{'\n'}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            )
          }}
          itemWidth={0.8 * windowWidth}
          inActiveOpacity={0.2}
          containerWidth={windowWidth}
          onScrollEnd={(item, index)=> setindicatorRent(index)}
          keyExtractor={(item: any, index: number) => index.toString()}
          // ref={carouselRef}
        />
        
        <SimplePaginationDot
          currentIndex={indicatorRent}
          length={propertiesHolderRentShow?.length}
        />
        <Text style={{padding: 8, textAlign: 'center', fontSize: 13, opacity: 0.6, marginBottom: 20}}>Swipe right to view more properties</Text>
      </View>
                  // end
                )}
              </View>

              {/* start */}
              <View style={[FLEXROW, { paddingHorizontal: 20, width: "100%" }]}>
                <View style={{ width: "40%" }}>
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.yellow}
                    displayText={"Book a shortlet"}
                    textStyle={{ textAlign: "left" }}
                  />
                </View>
                <View style={{ width: "40%" }}>
                  <CustomText
                    textSize={16}
                    textWeight={"normal"}
                    textcolor={colors.dark}
                    displayText={"SHORTLET"}
                    textStyle={{ textAlign: "right" }}
                  />
                </View>
              </View>
              <View style={{ width: "100%" }}>
                {loadingState ? (
                  <ActivityIndicator size="large" color={BgColor} />
                ) : (
                  // propertiesHolderShortLet
                  // start
                  <View style={{width: '100%'}}>
        <Carousel
          style={{aspectRatio: 1, flexGrow: 0, marginBottom: 2,}}
          data={propertiesHolderShortLetShow}
          renderItem={({item, index})=>{
            // const { uri, title, content } = item;

            // console.log(item)
            return (
              <TouchableOpacity
                style={{ width: '100%', height: '100%' }}
                key={index}
                onPress={() => {
                  loginState === true
                    ? processPropertyView(item.propertiesItems)
                    : navigation.navigate("Main", {
                        screen: "ThankYou",
                      });
                }}
              >
                <ImageBackground
                  resizeMode="stretch"
                  source={images.bg2}
                  style={{
                    flex: 1,
                    //   width: 395,
                    padding: 10,
                    justifyContent: "center",
                  }}
                >
                  <View style={[CARDCONTAINER]}>
                    <Image
                      source={{
                        uri: item.url,
                      }}
                      style={CARDIMAGE}
                    />

                    <View style={FULLWIDTH}>
                      <View style={FULLWIDTH}>
                        <CustomText
                          textSize={16}
                          textWeight={"normal"}
                          textcolor={colors.black}
                          displayText={item.address}
                          textStyle={{ marginBottom: 5, marginTop: 20 }}
                          numberOfLines={1}
                        />
                        <CustomText
                          textSize={16}
                          textWeight={"normal"}
                          textcolor={colors.black}
                          displayText={item.description}
                          textStyle={{ marginBottom: 2 }}
                          numberOfLines={2}
                        />
                      </View>
                      <View style={[FULLWIDTH, FLEXROW, {justifyContent: 'flex-start', marginBottom: 6}]}>
                        <Naira
                          amount={item.price}
                          amountWeight={"bold"}
                          boldnaira
                        />
                        <Text style={{fontSize: 16, fontWeight: '400'}}>/Per night</Text>
                        <Text >{'\n'}{'\n'}{'\n'}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            )
          }}
          itemWidth={0.8 * windowWidth}
          inActiveOpacity={0.2}
          containerWidth={windowWidth}
          onScrollEnd={(item, index)=> setindicatorShortlet(index)}
          keyExtractor={(item: any, index: number) => index.toString()}
          // ref={carouselRef}
        />
        
        <SimplePaginationDot
          currentIndex={indicatorShortlet}
          length={propertiesHolderShortLetShow?.length}
        />
        <Text style={{padding: 8, textAlign: 'center', fontSize: 13, opacity: 0.6, marginBottom: 20}}>Swipe right to view more properties</Text>
      </View>
                  // end
                )}
              </View>
              {/* stop */}

              {/* <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Main", { screen: "BuyersProfile" });
                }}
              >
                <ImageBackground
                  resizeMode="stretch"
                  source={images.bg2}
                  style={{
                    flex: 1,
                    //   width: 395,
                    padding: 10,
                    justifyContent: "center",

                    //   backgroundColor: "#F2F2F2",
                    //   borderRadius: 10,
                    //   elevation: 1,
                    //   marginVertical: 15,
                  }}
                >
                  <View style={CARDCONTAINER}>
                    <Image
                      source={{
                        uri: "https://res.cloudinary.com/vaccinenudge/image/upload/v1594301923/sample.jpg",
                      }}
                      style={CARDIMAGE}
                    />
                    <View style={FULLWIDTH}>
                      <CustomText
                        textSize={16}
                        textWeight={"normal"}
                        textcolor={colors.black}
                        displayText={"Life camp, Abuja"}
                        textStyle={{ marginBottom: 5, marginTop: 20 }}
                      />
                      <CustomText
                        textSize={16}
                        textWeight={"normal"}
                        textcolor={colors.black}
                        displayText={"New 5 bedroom terrace duplex"}
                        textStyle={{ marginBottom: 5 }}
                      />
                      <Naira
                        amount={88000000}
                        amountWeight={"bold"}
                        boldnaira
                      />
                    </View>
                  </View>
                  <TouchableOpacity style={MOREBTN}>
                    <Image source={images.more} />
                  </TouchableOpacity>
                </ImageBackground>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Main", { screen: "SellerProfile" });
                }}
              >
                <ImageBackground
                  resizeMode="stretch"
                  source={images.bg2}
                  style={{
                    flex: 1,
                    //   width: 395,
                    padding: 10,
                    justifyContent: "center",

                    //   backgroundColor: "#F2F2F2",
                    //   borderRadius: 10,
                    //   elevation: 1,
                    //   marginVertical: 15,
                  }}
                >
                  <View style={CARDCONTAINER}>
                    <Image
                      source={{
                        uri: "https://res.cloudinary.com/vaccinenudge/image/upload/v1594301923/sample.jpg",
                      }}
                      style={CARDIMAGE}
                    />
                    <View style={FULLWIDTH}>
                      <CustomText
                        textSize={16}
                        textWeight={"normal"}
                        textcolor={colors.black}
                        displayText={"Life camp, Abuja"}
                        textStyle={{ marginBottom: 5, marginTop: 20 }}
                      />
                      <CustomText
                        textSize={16}
                        textWeight={"normal"}
                        textcolor={colors.black}
                        displayText={"New 5 bedroom terrace duplex"}
                        textStyle={{ marginBottom: 5 }}
                      />
                      <Naira
                        amount={88000000}
                        amountWeight={"bold"}
                        boldnaira
                      />
                    </View>
                  </View>
                  <TouchableOpacity style={MOREBTN}>
                    <Image source={images.more} />
                  </TouchableOpacity>
                </ImageBackground>
              </TouchableOpacity> */}
              <ImageBackground
                resizeMode="stretch"
                source={images.bg2}
                style={{
                  flex: 1,
                  //   width: 395,
                  padding: 30,
                  justifyContent: "center",
                  marginBottom: 15,

                  //   backgroundColor: "#F2F2F2",
                  //   borderRadius: 10,
                  //   elevation: 1,
                  //   marginVertical: 15,
                }}
              >
                <View style={CARDCONTAINER}>
                  <View style={FULLWIDTH}>
                    <CustomText
                      textSize={18}
                      textWeight={"500"}
                      textcolor={colors.black}
                      displayText={"Sell/Lease"}
                      textStyle={{ textAlign: "center", marginBottom: 5 }}
                    />
                    <CustomText
                      textSize={15}
                      textWeight={"500"}
                      textcolor={colors.dark}
                      displayText={
                        "As a property owner, you can list your property in 5 easy steps."
                      }
                      textStyle={{ textAlign: "center" }}
                    />
                    <CustomText
                      textSize={15}
                      textWeight={"500"}
                      textcolor={colors.dark}
                      displayText={
                        "Your property will go live after   verification."
                      }
                      textStyle={{ textAlign: "center" }}
                    />
                  </View>
                  <View
                    style={[
                      FLEXROW,
                      {
                        justifyContent: "flex-end",
                        width: "100%",
                        marginTop: 10,
                      },
                    ]}
                  >
                    <TouchableOpacity
                      style={GETSTARTED}
                      onPress={() => {
                        loginState === true
                          ? navigation.navigate("Main", {
                              screen: "StepRegistration",
                            })
                          : navigation.navigate("Main", { screen: "Login" });
                      }}
                    >
                      <CustomText
                        textSize={12}
                        textWeight={"bold"}
                        textcolor={colors.dark}
                        displayText={"Get Started"}
                        textStyle={{ textAlign: "center" }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </ImageBackground>
            </View>
          </Animatable.View>
        </Animatable.View>
      </ScrollView>
    </View>
  );
};

export default Onboarding;
