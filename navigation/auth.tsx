import React from "react";
import {
  createNativeStackNavigator,
  NativeStackNavigationProp,
} from "@react-navigation/native-stack";
import Onboarding from "../screens/explore";
import Login from "../screens/login/login";
import SignUp from "../screens/signUp/SignUp";
import ForgetPassword from "../screens/forgetPassword/ForgetPassword";
import ResetPassword from "../screens/resetPassword/ResetPassword";
import ViewDetails from "../screens/explore/viewDetails";
import BuyersProfile from "../screens/buyerProfile";
import SellerProfile from "../screens/sellerProfile";
import StepRegistration from "../screens/stepRegistration";
import SendMsg from "../screens/sendMsg";
import ThankYou from "../screens/thankYou";
import Message from "../screens/message";
import { RouteProp } from "@react-navigation/native";
// import MainNavigator from "./main";

export type Routes = {
  Onboarding: undefined;
  Main: undefined;
  ViewDetails: undefined;
  BuyersProfile: undefined;
  SellerProfile: undefined;
  Login: undefined;
  SignUp: undefined;
  ForgetPassword: undefined;
  ResetPassword: undefined;
  StepRegistration: undefined;
  SendMsg: undefined;
  ThankYou: undefined;
  Message: undefined;
};

export type AuthNavigationProps<T extends keyof Routes> = {
  navigation: NativeStackNavigationProp<Routes, T>;
  route: RouteProp<Routes, T>;
};

const Stack = createNativeStackNavigator<Routes>();

const AuthNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Onboarding"
    >
      <Stack.Screen
        name="Onboarding"
        component={Onboarding}
        options={{ title: "Onboarding" }}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{ title: "SignUp" }}
      />
      <Stack.Screen
        name="ForgetPassword"
        component={ForgetPassword}
        options={{ title: "ForgetPassword" }}
      />
      <Stack.Screen
        name="ResetPassword"
        component={ResetPassword}
        options={{ title: "ResetPassword" }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ title: "Login" }}
      />
      {/* <Stack.Screen
        name="ViewDetails"
        component={ViewDetails}
        options={{ title: "Blog" }}
      />
      <Stack.Screen
        name="BuyersProfile"
        component={BuyersProfile}
        options={{ title: "Blog" }}
      /> */}
      {/* <Stack.Screen
        name="SellerProfile"
        component={SellerProfile}
        options={{ title: "Blog" }}
      /> */}
      {/* <Stack.Screen
        name="StepRegistration"
        component={StepRegistration}
        options={{ title: "Blog" }}
      /> */}
      {/* <Stack.Screen
        name="SendMsg"
        component={SendMsg}
        options={{ title: "Send Msg" }}
      /> */}
      {/* <Stack.Screen
        name="ThankYou"
        component={ThankYou}
        options={{ title: "Thank You" }}
      /> */}
      {/* <Stack.Screen
        name="Message"
        component={Message}
        options={{ title: "Message" }}
      /> */}

      {/* <Stack.Screen
                name="Main"
                component={MainNavigator}
            /> */}
    </Stack.Navigator>
  );
};

export default AuthNavigator;
