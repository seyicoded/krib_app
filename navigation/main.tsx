import React from "react";
import {
  createNativeStackNavigator,
  NativeStackNavigationProp,
} from "@react-navigation/native-stack";
import { RouteProp } from "@react-navigation/native";
import { Icon } from "./../components/icon";

import Explore from "../screens/explore";
import ViewedProperties from "../screens/propertyOverView/viewedProperties";
import ListedProperties from "../screens/propertyOverView/listedProperties";
import Login from "../screens/login/login";
import SignUp from "../screens/signUp/SignUp";
import ForgetPassword from "../screens/forgetPassword/ForgetPassword";
import UserVerification from "../screens/userVerification/UserVerification";
import ResetPassword from "../screens/resetPassword/ResetPassword";
import ViewDetails from "../screens/explore/viewDetails";
import ShortLetInfo from "../screens/propertyOverView/ShortLetInfo.js";
import BuyersProfile from "../screens/buyerProfile";
import SellerProfile from "../screens/sellerProfile";
import MySettings from "../screens/settings";
import StepRegistration from "../screens/stepRegistration";
import ProfileDetail from "../screens/profileDetail";
import Viewbank from "../screens/bank";
import Addbank from "../screens/bank/addBank";
import SecurePin from "../screens/bank/securePin";
import SendMsg from "../screens/sendMsg";
import ThankYou from "../screens/thankYou";
import Message from "../screens/message";
import NotificationScreen from "../screens/notification";
import MessageView from "../screens/message/messageView";
import AddPropertyResponse from "../screens/addPropertyResponsescreen";

import { Button, View, Image } from "react-native";
import CustomDrawerMenu from "../components/customer-drawer-menu/custom-drawer-menu";

import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { colors, images } from "../theme";
import { ComingSoonScreen } from "../screens/comingSoon";
import userProfile from "../screens/userProfile";
// import message from "../screens/message";
export type Routes = {
  Explore: undefined;
  Main: undefined;
  ViewDetails: undefined;
  ShortLetInfo: undefined;
  BuyersProfile: undefined;
  SellerProfile: undefined;
  Login: undefined;
  SignUp: undefined;
  ForgetPassword: undefined;
  UserVerification: undefined;
  ResetPassword: undefined;
  StepRegistration: undefined;
  SendMsg: undefined;
  ThankYou: undefined;
  Message: undefined;
  TabHome: undefined;
  MySettings: undefined;
  ProfileDetail: undefined;
  Viewbank: undefined;
  Addbank: undefined;
  SecurePin: undefined;
  ViewedProperties: undefined;
  ListedProperties: undefined;
  AddPropertyResponse: undefined;
  MessageView: undefined;
  NotificationScreen: undefined;
};
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarStyle: {
          elevation: 0,
          borderTop: 1,
          borderTopColor: "#FFF",
          paddingBottom: 10,
          height: 60,
        },
        tabBarIcon: ({ focused, color, size }) => {
          // console.warn(route.name);
          let iconName;
          if (route.name === "Explore") {
            iconName = focused ? images.exp_A : images.exp;
          } else if (route.name === "Message") {
            iconName = focused ? images.msg_A : images.msgs;
          } else if (route.name === "Notifications") {
            iconName = focused ? images.bell_A : images.bell;
          } else if (route.name === "Profile") {
            iconName = focused ? images.prof_A : images.prof;
          }
          // You can return any component that you like here!
          return <Image source={iconName} />;
        },
        tabBarActiveTintColor: colors.alerzoBlue,
        tabBarInactiveTintColor: "gray",
        headerShown: false,
      })}
    >
      <Tab.Screen name="Explore" component={Explore} />
      <Tab.Screen name="Message" component={Message} />
      <Tab.Screen name="Notifications" component={NotificationScreen} />
      {/* <Tab.Screen name="Profile" component={SellerProfile} /> */}
      <Tab.Screen name="Profile" component={userProfile} />
    </Tab.Navigator>
  );
};
const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
        drawerStyle: {
          // backgroundColor: '#c6cbef',
          width: "80%",
        },
      }}
      drawerContent={(props) => <CustomDrawerMenu {...props} />}
    >
      <Drawer.Screen name="Home" component={TabNavigator} />
    </Drawer.Navigator>
  );
};

export type AuthNavigationProps<T extends keyof Routes> = {
  navigation: NativeStackNavigationProp<Routes, T>;
  route: RouteProp<Routes, T>;
};

const Stack = createNativeStackNavigator<Routes>();

const MainNavigator = () => (
  <Stack.Navigator
    screenOptions={{
      headerShown: false,
    }}
    // initialRouteName="Login"
    initialRouteName="TabHome"
  >
    <Stack.Screen name="TabHome" component={DrawerNavigator} />

    <Stack.Screen
      name="ViewDetails"
      component={ViewDetails}
      options={{ title: "Blog" }}
    />
    <Stack.Screen
      name="ShortLetInfo"
      component={ShortLetInfo}
      options={{ title: "Shortlet" }}
    />
    <Stack.Screen
      name="BuyersProfile"
      component={BuyersProfile}
      options={{ title: "Buyers Profile" }}
    />
    {/* <Stack.Screen
      name="SellerProfile"
      component={SellerProfile}
      options={{ title: "Seller Profile" }}
    /> */}
    <Stack.Screen
      name="StepRegistration"
      component={StepRegistration}
      options={{ title: "Step Registration" }}
    />
    <Stack.Screen
      name="SendMsg"
      component={SendMsg}
      options={{ title: "Send Msg" }}
    />
    <Stack.Screen
      name="ThankYou"
      component={ThankYou}
      options={{ title: "Thank You" }}
    />
    <Stack.Screen
      name="ResetPassword"
      component={ResetPassword}
      options={{ title: "Reset Password" }}
    />
    <Stack.Screen
      name="UserVerification"
      component={UserVerification}
      options={{ title: "User Verification" }}
    />
    <Stack.Screen
      name="ForgetPassword"
      component={ForgetPassword}
      options={{ title: "Forget Password" }}
    />
    <Stack.Screen
      name="SignUp"
      component={SignUp}
      options={{ title: "Sign Up" }}
    />
    <Stack.Screen
      name="MySettings"
      component={MySettings}
      options={{ title: "My Settings" }}
    />
    <Stack.Screen
      name="Viewbank"
      component={Viewbank}
      options={{ title: "View bank" }}
    />
    <Stack.Screen
      name="Addbank"
      component={Addbank}
      options={{ title: "Add Bank" }}
    />
    <Stack.Screen name="Login" component={Login} options={{ title: "Login" }} />
    {/* <Stack.Screen
      name="Message"
      component={Message}
      options={{ title: "Message" }}
    /> */}
    <Stack.Screen
      name="ProfileDetail"
      component={ProfileDetail}
      options={{ title: "Profile Detail" }}
    />
    <Stack.Screen
      name="SecurePin"
      component={SecurePin}
      options={{ title: "Secure Pin" }}
    />
    <Stack.Screen
      name="ViewedProperties"
      component={ViewedProperties}
      options={{ title: "Viewed Properties" }}
    />
    <Stack.Screen
      name="ListedProperties"
      component={ListedProperties}
      options={{ title: "Listed Properties" }}
    />
    <Stack.Screen
      name="AddPropertyResponse"
      component={AddPropertyResponse}
      options={{ title: "Add Property Response" }}
    />
    <Stack.Screen
      name="MessageView"
      component={MessageView}
      options={{ title: "Message View" }}
    />
  </Stack.Navigator>
);

export default MainNavigator;
