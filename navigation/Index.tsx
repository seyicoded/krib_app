import React from "react";
import {
  createNativeStackNavigator,
  NativeStackNavigationProp,
} from "@react-navigation/native-stack";
import Onboarding from "../screens/explore";
import Login from "../screens/login/login";
import SignUp from "../screens/signUp/SignUp";
import ForgetPassword from "../screens/forgetPassword/ForgetPassword";
import ResetPassword from "../screens/resetPassword/ResetPassword";
import ViewDetails from "../screens/explore/viewDetails";
import BuyersProfile from "../screens/buyerProfile";
import SellerProfile from "../screens/sellerProfile";
import StepRegistration from "../screens/stepRegistration";
import SendMsg from "../screens/sendMsg";
import ThankYou from "../screens/thankYou";
import Message from "../screens/message";
import { RouteProp } from "@react-navigation/native";
import MainNavigator from "./main";
import AuthNavigator from "./auth";
// import MainNavigator from "./main";
import FAQScreen from "../screens/faq/faq";

export type Routes = {
  Onboarding: undefined;
  Main: undefined;
  ViewDetails: undefined;
  BuyersProfile: undefined;
  SellerProfile: undefined;
  Login: undefined;
  SignUp: undefined;
  ForgetPassword: undefined;
  ResetPassword: undefined;
  StepRegistration: undefined;
  SendMsg: undefined;
  ThankYou: undefined;
  Message: undefined;
  FAQScreen: undefined;
};

export type AuthNavigationProps<T extends keyof Routes> = {
  navigation: NativeStackNavigationProp<Routes, T>;
  route: RouteProp<Routes, T>;
};

const Stack = createNativeStackNavigator<Routes>();

const RootNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Main"
    >
      <Stack.Screen
        name="Onboarding"
        component={AuthNavigator}
        // options={{ title: 'Blog' }}
      />

      <Stack.Screen name="Main" component={MainNavigator} />
      <Stack.Screen name="FAQScreen" component={FAQScreen} />
    </Stack.Navigator>
  );
};

export default RootNavigator;
