// react
import React, { useEffect, useState } from "react";

// third-parties
import { createMaterialBottomTabNavigator, MaterialBottomTabScreenProps } from '@react-navigation/material-bottom-tabs';
import { RouteProp } from "@react-navigation/native";
import { FontAwesome, FontAwesome5, Ionicons } from 'react-native-vector-icons';
import { useFocusEffect } from "@react-navigation/native";


// navigation
import HomeNavigator from "./home";

// styles
import { colors, fonts, images } from "../theme";
import { Icon } from "../components/icon";
import { View, Image, Text, Appearance, StatusBar } from "react-native";
import { ComingSoonScreen } from "../screens/comingSoon";
import useReduxStore from "../utils/hooks/useRedux";
import { Layout } from "../constants";


export type Routes = {
    Home: undefined;
    Account: undefined;
    Save: undefined;
    More: undefined;
};

export type AuthNavigationProps<T extends keyof Routes> = {
    navigation: MaterialBottomTabScreenProps<Routes, T>;
    route: RouteProp<Routes, T>;
};

const Stack = createMaterialBottomTabNavigator<Routes>();

const LandingNavigator = () => {
    const [dispatchWallet, selectStoreWallet] = useReduxStore("wallet");
    const isLoading = selectStoreWallet("loading")

    let [isDark, setIsDark] = useState(false);

    const [dispatchAuthAction, selectAuthStore] = useReduxStore("auth");
    const mode = selectAuthStore('colorScheme');

    React.useEffect(() => {
        console.log(mode, "<=== mode")
        setIsDark(mode === "dark")
    }, [mode])

    return (
        <Stack.Navigator
            screenOptions={{
                // @ts-ignore
                // headerShown: false,
            }}
            barStyle={{
                backgroundColor: isDark ? colors.alerzoDark : colors.white,
                height: isLoading ? 0 : Layout.window.height / 9.5
            }}
            labeled={false}

        >
            <Stack.Screen
                name="Home"
                component={HomeNavigator}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <Image
                                source={focused ? images.homeTabIcon : images.homeTabIconUnFocused}
                            />
                            <Text
                                style={{
                                    fontSize: 10,
                                    width: '100%',
                                    top: 5,
                                    textAlign: 'center',
                                    color: focused ? colors.alerzoBlue : colors.greyTwo,
                                    fontWeight: 'bold',
                                    fontFamily: fonts.GilmerMedium
                                }}
                            >
                                Home
                            </Text>

                            {
                                focused && (
                                    <Image
                                        source={images.dot}
                                        style={{
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                    />
                                )
                            }
                        </View>
                    ),
                }}
            />

            {/* <Stack.Screen
                name="Account"
                component={ComingSoonScreen}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <Image
                                source={focused ? images.accountFocused : images.accountUnFocused}
                            />
                            <Text
                                style={{
                                    fontSize: 10,
                                    width: '100%',
                                    top: 5,
                                    textAlign: 'center',
                                    color: focused ? colors.alerzoBlue : colors.greyTwo,
                                    fontWeight: 'bold',
                                    fontFamily: fonts.GilmerMedium
                                }}
                            >
                                Account
                            </Text>

                            {
                                focused && (
                                    <Image
                                        source={images.dot}
                                        style={{
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                    />
                                )
                            }
                        </View>
                    ),
                }}
            />

            <Stack.Screen
                name="Save"
                component={ComingSoonScreen}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <Image
                                source={focused ? images.saveIconFocused : images.saveIcon}
                            />
                            <Text
                                style={{
                                    fontSize: 10,
                                    width: '100%',
                                    top: 5,
                                    textAlign: 'center',
                                    color: focused ? colors.alerzoBlue : colors.greyTwo,
                                    fontWeight: 'bold',
                                    fontFamily: fonts.GilmerMedium
                                }}
                            >
                                Save
                            </Text>

                            {
                                focused && (
                                    <Image
                                        source={images.dot}
                                        style={{
                                            top: 10,
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                    />
                                )
                            }
                        </View>
                    ),
                }}
            />

            <Stack.Screen
                name="More"
                component={ComingSoonScreen}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <Image
                                source={focused ? images.moreUnFocused : images.moreUnFocused}
                            />
                            <Text
                                style={{
                                    fontSize: 10,
                                    width: '100%',
                                    top: 5,
                                    textAlign: 'center',
                                    color: focused ? colors.alerzoBlue : colors.greyTwo,
                                    fontWeight: 'bold',
                                    fontFamily: fonts.GilmerMedium
                                }}
                            >
                                More
                            </Text>

                            {
                                focused && (
                                    <Image
                                        source={images.dot}
                                        style={{
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                    />
                                )
                            }
                        </View>
                    ),
                }}
            /> */}

        </Stack.Navigator>
    );
}

export default LandingNavigator;
