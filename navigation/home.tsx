import React, { useEffect, useState } from "react";
import { View, Appearance, StatusBar, TouchableOpacity, Image, Text, Platform } from "react-native";
import { createNativeStackNavigator, NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RouteProp } from "@react-navigation/native";
// import Landing from "../screens/landing";
// import { BillPaymentsScreen } from "../screens/billPayment";
// import { CableScreen } from "../screens/cable/Cable";
// import { DataPurchaseScreen } from "../screens/data/Data";
// import { EletricityScreen } from "../screens/eletricity/Eletricity";
// import { InternetSubscriptionScreen } from "../screens/internet";
// import { EletricitySummaryScreen } from "../screens/electricitySummary";
// import { PinSetupScreen } from "../screens/PinSetup";
// import { SuccessPageScreen } from "../screens/successPage";
// import { CableSummaryScreen } from "../screens/cableSummary";
import { colors, fonts, images } from "../theme";
import { translate } from "../i18n";
import { Layout } from "../constants";
import moment from "moment";
// import Header from "../components/header/header";
import useReduxStore from "../utils/hooks/useRedux";


import Login from "../screens/login/login";
import SignUp from "../screens/signUp/SignUp";
import ForgetPassword from "../screens/forgetPassword/ForgetPassword";
import ResetPassword from "../screens/resetPassword/ResetPassword";
import ViewDetails from "../screens/explore/viewDetails";
import ShortLetInfo from "../screens/propertyOverView/ShortLetInfo";
import BuyersProfile from "../screens/buyerProfile";
import SellerProfile from "../screens/sellerProfile";
import StepRegistration from "../screens/stepRegistration";
import SendMsg from "../screens/sendMsg";
import ThankYou from "../screens/thankYou";
import Message from "../screens/message";






export type Routes = {
    // Home: undefined;
    // billPayments: undefined;
    // internetSubscription: undefined;
    // dataPurchase: undefined;
    // cable: undefined;
    // eletricity: undefined;
    // eletricitySummary: undefined;
    // pinSetupScreen: undefined;
    // successPageScreen: undefined;
    // eletricitySummaryScreen: undefined;
    // cableSummaryScreen: undefined;
    // betting: undefined;
    // electricitySummary: undefined;
    // bettingSummaryScreen: undefined;
    // airtime: undefined;
    // airtimeSummary: undefined;
    // bvn: undefined;
    // setupPin: undefined;
    /////////
    // amount: undefined;
    // addMoney: undefined;
    // support: undefined;
    // security: undefined;
    // chat: undefined;
    // customerCare: undefined;
    // passcode: undefined;
    // comingSoon: undefined;
    // transfer: undefined;
    // transferDestination: undefined;
    // transferSummary: undefined;
    Onboarding: undefined;
    Main: undefined;
    ViewDetails: undefined;
    BuyersProfile: undefined;
    SellerProfile: undefined;
    Login: undefined;
    SignUp: undefined;
    ForgetPassword: undefined;
    ResetPassword: undefined;
    StepRegistration: undefined;
    SendMsg: undefined;
    ThankYou: undefined;
    Message: undefined;
    ShortLetInfo: undefined;

};

export type AuthNavigationProps<T extends keyof Routes> = {
    navigation: NativeStackNavigationProp<Routes, T>;
    route: RouteProp<Routes, T>;
};

const Stack = createNativeStackNavigator<Routes>();

const HomeNavigator = () => {

    const generateGreetings = () => {
        var currentHour = parseInt(moment().format("HH"));

        console.log(currentHour, "currentHour")

        if (currentHour >= 3 && currentHour < 12) {
            return `${translate("homepage.goodMorning")}`;
        } else if (currentHour >= 12 && currentHour < 15) {
            return `${translate("homepage.goodAfternoon")}`;
        } else if (currentHour >= 15 && currentHour < 20) {
            return `${translate("homepage.goodEvening")}`;
        } else if (currentHour >= 20) {
            return `${translate("homepage.goodNight")}`;
        } else if (currentHour < 12) {
            return `${translate("homepage.goodMorning")}`;
        }
    };

    const [dispatchWallet, selectStoreWallet] = useReduxStore("wallet");
    const [dispatchAuth, selectStoreAuth] = useReduxStore("auth");
    const isLoading = selectStoreWallet("loading")
    const user = selectStoreAuth("user")
    console.log(user, "selectStoreWallet")

    const { firstName } = user

    let [isDark, setIsDark] = useState(false);

    const [dispatchAuthAction, selectAuthStore] = useReduxStore("auth");
    const mode = selectAuthStore('colorScheme');
    const loading = selectStoreWallet("loading");
    console.log(loading, "selectWalletStore")

    React.useEffect(() => {
        console.log(mode, "<=== mode")
        setIsDark(mode === "dark")
    }, [mode])

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: !loading
            }}
        >
            <Stack.Screen
                name="Home"
                component={Landing}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                navigation={navigation}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.openDrawer()}
                                    >
                                        <View
                                            style={{
                                                flexDirection: "row",
                                            }}
                                        >
                                            <Image
                                                source={images.hamburger}
                                                resizeMethod="auto"
                                                resizeMode="contain"
                                            />

                                            <Text
                                                style={{
                                                    fontSize: 18,
                                                    fontFamily: fonts.GilmerMedium,
                                                    fontWeight: "400",
                                                    bottom: 5,
                                                    left: 10,
                                                    color: mode === "dark" ? colors.white : colors.alerzoDark
                                                }}
                                            >
                                                {translate("homepage.greetings", {
                                                    name: `${firstName} ${generateGreetings()}`,
                                                })}
                                            </Text>
                                        </View>


                                    </TouchableOpacity>
                                }

                                rightView={
                                    <View>
                                        <View
                                            style={{
                                                flexDirection: "row",
                                                bottom: 5,
                                            }}
                                        >
                                            <TouchableOpacity>
                                                <Image
                                                    source={images.scan}
                                                    style={{
                                                        right: 10,
                                                    }}
                                                />
                                            </TouchableOpacity>

                                            <TouchableOpacity>
                                                <Image
                                                    source={images.bell}
                                                    style={{
                                                        // right: 10,
                                                    }}
                                                />
                                            </TouchableOpacity>
                                        </View>


                                    </View>
                                }
                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="billPayments"
                component={BillPaymentsScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"billPayment.header"}
                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="internetSubscription"
                component={InternetSubscriptionScreen}
            />

            <Stack.Screen
                name="dataPurchase"
                component={DataPurchaseScreen}
            />

            <Stack.Screen
                name="cable"
                component={CableScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"cable.header"}

                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="cableSummaryScreen"
                component={CableSummaryScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"electricitySummary.header"}

                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="eletricity"
                component={EletricityScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"eletricity.header"}

                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="eletricitySummary"
                component={EletricitySummaryScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"electricitySummary.header"}

                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="pinSetupScreen"
                component={PinSetupScreen}
            />

            <Stack.Screen
                name="successPageScreen"
                component={SuccessPageScreen}
                options={{
                    headerShown: false
                }}
            />

            <Stack.Screen
                name="betting"
                component={BettingScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"billPayment.header"}


                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="bettingSummaryScreen"
                component={BettingSummaryScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"electricitySummary.header"}


                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="airtime"
                component={AirtimeScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"billPayment.header"}


                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="airtimeSummary"
                component={AirtimeSummaryScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"billPayment.header"}


                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="bvn"
                component={BvnScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"bvn.header"}


                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="setupPin"
                component={SetupPinScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"setupPin.header"}


                            />

                        );
                    }
                }}
            />
            {/* ////////////////////// */}
            <Stack.Screen
                name="amount"
                component={AmountScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"fundAccount.header"}



                            />

                        );
                    }
                }}
            />
            <Stack.Screen
                name="addMoney"
                component={AddMoneyScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"fundAccount.header"}



                            />

                        );
                    }
                }}
            />
            <Stack.Screen
                name="support"
                component={SupportScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"profile.profile"}



                            />

                        );
                    }
                }}
            />
            <Stack.Screen
                name="customerCare"
                component={CustomerCareScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"customerCare.header"}



                            />

                        );
                    }
                }}
            />
            <Stack.Screen
                name="chat"
                component={chatScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"customerCare.chatWIthUsName"}



                            />

                        );
                    }
                }}
            />
            <Stack.Screen
                name="passcode"
                component={SetupPasscodeScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"customerCare.chatWIthUsName"}



                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="comingSoon"
                component={ComingSoonScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"comingSoon.header"}



                            />

                        );
                    }
                }}
            />
            <Stack.Screen
                name="transfer"
                component={TransferScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"sendMoney.header"}



                            />

                        );
                    }
                }}
            />
            <Stack.Screen
                name="transferDestination"
                component={TransferDestinationScreen}
                // component={TransferScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"sendMoney.header"}



                            />

                        );
                    }
                }}
            />

            <Stack.Screen
                name="transferSummary"
                component={TransferSummaryScreen}
                // component={TransferScreen}
                options={{
                    header: ({ navigation, route, options, back }) => {

                        return (

                            <Header
                                leftIcon="arrowBackWhite"
                                navigation={navigation}
                                onLeftPress={() => navigation.goBack()}
                                leftView={
                                    <TouchableOpacity
                                        onPress={() => navigation.goBack()}
                                        style={{
                                            height: 45,
                                            top: 5
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: mode === "dark" ? colors.white : colors.black
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                rightView={
                                    <TouchableOpacity
                                        style={{
                                            height: 45
                                        }}
                                    >
                                        <Image
                                            source={images.angleLeft}
                                            style={{
                                                tintColor: colors.transparent,
                                            }}
                                        />
                                    </TouchableOpacity>
                                }
                                titleTx={"transferSummary.header"}



                            />

                        );
                    }
                }}
            />


        </Stack.Navigator>
    )
}

export default HomeNavigator;
