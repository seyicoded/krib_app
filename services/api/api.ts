// a library to wrap and simplify api calls
import apisauce, { DEFAULT_HEADERS } from "apisauce";
// import {APP_BASE_URL, MONIFY_APIKEY} from '@env';
import * as Types from "./api.types";
import { getGeneralApiProblem } from "./api-problem";
import { GetPersistData } from "../../utils/UtilityFunctions";
// console.log(APP_BASE_URL, 'APP_BASE_URLAPP_BASE_URL');
// console.log(MONIFY_APIKEY, 'MONIFY_APIKEY');

const api = apisauce.create({
  // base URL is read from the "constructor"
  // baseURL: "https://kribserver.herokuapp.com",
  // baseURL: APP_BASE_URL,
  // baseURL: "https://kribserverprod.herokuapp.com",
  baseURL: "https://kribprotest.herokuapp.com",
  // here are some default headers
  headers: {
    "Cache-Control": "no-cache",
    Accept: "application/json",
    ContentType: "application/json",
    // Authorization: `Bearer ${token}`
  },
  // 10 second timeout...
  timeout: 100000,
});

/**
 * Process the api response
 */
const processResponse = async (response: any): Promise<any> => {
  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response);
    if (problem) {
      // console.tron.error({ ...response, message: response.config.url })
      return problem;
    }
  }

  // we're good
  // replace with `data` once api change is made.
  return { kind: "ok", data: response.data };
};

const signInUser = async ({
  code,
  code_challenge,
}): Promise<Types.getResponse> => {
  console.log(code, code_challenge);
  await api.setHeaders({
    code,
    code_challenge,
  });
  const response = await api.get("/auth/me");
  return processResponse(response);
};

const userLogin = async (values: any): Promise<Types.getResponse> => {
  // await api.setHeaders({'Authorization': `Bearer ${token}`})
  const response = await api.post("api/auth/login", {
    ...values,
  });
  return processResponse(response);
};
const userSignUp = async (values: any): Promise<Types.getResponse> => {
  console.warn(values, "lets dod this");
  // await api.setHeaders({'Authorization': `Bearer ${token}`})
  const response = await api.post("api/auth/signup", {
    ...values,
  });
  return processResponse(response);
};
const accountVerification = async (values: any): Promise<Types.getResponse> => {
  // await api.setHeaders({'Authorization': `Bearer ${token}`})
  const response = await api.post("api/auth/Verifyauthcode", {
    ...values,
  });
  return processResponse(response);
};
const Forgetpassword = async (values: any): Promise<Types.getResponse> => {
  // await api.setHeaders({'Authorization': `Bearer ${token}`})
  const response = await api.post("/api/auth/initForgetpassword", {
    ...values,
  });
  return processResponse(response);
};
const ResetPassword = async (values: any): Promise<Types.getResponse> => {
  // await api.setHeaders({'Authorization': `Bearer ${token}`})
  const response = await api.post("/api/auth/ResetPassword", {
    ...values,
  });
  return processResponse(response);
};
const getProperty = async (): Promise<Types.getResponse> => {
  const response = await api.get("/api/property");
  return processResponse(response);
};
const getBankList = async (): Promise<Types.getResponse> => {
  const response = await api.get("/api/utils/getallbanks");
  return processResponse(response);
};
const getPropertyType = async (): Promise<Types.getResponse> => {
  const response = await api.get("/api/utils/property/types");
  return processResponse(response);
};
const getBuildingAmenities = async (): Promise<Types.getResponse> => {
  const response = await api.get("/api/utils/BuildingAmenities");
  return processResponse(response);
};
const getAppliancesAndFinishings = async (): Promise<Types.getResponse> => {
  const response = await api.get("/api/utils/AppliancesAndFinishings");
  return processResponse(response);
};
// const PurchaseProperty = async (): Promise<
//   Types.getResponse
//   > => {
//   const response = await api.get("/api/property/purchaseProperty")
//   return processResponse(response)
// }
const PurchaseProperty = async (values: any): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;
  console.log(
    ss,
    "??????accessToeknaccessToeknaccessToeknaccessToeknaccessToekn????"
  );
  console.log(
    accessToekn,
    "accessToeknaccessToeknaccessToeknaccessToeknaccessToekn"
  );

  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.post("/api/property/purchaseProperty", {
    ...values,
  });
  return processResponse(response);
};
const addProperty = async (values: any): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;
  console.log(
    ss,
    "??????accessToeknaccessToeknaccessToeknaccessToeknaccessToekn????"
  );
  console.log(
    accessToekn,
    "accessToeknaccessToeknaccessToeknaccessToeknaccessToekn"
  );

  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.post("/api/property/add", {
    ...values,
  });
  return processResponse(response);
};
const updateProfile = async (values: any): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;
  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.put("/api/user/profile", {
    ...values,
  });
  return processResponse(response);
};
const addBank = async (values: any): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;
  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.post("/api/bankaccount/add", {
    ...values,
  });
  return processResponse(response);
};
const getProfile = async (): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;
  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.get("/api/user/profile");
  console.log("profile settings : ", response);
  return processResponse(response);
};
const getPropertyWithAuth = async (): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;
  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.get("/api/propertywithAuth");
  return processResponse(response);
};
const getviewedproperty = async (): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;

  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.get("/api/property/propertyOverview");
  return processResponse(response);
};
const getMessage = async (): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;

  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.get("/api/message");
  return processResponse(response);
};
const recordviewedProperty = async (
  values: any
): Promise<Types.getResponse> => {
  console.warn(values, "ffdd");

  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;

  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.post("/api/property/recordview", { ...values });
  return processResponse(response);
};
const sendMsg = async (values: any): Promise<Types.getResponse> => {
  console.warn(values, "ffdd");

  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;

  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.post("/api/message/send", { ...values });
  return processResponse(response);
};
const verifyAccount = async (values: any): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;

  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.post("/api/bankaccount/verify", { ...values });
  return processResponse(response);
};
const replyMessage = async (values: any): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;

  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.post("/api/message/reply", { ...values });
  return processResponse(response);
};
const getchatHistory = async (values: any): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;
  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.get(`/api/convasation?conversationId=${values}`);
  return processResponse(response);
};
const getNotification = async (): Promise<Types.getResponse> => {
  let userToken = await GetPersistData("Loggedinstate");
  let ss = JSON.parse(userToken);

  let accessToekn = ss?.accessToken;
  await api.setHeaders({ "krip-access-token": accessToekn });
  const response = await api.get("/api/utils/getNotification");
  console.warn(response, "drink");

  return processResponse(response);
};
export {
  signInUser,
  userLogin,
  getProperty,
  getPropertyType,
  Forgetpassword,
  ResetPassword,
  userSignUp,
  accountVerification,
  getBuildingAmenities,
  getAppliancesAndFinishings,
  PurchaseProperty,
  addProperty,
  getPropertyWithAuth,
  recordviewedProperty,
  getviewedproperty,
  sendMsg,
  getMessage,
  updateProfile,
  getProfile,
  addBank,
  getBankList,
  verifyAccount,
  getchatHistory,
  replyMessage,
  getNotification,
};
