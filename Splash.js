import React, { useState, useEffect, useRef } from "react";
import {View, Image, StyleSheet, StatusBar} from "react-native"
import App from './App'

function Load(){
    const [loaded, setLoaded] = useState(false)

    useEffect(()=>{
        setTimeout(()=>{
            setLoaded(true)
        }, 2455)
    }, [])
    return (
        <>
            {
                loaded ? 
                <App />
                : 
                <View style={styles.container}>
                    <Image source={require('./assets/splash/splash.png')} style={styles.image}/>
                </View>
            }
        </>
        
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(0, 32, 57)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {

    }
})
export default Load;